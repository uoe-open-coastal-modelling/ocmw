OCMW
====

[![Documentation Status](https://readthedocs.org/projects/ocmw/badge/?version=latest)](https://ocmw.readthedocs.io/en/latest/?badge=latest) 

![OCMW Workflow Diagram](https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/raw/main/images/OCMW_workflow_diagram_cropped.png)

The purpose of this repository is to collate and consolidate our workflow for coastal model development, post-processing, and analysis.

Documentation
-------------

The latest version of the documentation is available on [Read the Docs](https://ocmw.readthedocs.io/en/latest/).


Installation
------------

OCMW source code can be download locally so that you can modify it, you can clone the repository and then use pip to install it as an 'editable' package:
    
    $ git clone https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw
    $ cd ocmw
    $ pip install -e .
	 
Once installed, to create documentation::

	$ cd ocmw/docs
	$ make html

To avoid conflicts between different versions of packages, we recommend installing inside an Anaconda or python environment.
However, this is not required.

Conda Virtual Environment
-------------------------

Included in the repository is an environment.yml file that can be used to build a conda environment for installing the OCMW package. 
To build the conda environment in an Anaconda shell, run the commands:

	$ cd ocmw
    $ conda env create -f environment.yml
        
This will build a conda environment called `ocmw-env`. 
To check that the environment has been built use:

    $ conda env list
        
The environment `ocmw-env` should be in the list. 

To activate this environment use:

    $ conda activate ocmw-env

The ``OCMW`` package is install in this environment by changing to the location of the `ocmw` local repository then use:

    $ pip install -e .
    
Python Virtual Environment
--------------------------

A similar process can be followed to create a python virtual environment.
To create and activate the environment, run:

    $ python3 -m venv ocmw-env
    $ source ocmw-env/bin/activate
	
To install the ``OCMW`` tools, run:

    $ cd ocmw
    $ pip install -e .


	
Licence
-------

OCMW is copyright through the Institute for Energy Systems, School of Engineering, The University of Edinburgh. 
The software is distributed under the Revised BSD Licence.
See the [licence](Licence-BSD-3.txt) for more information.


