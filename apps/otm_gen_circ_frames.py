#!/usr/bin/env python3

"""
Extract circulation frames for a set of timesteps from OpenTelemac 
model 2D output.

Chris Old
IES, School of Engineering, University of Edinburgh
Oct 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import sys
import getopt
import os
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.dataproc.otm_extraction import extractMesh, enhanceMesh
from ocmw.dataproc.otm_extraction import InitialiseFile
from ocmw.dataproc.otm_vectorfield import getCirculationField, frameSetIndices
from ocmw.dataproc.ocmw_extract import load_ocmw_file
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
    
    
#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:f:c:n:",["--dpath","--fname","--ncores","--setnum"])
    except getopt.GetoptError:
        print('USAGE: ./otm_gen_circ_frames.py -d <dpath> -f <fname> -c <ncores> -n <setnum>')
        sys.exit(2)
    if opts == []:
        print('USAGE: ./otm_gen_circ_frames.py -d <dpath> -f <fname> -c <ncores> -n <setnum>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('USAGE: ./otm_gen_circ_frames.py -d <dpath> -f <fname> -c <ncores> -n <setnum>')
            sys.exit(2)
        elif opt in ("-d","--dpath"):
            filePath = arg
        elif opt in ("-f","--fname"):
            fileName = arg
        elif opt in ("-c","--numcores"):
            nCores = int(arg)
        elif opt in ("-n","--setnum"):
            setNum = int(arg)
    slfHnd = InitialiseFile(filePath+'/'+fileName)
    meshPath = filePath.replace('RESULTS','EXTRACT')
    if os.path.isfile(meshPath+'/mesh.mat'):
        mesh = load_ocmw_file(meshPath, 'mesh.mat')
        if 'cWghts' not in mesh.keys():
            mesh = enhanceMesh(mesh)
    else:
        mesh = extractMesh(slfHnd,enhance=True,meshFile='mesh.mat',saveMesh=True)
    nFrames = np.max(mesh['times'].shape)
    frameSet = frameSetIndices(nFrames, nCores, setNum)
    fname = getCirculationField(slfHnd, mesh, tsteps=frameSet, dispFreq=100)
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
