#!/usr/bin/env python3

"""
Merge vector analysis files from OpenTelemac model output

Chris Old
IES, School of Engineering, University of Edinburgh
Feb 2020

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import sys
import getopt
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles
from ocmw.dataproc.ocmw_extract import load_ocmw_file, save_ocmw_file, ocmwExtn
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:s:",["--dpath","--srchstr"])
    except getopt.GetoptError:
        print('USAGE: ./otm_merge_model_vector_fields.py -d <dpath> -s <srchstr>')
        sys.exit(4)
    if opts == []:
        print('USAGE: ./otm_merge_model_vector_fields.py -d <dpath> -s <srchstr>')
        sys.exit(4)
    for opt, arg in opts:
        if opt in ['-h','--help']:
            print('USAGE: ./otm_merge_model_vector_fields.py -d <dpath> -s <srchstr>')
            sys.exit()
        elif opt in ("-d","--dpath"):
            filePath = arg
        elif opt in ("-s","--srchstr"):
            srchstr = arg
    filePath = os.path.join(filePath,'EXTRACT')
    files = getListOfFiles(filePath,'*'+srchstr+'*gradient*'+ocmwExtn)
    nfiles = len(files)
    print('Number of files to merge = '+str(nfiles))
    for f in np.arange(nfiles):
        data = load_ocmw_file(filePath,files[f])
        if f == 0:
            nsteps = data['nsteps']
            dpth = data['dpth'].copy()
            elev = data['elev'].copy()
            velx = data['U'].copy()
            vely = data['V'].copy()
            dhdx = data['dhdx'].copy()
            dhdy = data['dhdy'].copy()
            dudx = data['dudx'].copy()
            dudy = data['dudy'].copy()
            dvdx = data['dvdx'].copy()
            dvdy = data['dvdy'].copy()
            vort = data['vort'].copy()
        else:
            dpth = np.concatenate((dpth,data['dpth'].copy()),axis=1)
            elev = np.concatenate((elev,data['elev'].copy()),axis=1)
            velx = np.concatenate((velx,data['U'].copy()),axis=1)
            vely = np.concatenate((vely,data['V'].copy()),axis=1)
            dhdx = np.concatenate((dhdx,data['dhdx'].copy()),axis=1)
            dhdy = np.concatenate((dhdy,data['dhdy'].copy()),axis=1)
            dudx = np.concatenate((dudx,data['dudx'].copy()),axis=1)
            dudy = np.concatenate((dudy,data['dudy'].copy()),axis=1)
            dvdx = np.concatenate((dvdx,data['dvdx'].copy()),axis=1)
            dvdy = np.concatenate((dvdy,data['dvdy'].copy()),axis=1)
            vort = np.concatenate((vort,data['vort'].copy()),axis=1)
    for istp in np.arange(nsteps):
        datOut = {}
        datOut['dpth'] = dpth[istp,:].astype(np.float)
        datOut['elev'] = elev[istp,:].astype(np.float)
        datOut['velx'] = velx[istp,:].astype(np.float)
        datOut['vely'] = vely[istp,:].astype(np.float)
        datOut['dhdx'] = vely[istp,:].astype(np.float)
        datOut['dhdy'] = vely[istp,:].astype(np.float)
        datOut['dudx'] = vely[istp,:].astype(np.float)
        datOut['dudy'] = vely[istp,:].astype(np.float)
        datOut['dvdx'] = vely[istp,:].astype(np.float)
        datOut['dvdy'] = vely[istp,:].astype(np.float)
        datOut['vort'] = vort[istp,:].astype(np.float)
        matFile = '_'.join(files[0].split('_')[0:-1])+'_STEP'+str(istp).zfill(4)+ocmwExtn
        save_ocmw_file(filePath,matFile,datOut)
        del datOut
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
