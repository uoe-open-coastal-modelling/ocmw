#!/usr/bin/env python3

"""
Batch process vector analysis of OpenTelemac model output

Chris Old
IES, School of Engineering, University of Edinburgh
Feb 2020

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import sys
import getopt
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:f:c:",["--dpath","--fname","--numcores"])
    except getopt.GetoptError:
        print('USAGE: ./otm_batch_model_vector_fields.py -d <dpath> -f <fname> -c <numcores>')
        sys.exit(4)
    if opts == []:
        print('USAGE: ./otm_batch_model_vector_fields.py -d <dpath> -f <fname> -c <numcores>')
        sys.exit(4)
    for opt, arg in opts:
        #print(opt)
        if opt == '-h':
            print('USAGE: ./otm_batch_model_vector_fields.py -d <dpath> -f <fname> -c <numcores>')
            sys.exit()
        elif opt in ("-d","--dpath"):
            filePath = arg
        elif opt in ("-f","--fname"):
            fileName = arg
        elif opt in ("-c","--numcores"):
            nCores = int(arg)
    dataPath = filePath + '/RESULTS'
    print('Data Path: '+dataPath+'\n\n')
    print('')
    print(fileName)
    print(nCores)
    logPath = filePath+'/EXTRACT/'
    for setNum in np.arange(nCores):
        logFile = logPath+'batch_vector_set_'+str(setNum).zfill(4)+'.log'
        print('logfile: '+logFile)
        procstr = ' '.join(['nohup','./otm_model_vector_fields.py',
                            '-d',dataPath,'-f',fileName,'-c',str(nCores),'-n',str(setNum),
                            '>',logFile,'2>&1 &'])
        print(procstr)
        os.system(procstr)
        print('Process of Set '+str(setNum)+' Submitted'+'  N Cores: '+str(nCores)+'  Set Num: '+str(setNum))
    print('********   '+str(nCores)+' Jobs Submitted   ********\n\n')
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])



