#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 12:58:38 2023

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import sys
import getopt
import os
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles
from ocmw.dataproc.processADCPData import merge_adcp_daily_files
from ocmw.dataproc.processModelData import merge_model_daily_files
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def dataExists(datapath,srchstr):
    dataExists = False
    if os.path.isdir(datapath):
        files = getListOfFiles(datapath,srchstr)
        if len(files) > 0:
            dataExists = True
    return dataExists


def main(argv):

    try:
        opts, args = getopt.getopt(argv,"hd:s:t:o:",["--dir","--srch","--type","--outf"])
    except getopt.GetoptError:
        print('./merge_ocmw_extracts.py -d <dir> -s <srch> -t <type> -o <outf>')
        sys.exit(2)
    if opts == []:
        print('./merge_ocmw_extracts.py -d <dir> -s <srch> -t <type> -o <outf>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('USAGE: ./merge_ocmw_extracts.py -d <dir> -s <srch> -t <type> -o <outf>')
            sys.exit(2)
        elif opt in ("-d","--dir"):
            dataPath = arg
        elif opt in ("-s","--srch"):
            srchStr = arg
        elif opt in ("-t","--type"):
            dataType = arg
        elif opt in ("-o","--outf"):
            outFile = arg
    
    if dataExists(dataPath,srchStr):
        if dataType.lower() in ['adcp','obs']:
            mrgd = merge_adcp_daily_files(dataPath, srchStr, outFile)
        elif dataType.lower() in ['model','pred']:
            mrgd = merge_model_daily_files(dataPath, srchStr, outFile)
        else:
            print('Invalid data type: '+dataType)
            print('Valid data types: [ADCP, Obs, Model, Pred]')
            print('Process terminated.')
    else:
        print('Data '+dataPath+'/*'+srchStr+'* not found')
        print('Process terminated.')
    
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
