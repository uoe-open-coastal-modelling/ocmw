#!/usr/bin/env python3

"""
Transect data extraction from OpenTelemac model output

Chris Old
IES, School of Engineering, University of Edinburgh
Sep 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import sys
import getopt
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.dataproc.otm_extraction import parseTransectRunFile
from ocmw.dataproc.otm_extraction import extractTransect
from ocmw.dataproc.ocmw_extract import save_ocmw_file
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):

    try:
        opts, args = getopt.getopt(argv,"hf:",["--fname"])
    except getopt.GetoptError:
        print('USAGE: ./extract_transect.py -f <fname>')
        sys.exit(2)
    if opts == []:
        print('USAGE: ./extract_transect.py -f <fname>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('USAGE: ./extract_transect.py -f <fname>')
            sys.exit(2)
        elif opt in ("-f","--fname"):
            fileName = arg
    

    print('Process complete...')
    
    params = parseTransectRunFile(fileName)

    print('  MODEL_'+params['runStr']+'  '+params['titleStr'])
    trnsct = extractTransect(params['dataPath'],params['runStr'],
                             params['titleStr'],params['loc01'],
                             params['loc02'],params['dL'])
    
    save_ocmw_file(params['outPath'],params['outFile'],trnsct)
    
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
