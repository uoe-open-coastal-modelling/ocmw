#!/usr/bin/env python3

"""
Vector Field analysis of OpenTelemac model output

Chris Old
IES, School of Engineering, University of Edinburgh
Sep 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import sys
import getopt
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.dataproc.otm_extraction import extractMesh
from ocmw.dataproc.otm_vectorfield import processNodeSet
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:f:c:n:",["--dpath","--fname","--numcores","--setnum"])
    except getopt.GetoptError:
        print('USAGE: ./otm_model_vector_fields.py -d <dpath> -f <fname> -c <numcores> -n <setnum>')
        sys.exit(2)
    if opts == []:
        print('USAGE: ./otm_model_vector_fields.py -d <dpath> -f <fname> -c <numcores> -n <setnum>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('USAGE: ./otm_model_vector_fields.py -d <dpath> -f <fname> -c <numcores> -n <setnum>')
            sys.exit(2)
        elif opt in ("-d","--dpath"):
            filePath = arg
        elif opt in ("-f","--fname"):
            fileName = arg
        elif opt in ("-c","--numcores"):
            nCores = int(arg)
        elif opt in ("-n","--setnum"):
            setNum = int(arg)
    if setNum == 0:
        srchStr = fileName[0:-7]
        extractMesh(filePath,srchStr,meshFile='mesh.mat',saveMesh=True)
    processNodeSet(filePath,fileName,nCores,setNum)
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
