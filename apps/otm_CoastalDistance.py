#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Extract distance to coast of nodes in an OpenTelemac Geometry file relative
to a set of shapefiles containing coastlines as line geometry data.

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import sys
import getopt
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.dataproc.otm_extraction import InitialiseFile
from ocmw.design import gradRaster as grst
# Other Dependencies


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
def parseRunFile(runFile):
    params = {}
    shpFiles = []
    with open(runFile) as fin:
        for line in fin:
            astr = line.strip()
            if '=' in astr:
                if 'Data File Path' in astr:
                    val = astr.split(' = ')[1]
                    params['dataFile'] = val
                if 'Boundary ID' in astr:
                    val = astr.split(' = ')[1]
                    params['boundID'] = int(val)
                if 'Output File Path' in astr:
                    val = astr.split(' = ')[1]
                    params['outFile'] = val
                if 'Shapefile' in astr:
                    val = astr.split(' = ')[1]
                    shpFiles.append(val)
    params['shapefiles'] = shpFiles
    return params

def dist2coast(runFile):
    # Parse run conifguration file
    params = parseRunFile(runFile)
    
    # Open telemac model SLF geo file
    slf = InitialiseFile(params['dataFile'])
    # Extract node coordinates
    x = slf.meshx
    y = slf.meshy
    
    mindist = None
    # Loop over all shapefiles 
    for afile in params['shapefiles']:
        spath = os.path.dirname(afile)
        fname = os.path.basename(afile)
        dist = grst.distance2coast(spath,fname,[params['boundID']],x,y)
        if mindist is None:
            mindist = dist
        else:
            mindist = np.minimum(mindist,dist)
    
    # Write to ASCII file
    outf = open(params['outFile'],'w')
    outf.writelines("Easting Northing Distance\n")
    for indx in range(len(x)):
        outf.writelines(str(x[indx])+" "+str(y[indx])+" "+str(mindist[indx])+"\n")
    
    outf.close()
    
    return

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    # Parse input command line parameters
    try:
        opts, args = getopt.getopt(argv,"hf:",["--file"])
    except getopt.GetoptError:
        print('USAGE python3 otm_CoastalDistance.py -f <file>')
        sys.exit(2)
    if opts == []:
        print('USAGE python3 otm_CoastalDistance.py -f <file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h','--help'):
            print('USAGE: ./otm_CoastalDistance.py -f <file>')
            sys.exit()
        elif opt in ("-f","--file"):
            runFile = arg

    dist2coast(runFile)

    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])
