#!/usr/bin/env python3

"""
Batch process to extract circulation field as frames from OpenTelemac 
model 2D output.

Chris Old
IES, School of Engineering, University of Edinburgh
Oct 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import sys
import getopt
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.dataproc.otm_extraction import extractMesh, enhanceMesh
from ocmw.dataproc.ocmw_extract import load_ocmw_file
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hd:f:c:",["--dpath","--fname","--numcores"])
    except getopt.GetoptError:
        print('USAGE: ./otm_batch_gen_circ_frames.py -d <dpath> -f <fname> -c <numcores>')
        sys.exit(4)
    if opts == []:
        print('USAGE: ./otm_batch_gen_circ_frames.py -d <dpath> -f <fname> -c <numcores>')
        sys.exit(4)
    for opt, arg in opts:
        #print(opt)
        if opt == '-h':
            print('USAGE: ./otm_batch_gen_circ_frames.py -d <dpath> -f <fname> -c <numcores>')
            sys.exit()
        elif opt in ("-d","--dpath"):
            dataPath = arg
        elif opt in ("-f","--fname"):
            fileName = arg
        elif opt in ("-c","--numcores"):
            nCores = int(arg)

    print('Data Path: '+dataPath)
    print('Data File: '+fileName+'\n')
    print('Number of Cores Used = '+str(nCores)+'\n')

    logPath = dataPath.replace('RESULTS','EXTRACT')
    print('Get model mesh...')
    meshPath = dataPath.replace('RESULTS','EXTRACT')
    if os.path.isfile(meshPath+'/mesh.mat'):
        mesh = load_ocmw_file(meshPath, 'mesh.mat')
        if 'cWghts' not in mesh.keys():
            mesh = enhanceMesh(mesh)
    else:
        slfFile = dataPath+'/'+fileName
        mesh = extractMesh(slfFile,enhance=True,meshFile='mesh.mat',saveMesh=True)
    print('Generate batch processes...')
    for setNum in np.arange(nCores):
        logFile = logPath+'/'+'batch_circ_set_'+str(setNum).zfill(4)+'.log'
        print('logfile: '+logFile)
        procstr = ' '.join(['nohup','./otm_gen_circ_frames.py',
                            '-d',dataPath,'-f',fileName,'-c',str(nCores),'-n',str(setNum),
                            '>',logFile,'2>&1 &'])
        print(procstr)
        os.system(procstr)
        print('Process of Set '+str(setNum)+' Submitted'+'  N Cores: '+str(nCores)+'  Set Num: '+str(setNum))
    print('********   '+str(nCores)+' Jobs Submitted   ********\n\n')
    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

if __name__ == "__main__":
    main(sys.argv[1:])



