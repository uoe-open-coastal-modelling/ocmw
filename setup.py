import setuptools
from setuptools import setup

setup(name='ocmw',
      version='0.0b0',
      description='Open Coastal Modelling Workflow',
      url='https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw',
      author='Chris Old',
      author_email='',
      license='BSD',
      packages=setuptools.find_packages(),
      zip_safe=False,
      python_requires='>=3.9',
      install_requires=[
                  'numpy',
                  'scipy',
                  'pandas',
                  'matplotlib',
                  'h5py',
                  'netCDF4',
                  'metpy',
                  'cartopy',
                  'utm',
                  'meshio',
                  'scikit-learn',
                  'utide',
            ]
      )
