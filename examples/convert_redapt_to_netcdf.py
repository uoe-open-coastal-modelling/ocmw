#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 12:01:47 2021

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import numpy as np
import h5py
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles
from ocmw.core.timeFuncs import dateStrFromDateNum, matlab2python
from ocmw.dataman.adcp2netcdf import build_adcp_netcdf
from ocmw.dataman.adcp2netcdf import populate_adcp_core, populate_adcp_fields
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#deploy = 'ADCP01_NW_Dep5'
deploy = 'ADCP02_NW_Dep5'
#deploy = 'ADCPTD7_01_Dep1'
#deploy = 'ADCPTD7_02_Dep1'

#dpath = 'E:/ReDAPT/Data_Processed/'+deploy
dpath = 'D:/Data/ReDAPT/MAT/'+deploy
files = getListOfFiles(dpath,'*.mat')

genfields = ['beam','ampl','corr','pcgd','inst','earth','vert','qc']
loadfields = ['beam','ampl','corr','pcgd','inst','earth','qc']

#ncpath = 'E:/ReDAPT/Data_Standard/'+deploy
ncpath = 'D:/Data/ReDAPT/NETCDF/'+deploy
findx = 0

# open source deployment data file
data_file = os.path.join(dpath,files[0]).replace('\\','/')
data = h5py.File(data_file, 'r')
src = data['data']
# extract data dimensions
num_beams = np.int64(src['Config']['numBeams'][0][0])
num_bins = np.int64(src['Config']['numCells'][0][0])
# close file
data.close()
del src  


for file in files:

    print(file)
    dfile = os.path.join(dpath,file).replace('\\','/')
    src = h5py.File(dfile, 'r')
    dn0 = matlab2python(np.double(np.floor(src['data']['TimestampUTC'][0][0])))
    dn1 = matlab2python(np.double(np.floor(src['data']['TimestampUTC'][0][-1])))
    
    print(dn0,dn1)
    
    t = matlab2python(src['data']['TimestampUTC'][0])
    src.close()
    
    ndays = np.int64(dn1-dn0)+1
    
    for aday in range(ndays):
        
        print(aday)

        tstr = dateStrFromDateNum(dn0+aday,timeFmt='%Y%m%d')
        ncfile = deploy+'_'+tstr+'.nc'
        print(ncfile)
    
        if not os.path.isfile(ncpath+'/'+ncfile):
            build_adcp_netcdf(ncpath,ncfile,genfields,num_beams,num_bins)
            populate_adcp_core(dpath,dfile,ncpath,ncfile)

        indices = np.where( (t >= dn0+aday) & (t < dn0+aday+1.0) )[0]
        populate_adcp_fields(dpath,dfile,ncpath,ncfile,loadfields,indices)


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------
