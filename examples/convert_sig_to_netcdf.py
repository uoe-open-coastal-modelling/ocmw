#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 12:01:47 2021

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import numpy as np
import utm
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles, loadmat
from ocmw.core.timeFuncs import dateStrFromDateNum, matlab2std
from ocmw.dataman.adcp2netcdf import metaDataObj, build_adcp_netcdf
from ocmw.dataman.adcp2netcdf import populate_sig_core, populate_sig_fields
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def populateMetadata():
    metadata = metaDataObj()
    metadata.title = 'RealTide UEDIN SIG1000 standardization'
    metadata.projects = 'RealTide'
    metadata.references = ''
    metadata.site = 'Fromveur Strait, France'
    metadata.deploy_id= ''
    metadata.deploy_mooring_type = 'Concrete bed frame'
    metadata.deploy_vessel = 'Olympic Challenger'
    metadata.deploy_vessel_operator = 'Olympic Subsea ASA'
    metadata.geospatial_easting = 0.0
    metadata.geospatial_northing = 0.0
    metadata.geospatial_easting = 0.0
    metadata.geospatial_northing = 0.0
    metadata.geospatial_crs = ''
    metadata.data_type = 'field measurement'
    metadata.processing_level = '1'
    metadata.quality_control_indicator = '1'
    
    # Instrument
    metadata.instrument_owner = 'Universidade do Algarve'
    metadata.instrument_maker = 'Nortek'
    metadata.instrument_type = 'Signature'
    metadata.instrument_freq = '1000kHz'
    metadata.instrument_serial_no = ''
    metadata.instrument_firmware_ver = ''
    metadata.instrument_head_hab = 0.0
    
    # File generation
    metadata.contact = ''
    metadata.institution = 'IES, School of Engineering, The University of Edinburgh, UK'
    metadata.data_assembly_center = ''
    metadata.date_created = ''
    metadata.date_updated = ''
    metadata.history = ''
    metadata.naming_authority = 'University of Edinburgh'
    metadata.data_format_version = '1.0'
    metadata.conventions = 'CF-1.8'
    metadata.netcdf_version = 'netCDF-4 classic model'
    metadata.license = ''
    metadata.citation = ''
    metadata.doi = ''
    return metadata

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

deploy = 'UEdin_Sig1000_Nov5'

deploy_utm = [349601.7, 5368063.6]
deploy_loc = utm.to_latlon(deploy_utm[0],deploy_utm[1],30,'N')

dpath = 'D:/Data/Fromveur_SIG1000_2019/measurements_SIG1000_2019/Converted'
files = getListOfFiles(dpath,'*.mat')

metadata = populateMetadata()

genfields = ['beam','ampl','corr','inst','earth','vert']
loadfields = ['beam','ampl','corr','inst','earth']

ncpath = 'D:/Data/Fromveur_SIG1000_2019/measurements_SIG1000_2019/NETCDF'
findx = 0

# open source deployment data file
data_file = os.path.join(dpath,files[0]).replace('\\','/')
data = loadmat(data_file)
cfg = data['Config']
# extract data dimensions
num_beams = cfg['Burst_NBeams']
num_bins = cfg['Burst_NCells']
# set metadata
metadata.instrument_serial_no = cfg['SerialNo']
metadata.instrument_firmware_ver = str(cfg['FWVersion']/1000)
metadata.instrument_beam_angle = 25.0
metadata.instrument_head_hab = 0.8
metadata.geospatial_easting = deploy_utm[0]
metadata.geospatial_northing = deploy_utm[0]
metadata.geospatial_latitude = deploy_loc[0]
metadata.geospatial_longitude = deploy_loc[1]
metadata.geospatial_crs = 'WGS84 / UTM Zone 30N'

del cfg


for file in files:

    print(file)
    dfile = os.path.join(dpath,file).replace('\\','/')
    src = loadmat(dfile)
    dn0 = np.double(np.floor(matlab2std(src['Data']['Burst_Time'][0])))
    dn1 = np.double(np.floor(matlab2std(src['Data']['Burst_Time'][-1])))
    
    print(dn0,dn1)
    
    t = matlab2std(src['Data']['Burst_Time'])
    
    ndays = np.int64(dn1-dn0)+1
    
    for aday in range(ndays):
        
        print(aday)

        tstr = dateStrFromDateNum(dn0+aday,timeFmt='%Y%m%d')
        ncfile = deploy+'_'+tstr+'.nc'
        print(ncfile)
    
        if not os.path.isfile(ncpath+'/'+ncfile):
            build_adcp_netcdf(ncpath,ncfile,genfields,num_beams,num_bins)
            populate_sig_core(dpath,file,ncpath,ncfile,metadata)

        indices = np.where( (t >= dn0+aday) & (t < dn0+aday+1.0) )[0]
        populate_sig_fields(dpath,dfile,ncpath,ncfile,loadfields,indices,metadata)


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------
