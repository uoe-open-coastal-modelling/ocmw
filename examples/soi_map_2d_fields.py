#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exmple of mapping 2D Open Telemac-Mascaret model fields where the data are
extracted directly from a Selafin output data file.

Chris Old
IES, School of Engineering, University of Edinburgh
Feb 2024
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.core.graphics import plotMeshField
from ocmw.core.physics import circulation2D
from ocmw.dataproc.otm_extraction import InitialiseFile, extractMesh
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#-----------------------------------------
# Open the Selafin file
#-----------------------------------------
datapath = 'D:/Models/SoI/soi_calib/RESULTS'
datafile = 'soi_calib_ext_v2_20231029_2D.slf'

slf = InitialiseFile(datapath+'/'+datafile)

#-----------------------------------------
# Extract the model mesh structure
#-----------------------------------------
mesh = extractMesh(slf,enhance=True,saveMesh=False)

#-----------------------------------------
# Get data for first timestep
#-----------------------------------------
fields = slf.get_values(0)
varNames = slf.varnames

#-----------------------------------------
# Plot bathymetry
#-----------------------------------------
print('bathymetry map')
if 'BOTTOM'.ljust(16) in varNames:
    indx = varNames.index('BOTTOM'.ljust(16))
    bathy = -fields[indx]

meshx = mesh['meshx']
meshy = mesh['meshy']
nElem = mesh['nElems']
elems = mesh['elems']
cWghts = mesh['cWghts'].copy()

print('  mapping node data to element centroids...')
bathy_cntr = np.zeros([nElem,],dtype=np.single)
for ielm in np.arange(nElem, dtype=np.int64):
    cw = cWghts[ielm]
    v = bathy[elems[ielm]]
    bathy_cntr[ielm] = np.sum((v*cw))

print('  generating plot...')
fig_bthy = plotMeshField(mesh,bathy_cntr,figsize=(6,10),vmin=0.0,vmax=70,
                    titleStr="SoI: Bathymetry",
                    unitStr="m", 
                    show_cbar=True, 
                    cbar_width="4.0%", 
                    sub_reg=[303000.0,311000.0,6185000.0,6204000.0],
                    ismesh=False)
ax = fig_bthy[1]
ax.set_xlabel('Eastings  [ m ]')
ax.set_ylabel('Northings  [ m ]')

#-----------------------------------------
# Plot velocity magnitude
#-----------------------------------------
print('velocity magnitude map')
indx = varNames.index('VELOCITY U'.ljust(16))
velu = fields[indx]
indx = varNames.index('VELOCITY V'.ljust(16))
velv = fields[indx]

vmag = np.sqrt(np.square(velu)+np.square(velv))

print('  mapping node data to element centroids...')
vmag_cntr = np.zeros([nElem,],dtype=np.single)
for ielm in np.arange(nElem, dtype=np.int64):
    cw = cWghts[ielm]
    v = vmag[elems[ielm]]
    vmag_cntr[ielm] = np.sum((v*cw))

print('  generating plot...')
fig_vmag = plotMeshField(mesh,vmag_cntr,figsize=(6,10),vmin=0.0,vmax=3.5,
                    titleStr="SoI: Velocity Magnitude",
                    unitStr="m/s", 
                    show_cbar=True, 
                    cbar_width="4.0%", 
                    sub_reg=[303000.0,311000.0,6185000.0,6204000.0],
                    ismesh=False)
ax = fig_vmag[1]
ax.set_xlabel('Eastings  [ m ]')
ax.set_ylabel('Northings  [ m ]')

#-----------------------------------------
# Plot vorticity
#-----------------------------------------
print('vorticity map')
print('  calculating vorticity...')
vort = np.zeros([nElem,],dtype=np.single)

for ielm in np.arange(nElem, dtype=np.int64):
    x = meshx[elems[ielm,:]].tolist()
    y = meshy[elems[ielm,:]].tolist()
    vx = velu[elems[ielm,:]].tolist()
    vy = velv[elems[ielm,:]].tolist()
    vort[ielm] = circulation2D(x, y, vx, vy)/mesh['area'][ielm]

print('  generating plot...')
fig_vort = plotMeshField(mesh,vort,figsize=(6,10),vmin=-0.02,vmax=0.02,
                    titleStr="SoI: 2D Vorticity",
                    unitStr="$s^{-1}$", 
                    show_cbar=True, 
                    cbar_width="4.0%", 
                    sub_reg=[303000.0,311000.0,6185000.0,6204000.0],
                    ismesh=False)
ax = fig_vort[1]
ax.set_xlabel('Eastings  [ m ]')
ax.set_ylabel('Northings  [ m ]')


print('Process complete.\n')
input('Press ENTER to exit...')


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

