#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 11:06:42 2023

@author: cold2

NOTE: This requires the Sonardyne Origin data processing tools
      Available from:
      
      https://github.com/Sonardyne/origin-message-deserialisers/tree/main
"""

#-----------------------------------------------------------------------------
# IMPORTS
#-----------------------------------------------------------------------------
# Standard Python Dependencies
# Non-Standard Python Dependencies
import scipy.io as sio
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles
from ocmw.dataman.originPD0reader import loadPD0File
# Other Dependencies


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

datapath = "D:/Data/Sample_Origin600/pd0"
outpath = datapath[:-3]+'mat'

files = getListOfFiles(datapath,'Mayflower')

for filename in files:
    data = loadPD0File(datapath,filename)
    outfile = filename[:-3]+'mat'
    sio.savemat(outpath+'/'+outfile,data)

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

