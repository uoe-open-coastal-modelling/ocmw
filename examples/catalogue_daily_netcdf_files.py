# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 10:04:18 2023

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import numpy as np
import utm
# Local Module Dependencies
from ocmw.core import timeFuncs as tf
from ocmw.core import physics as phy
from ocmw.core.fileTools import getListOfFiles, generateGlobalAttributes
from ocmw.dataproc.processADCPData import adcp_obj
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
secsPerDay = 24.0*60.0*60.0


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------
dpath = 'D:/IAA_Nova_Sonardyne/soi_sig500_oct2023/NETCDF'
files = getListOfFiles(dpath,'Oct2023')

deployStr = 'IAA-Nova_Sonardyne SoI Field Measurements 2023'

nFiles = len(files)

catalogue = {}

for indx,file in enumerate(files):
    data = adcp_obj(dpath+'/'+file)
    
    if indx == 0:
        catalogue['number_of_files'] = nFiles
        catalogue['filename'] = [file]
        catalogue['deployment'] = deployStr
        catalogue['site'] = data.site
        catalogue['instrument_type'] = data.instrument_type
        catalogue['instrument_freq'] = data.instrument_freq
        catalogue['deploy_lat'] = data.latitude
        catalogue['deploy_lon'] = data.longitude
        catalogue['deploy_easting'] = data.easting
        catalogue['deploy_northing'] = data.northing
        catalogue['head_hab'] = data.head_hab
        catalogue['n_range_cells'] = data.num_bins
        catalogue['sample_freq_Hz'] = data.sample_rate
        catalogue['start_time'] = [data.time_start]
        catalogue['end_time'] = [data.time_end]
        catalogue['n_samples'] = [len(data.times)]
        msl = np.nanmedian(data.getDepth(-0.48))
        catalogue['msl'] = [msl]
    else:
        catalogue['filename'].append(file)
        catalogue['start_time'].append(data.time_start)
        catalogue['end_time'].append(data.time_end)
        catalogue['n_samples'].append(len(data.times))
        msl = np.nanmedian(data.getDepth(-0.48))
        catalogue['msl'].append(msl)

    

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------


