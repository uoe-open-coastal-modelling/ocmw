# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 16:03:57 2023

@author: cold2
"""

from ocmw.core.fileTools import getListOfFiles
from ocmw.dataproc.processADCPData import adcp2ocmw


dpath = 'D:/IAA_Nova_Sonardyne/soi_sig500_oct2023/NETCDF'
#dpath = 'F:/IAA_Nova_Sonardyne/soi_sig500_oct2023/NETCDF'
files = getListOfFiles(dpath,'Oct2023')

for file in files:
    print('Processing ',file,'...')
    data = adcp2ocmw(dpath,file,300.0,300,d0=-0.48)
    del data
