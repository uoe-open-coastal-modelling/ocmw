#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example Script:  Validation of the FASTWATER FoW models against ReDAPT ADCP data


"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.core.tecSpecs import getTec
from ocmw.dataproc.processModelData import hub_height_data
from ocmw.calval.flowValidation import validateFlowParameter
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------
# Define turbine to model
tec = getTec('DeepGenIV')

if tec.bottomMounted:
    rstr = '_hab_'
else:
    rstr = '_dbs_'


# Model data
modDPath = 'D:/Models/fw_fow_case_study_v02'
modFName = 'fow_vort_model_merged_td7_01.mat'
modDataSrc = 'FoW Vorticity Refined Model'
modDataSet = 'TD7_01'

# Extract hub height PWRA data
ts_mod, data_mod, pwra_mod = hub_height_data(modDPath, modFName,
                                             modDataSrc, modDataSet, tec,
                                             modFName)

zhub = tec.hubHeight
ofprts = modFName.split('.')
pwrafile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_PWRA.'+ofprts[1]
modFile = modDPath+'/ANALYSIS/'+pwrafile

# In situ ADCP data
obsDPath = 'D:/Data/ReDAPT/NETCDF/ADCPTD7_01_Dep1'
obsFName = 'ADCPTD7_01_Dep01_merged.mat'
obsDataSrc = 'ReDAPT ADCP archive data'
obsDataSet = 'ADCPTD7_01_Dep1'

ts_obs, data_obs, pwra_obs = hub_height_data(obsDPath, obsFName,
                                             obsDataSrc, obsDataSet, tec,
                                             obsFName)

ofprts = obsFName.split('.')
pwrafile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_PWRA.'+ofprts[1]
obsFile = obsDPath+'/ANALYSIS/'+pwrafile


# Calculate Validataion Statistics
metrics, figs, info = validateFlowParameter(modFile,obsFile,['magnitude'],doFlowChar=False,varStr='PWRA Velocity')

for fig in figs:
    fig.show()

print('Process complete.\n')
input('Press ENTER to exit...')


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

