#-------------------------------------------------------------------------
# Data Loader for ROWE RTI/DVL Binary *.ens Files
#
# Version: 0.0
# Author: C.P. Old
#
# History:
# 15 April 2019 - Initial code creation and testing
#
#-------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# IMPORTS
#-----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import io
import time
import struct
import datetime as dt
# Non-Standard Python Dependencies
import numpy as np
import scipy.io as sio
from matplotlib import pyplot as plt
import matplotlib.dates as dates
from pytictoc import TicToc
# Local Module Dependencies
from generic.fileTools import getListOfFiles
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------

#-------------------------------------------------------------------------
# ROWE Matlab Data Structure Elements
#
# E000001    Beam Velocity                  bins x beams
# E000002    Velocity Instrument Coords     bins x beams
# E000003    Velocity Earth Coords          bins x beams
# E000004    Amplitude (dB)                 bins x beams
# E000005    Correlation                    bins x beams
# E000006    Good Beam Pings                bins x beams
# E000007    Good Earth Pings               bins x beams
# E000008    Ensemble Data                  N x 1
# E000009    Ancillary Data                 N x 1
# E000010    Bottom Tracking                N x 1
# E000011    NMEA Data                      N x 1
# E000012    Profile Engineering Data       N x 1
# E000013    Bottom Track Engineering Data  N x 1
# E000014    Systems Settings               N x 1
# E000015    Range Ttracking Data           (3 x beams + 1) x 1
# E000016    Gage Height                    N x 1
#
# Description of data structures in Manual: RTI ADCP/DVL User Guide
#
#-------------------------------------------------------------------------

def getFileSize(fh):
    currentPos = fh.tell()
    fh.seek(0,2)
    fileSize = fh.tell()
    fh.seek(currentPos)
    return fileSize

def getFirstEnsIndex(fh):
    rec0Index = 0
    isEns = False
    while not(isEns):
        hdr = readHeader(fh,rec0Index)
        isEns = isEnsemble(hdr)
        if not(isEns):
            rec0Index += 1
    return rec0Index

def readHeader(fh,fileIndex):
    fh.seek(fileIndex)
    hdr =  struct.unpack('16B4I',fh.read(32))
    return hdr

def isEnsemble(hdr):
    ensID = (128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128)
    isEns =  hdr[0:15] == ensID[0:15]
    return isEns

def getEnsNum(hdr):
    return hdr[16]

def getEnsSize(hdr):
    return hdr[18]

def getPayload(fh,fileIndex,ensSize):
    fh.seek(fileIndex)
    fmt = str(ensSize)+'B'
    payload = struct.unpack(fmt,fh.read(ensSize))
    return payload

def unpackHdr(hdr):
    ensNum = hdr[16]
    ensNumInv = hdr[17]
    ensSize = hdr[18]
    ensSizeInv = hdr[19]
    return ensNum, ensNumInv, ensSize, ensSizeInv

def isGoodData(hdr):
    isGood = ( hdr[16] == ~np.uint32(hdr[17]) ) & ( hdr[18] == ~np.uint32(hdr[19]) )
    return isGood

def readEnsData(fh,ensStart,ensSize):
    fh.seek(ensStart)
    buf = io.BytesIO(fh.read(np.int32(ensSize)))
    ensData = sio.loadmat(buf)
    return ensData

def readCheckSum(fh,csIndex):
    fh.seek(csIndex)
    checkSum, = struct.unpack('I',fh.read(4))
    return checkSum

def calcCRC16CheckSum(fh,fileIndex,ensSize):
    payload = getPayload(fh,fileIndex,ensSize)
    crc = np.uint16(0)
    for n in payload:
        
        # crc = (ushort)((byte)(crc >> 8) | (crc << 8))
        bs1 = np.uint8(crc>>8)
        bs2 = np.uint16(crc<<8)
        crc = np.uint16(bs1)^bs2
        
        # crc ^= payload[i]
        crc = crc^n
        
        # crc ^= (byte)((crc & 0xff) >> 4)
        sVal = np.uint8((crc&255)>>4)
        crc = crc^np.uint16(sVal)
        
        # crc ^= (ushort)((crc << 8) << 4)
        s1Val = crc<<8
        s2Val = s1Val<<4
        crc = np.uint16(crc)^np.uint16(s2Val)
        
        # crc ^= (ushort)(((crc & 0xff) << 4) << 1)
        aVal = crc&255
        s3Val = aVal<<4
        s4Val = s3Val<<1
        crc = np.uint16(crc)^np.uint16(s4Val)
    #end
    return crc

def getNumOfEnsembles(fileSize,ens0Index,ensSize):
    recSize = ensSize + 36
    numEns = int((fileSize-ens0Index)/recSize)
    return numEns

def initEns(fh):
    fileSize = getFileSize(fh)
    ens0Index = getFirstEnsIndex(fh)
    hdr =  readHeader(fh,ens0Index)
    ensSize = getEnsSize(hdr)
    numEns = getNumOfEnsembles(fileSize,ens0Index,ensSize)
    return ens0Index,ensSize,numEns

def loadEnsembles(fh):
    HDRLEN = 32
    ens0Index,ensSize,numEns = initEns(fh)
    #print('Number of ensembles = '+str(numEns))
    ensStart = ens0Index + HDRLEN
    csIndex = ensStart + ensSize
    recSize = csIndex + 4
    BV1 = []; BV2 = []; BV3 = []; BV4 = []
    IV1 = []; IV2 = []; IV3 = []; IV4 = []
    EV1 = []; EV2 = []; EV3 = []; EV4 = []
    Amp1 = []; Amp2 = []; Amp3 = []; Amp4 = []
    Corr1 = []; Corr2 = []; Corr3 = []; Corr4 = []
    GBP1 = []; GBP2 = []; GBP3 = []; GBP4 = []
    GEP1 = []; GEP2 = []; GEP3 = []; GEP4 = []
    EnsMetaData = [];
    Ancillary = [];
    SystemSet = [];
    for indx in range(numEns):
        ensIndex = ens0Index + indx * recSize
        ensStart = ensIndex + HDRLEN
        hdr =  readHeader(fh,ensIndex)
        if isEnsemble(hdr):
            # Load Matlab payload data
            ensData = readEnsData(fh,ensStart,ensSize)
            # Get checksum value
            checkSum = readCheckSum(fh,csIndex)
            # Calculate check sum - NOTE: this is slowest process, probably because of data read
            #crc = calcCRC16CheckSum(fh,ensStart,ensSize)
            for key in ensData.keys():
                if (key == 'E000001'): # Velocity Data Beam coord
                        BV1.append(ensData[key][:,0])
                        BV2.append(ensData[key][:,1])
                        BV3.append(ensData[key][:,2])
                        BV4.append(ensData[key][:,3])
                if (key == 'E000002'): # Velocity Data Instrument coords
                        IV1.append(ensData[key][:,0])
                        IV2.append(ensData[key][:,1])
                        IV3.append(ensData[key][:,2])
                        IV4.append(ensData[key][:,3])
                if (key == 'E000003'): # Velocity Data Earth coords
                        EV1.append(ensData[key][:,0])
                        EV2.append(ensData[key][:,1])
                        EV3.append(ensData[key][:,2])
                        EV4.append(ensData[key][:,3])
                if (key == 'E000004'): # Amplitude data
                        Amp1.append(ensData[key][:,0])
                        Amp2.append(ensData[key][:,1])
                        Amp3.append(ensData[key][:,2])
                        Amp4.append(ensData[key][:,3])
                if (key == 'E000005'): # Correlation data
                        Corr1.append(ensData[key][:,0])
                        Corr2.append(ensData[key][:,1])
                        Corr3.append(ensData[key][:,2])
                        Corr4.append(ensData[key][:,3])
                if (key == 'E000006'): # Good beam pings
                        GBP1.append(ensData[key][:,0])
                        GBP2.append(ensData[key][:,1])
                        GBP3.append(ensData[key][:,2])
                        GBP4.append(ensData[key][:,3])
                if (key == 'E000007'): # Good Earth pings
                        GEP1.append(ensData[key][:,0])
                        GEP2.append(ensData[key][:,1])
                        GEP3.append(ensData[key][:,2])
                        GEP4.append(ensData[key][:,3])
                if (key == 'E000008'): # Ensemble data
                        EnsMetaData.append(ensData[key])
                if (key == 'E000009'): # Ancillary data
                        Ancillary.append(ensData[key])
                if (key == 'E000014'): # System settings
                        SystemSet.append(ensData[key])
        else:
            print("Invalid ensemble header")
    BeamVel = [np.array(BV1),np.array(BV2),np.array(BV3),np.array(BV4)]
    InstrVel = [np.array(IV1),np.array(IV2),np.array(IV3),np.array(IV4)]
    EarthVel = [np.array(EV1),np.array(EV2),np.array(EV3),np.array(EV4)]
    Amplitude = [np.array(Amp1),np.array(Amp2),np.array(Amp3),np.array(Amp4)]
    Correlation = [np.array(Corr1),np.array(Corr2),np.array(Corr3),np.array(Corr4)]
    GoodBPings = [np.array(GBP1),np.array(GBP2),np.array(GBP3),np.array(GBP4)]
    GoodEPings = [np.array(GEP1),np.array(GEP2),np.array(GEP3),np.array(GEP4)]
    return BeamVel,InstrVel,EarthVel, \
           Amplitude,Correlation, \
           GoodBPings,GoodEPings, \
           np.squeeze(np.array(EnsMetaData)), \
           np.squeeze(np.array(Ancillary)), \
           np.squeeze(np.array(SystemSet))

def getTimestamp(metaData):
    nrecs = metaData.shape[0]
    timestamp = np.empty(nrecs)
    for i in range(nrecs):
        timestamp[i] = dates.date2num( \
                       dt.datetime(metaData[i,6],metaData[i,7],metaData[i,8], \
                                   metaData[i,9],metaData[i,10],metaData[i,11], \
                                   metaData[i,12]*10000) \
                       )
    return timestamp

def getMeanVel(Vel,Good):
    Vel1 = Vel[0]
    Vel1[Good[0][:]==0] = np.NaN
    Vel1 = np.nanmean(Vel1,0)
    Vel2 = Vel[1]
    Vel2[Good[1][:]==0] = np.NaN
    Vel2 = np.nanmean(Vel2,0)
    Vel3 = Vel[2]
    Vel3[Good[2][:]==0] = np.NaN
    Vel3 = np.nanmean(Vel3,0)
    Vel4 = Vel[3]
    Vel4[Good[3][:]==0] = np.NaN
    Vel4 = np.nanmean(Vel4,0)
    return [Vel1,Vel2,Vel3,Vel4]

#========================================================
# Main process
#========================================================

#datapath = 'M:\RealTide\Sabella\ROWE_RTI_ADCP\Deployment_01\Data_ADCP_01'
#datapath = 'M:\RealTide\Sabella\ROWE_RTI_ADCP\Deployment_01\Data_ADCP_02'
#datapath = 'M:\RealTide\Sabella\ROWE_RTI_ADCP\Deployment_02\campagne2_290_west'
#datapath = 'M:\RealTide\Sabella\ROWE_RTI_ADCP\Deployment_02\campagne2_291_east'

#datapath = 'M:/Projects_Research/RealTide/Sabella/ROWE_RTI_ADCP/Deployment_01/Data_ADCP_02'
#outpath = 'E:/RealTide/insitu_data/ROWE_RTI_2016/deploy_01/east'
#datapath = 'M:/Projects_Research/RealTide/Sabella/ROWE_RTI_ADCP/Deployment_01/Data_ADCP_01'
#outpath = 'E:/RealTide/insitu_data/ROWE_RTI_2016/deploy_01/west'
datapath = 'M:/Projects_Research/RealTide/Sabella/ROWE_RTI_ADCP/Deployment_02/campagne2_290_west'
outpath = 'E:/RealTide/insitu_data/ROWE_RTI_2016/deploy_02/west'
#datapath = 'M:/Projects_Research/RealTide/Sabella/ROWE_RTI_ADCP/Deployment_02/campagne2_291_east'
#outpath = 'E:/RealTide/insitu_data/ROWE_RTI_2016/deploy_02/east'

t = TicToc()
t.tic()

times = []
t_water = []
t_sys = []
salinity = []
pressure = []
soundspeed = []
depth = []
heading = []
pitch = []
roll = []
vel_beam_1 = []
vel_beam_2 = []
vel_beam_3 = []
vel_beam_4 = []
vel_instr_x = []
vel_instr_y = []
vel_instr_z = []
vel_instr_q = []
vel_east = []
vel_north = []
vel_up = []
vel_error = []

count = 0

t.tic()

files = getListOfFiles(datapath,'*.ens')
files = files[24:-1]

for filename in files:
    if filename.endswith(".ens"):
        f = os.path.join(datapath,filename)
        fh = open(f,'rb')
        BVel,IVel,EVel,Amps,Corrs,GBPs,GEPs,Meta,Ancil,Syst = loadEnsembles(fh)
        if Meta.ndim == 1:
            Meta = Meta.reshape(1,Meta.shape[0])
        if Ancil.ndim == 1:
            Ancil = Ancil.reshape(1,Ancil.shape[0])
        if Syst.ndim == 1:
            Syst = Syst.reshape(1,Syst.shape[0])
        ts = np.mean(getTimestamp(Meta))
        fh.close()
        if (count == 0):
            fileDay = dates.num2date(ts).strftime('%Y%m%d')
        currentDay = dates.num2date(ts).strftime('%Y%m%d')
        if not( currentDay == fileDay ):
            fname = 'rti_ens_ave_'+fileDay+'.mat'
            outpath = os.path.join(datapath,'matlab')
            outfname = os.path.join(outpath,fname)
            binSize = np.nanmean(Ancil[:,1])
            distBin1 = np.nanmean(Ancil[:,0])
            nBins = Meta[0,1]
            z = np.array(range(nBins))*binSize+distBin1
            dict = {}
            dict['times'] = times
            dict['z'] = z
            dict['t_water'] = t_water
            dict['t_sys'] = t_sys
            dict['salinity'] = salinity
            dict['pressure'] = pressure
            dict['soundspeed'] = soundspeed
            dict['depth'] = depth
            dict['heading'] = heading
            dict['pitch'] = pitch
            dict['roll'] = roll
            dict['vel_beam_1'] = np.array(vel_beam_1).transpose()
            dict['vel_beam_2'] = np.array(vel_beam_2).transpose()
            dict['vel_beam_3'] = np.array(vel_beam_3).transpose()
            dict['vel_beam_4'] = np.array(vel_beam_4).transpose()
            dict['vel_instr_x'] = np.array(vel_instr_x).transpose()
            dict['vel_instr_y'] = np.array(vel_instr_y).transpose()
            dict['vel_instr_z'] = np.array(vel_instr_z).transpose()
            dict['vel_instr_q'] = np.array(vel_instr_q).transpose()
            dict['vel_east'] = np.array(vel_east).transpose()
            dict['vel_north'] = np.array(vel_north).transpose()
            dict['vel_up'] = np.array(vel_up).transpose()
            dict['vel_error'] = np.array(vel_error).transpose()
            sio.savemat(outfname,dict)
            print(outfname)
            t.toc()
            count = 0
            fileDay = currentDay
            times = []
            t_water = []
            t_sys = []
            salinity = []
            pressure = []
            soundspeed = []
            depth = []
            heading = []
            pitch = []
            roll = []
            vel_beam_1 = []
            vel_beam_2 = []
            vel_beam_3 = []
            vel_beam_4 = []
            vel_instr_x = []
            vel_instr_y = []
            vel_instr_z = []
            vel_instr_q = []
            vel_east = []
            vel_north = []
            vel_up = []
            vel_error = []
        times.append(ts)
        t_water.append(np.mean(Ancil[:,7],0))
        t_sys.append(np.mean(Ancil[:,8],0))
        salinity.append(np.mean(Ancil[:,9],0))
        pressure.append(np.mean(Ancil[:,10],0))
        soundspeed.append(np.mean(Ancil[:,12],0))
        depth.append(np.mean(Ancil[:,11],0))
        heading.append(np.median(Ancil[:,4],0))
        pitch.append(np.median(Ancil[:,5],0))
        roll.append(np.median(Ancil[:,6],0))
        vels = getMeanVel(BVel,GBPs)
        vel_beam_1.append(vels[0].copy())
        vel_beam_2.append(vels[1].copy())
        vel_beam_3.append(vels[2].copy())
        vel_beam_4.append(vels[3].copy())
        del vels
        vels = getMeanVel(IVel,GBPs)
        vel_instr_x.append(vels[0].copy())
        vel_instr_y.append(vels[1].copy())
        vel_instr_z.append(vels[2].copy())
        vel_instr_q.append(vels[3].copy())
        del vels
        vels = getMeanVel(EVel,GEPs)
        vel_east.append(vels[0].copy())
        vel_north.append(vels[1].copy())
        vel_up.append(vels[2].copy())
        vel_error.append(vels[3].copy())
        del vels
        count += 1
    #end if
#end for

if ( currentDay == fileDay ):
    fname = 'rti_ens_ave_'+fileDay+'.mat'
    outpath = os.path.join(datapath,'matlab')
    outfname = os.path.join(outpath,fname)
    binSize = np.nanmean(Ancil[:,1])
    distBin1 = np.nanmean(Ancil[:,0])
    nBins = Meta[0,1]
    z = np.array(range(nBins))*binSize+distBin1
    dict = {}
    dict['times'] = times
    dict['z'] = z
    dict['t_water'] = t_water
    dict['t_sys'] = t_sys
    dict['salinity'] = salinity
    dict['pressure'] = pressure
    dict['soundspeed'] = soundspeed
    dict['depth'] = depth
    dict['heading'] = heading
    dict['pitch'] = pitch
    dict['roll'] = roll
    dict['vel_beam_1'] = np.array(vel_beam_1).transpose()
    dict['vel_beam_2'] = np.array(vel_beam_2).transpose()
    dict['vel_beam_3'] = np.array(vel_beam_3).transpose()
    dict['vel_beam_4'] = np.array(vel_beam_4).transpose()
    dict['vel_instr_x'] = np.array(vel_instr_x).transpose()
    dict['vel_instr_y'] = np.array(vel_instr_y).transpose()
    dict['vel_instr_z'] = np.array(vel_instr_z).transpose()
    dict['vel_instr_q'] = np.array(vel_instr_q).transpose()
    dict['vel_east'] = np.array(vel_east).transpose()
    dict['vel_north'] = np.array(vel_north).transpose()
    dict['vel_up'] = np.array(vel_up).transpose()
    dict['vel_error'] = np.array(vel_error).transpose()
    sio.savemat(outfname,dict)
    print(outfname)
    t.toc()

