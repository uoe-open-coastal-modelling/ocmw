#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of generating tide chart information from a time series of data 
extracted from OTM model output at a specfic geospatial location.

Chris Old
IES, School of Engineering, University of Edinburgh
Aug 2023

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import matplotlib.pyplot as plt
import numpy as np
import utm
# Local Module Dependencies
from ocmw.core.timeFuncs import dateNumFromDateStr, datetimeFromNum
from ocmw.core.timeFuncs import dateStrFromDateNum
from ocmw.core.tidal import get_slack_water_times, get_tidal_range, print_tide_times
from ocmw.dataproc.processModelData import dav_data, srf_data
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#-------------------------------------------
# Load data
#-------------------------------------------

# General file information
modelpath = 'D:/Models/soi_hr_msl_04'
datasrc = 'SoI HR MSL Model ( TS=4s )'
prefix = 'soi_hr_msl_04_model'

#-------------------------------------------
# Southern Location
#-------------------------------------------
filename =prefix+'_merged_southern.mat'
dataset = 'Sth_Loc'
outfile = prefix+'_south_slack.mat'

ts_dav, south_dav = dav_data(modelpath,filename,datasrc,dataset,outfile)
south_vmag = south_dav['magn']
south_elev = south_dav['surface_elev']

ts_srf, south_srf = srf_data(modelpath,filename,datasrc,dataset,outfile)
south_vsrf = south_srf['magn']

if ts_dav['crs'] in ['EPSG:32630']:
    south_loc = np.flip(np.asarray(utm.to_latlon(ts_dav['dataLoc'][0],ts_dav['dataLoc'][1],30,'N')))
else:
    south_loc = ts_dav['dataLoc']
south_msl = np.mean(ts_dav['depth'])

dnum = south_dav['times']+dateNumFromDateStr(south_dav['epoch'].replace('/','-'))
dt = []
for t in dnum:
    dt.append(datetimeFromNum(t))

#-------------------------------------------
# Northern Location
#-------------------------------------------
filename =prefix+'_merged_northern.mat'
dataset = 'Nth_Loc'
outfile = prefix+'_north_slack.mat'

ts_dav, north_dav = dav_data(modelpath,filename,datasrc,dataset,outfile)
north_vmag = north_dav['magn']
north_elev = north_dav['surface_elev']

ts_srf, north_srf = srf_data(modelpath,filename,datasrc,dataset,outfile)
north_vsrf = north_srf['magn']

if ts_dav['crs'] in ['EPSG:32630']:
    north_loc = np.flip(np.asarray(utm.to_latlon(ts_dav['dataLoc'][0],ts_dav['dataLoc'][1],30,'N')))
else:
    north_loc = ts_dav['dataLoc']
north_msl = np.mean(ts_dav['depth'])

#-------------------------------------------
# Display results for slack water
#-------------------------------------------

dstrt = dateNumFromDateStr('2023-09-10 00:00:00')
dstop = dstrt + 1.0
indx = np.where((dnum >= dstrt)&(dnum <= dstop))[0]

mps2kts = 1.94384

ts = (dnum[indx]-dnum[indx[0]])*24.0
fig, (ax1,ax2) = plt.subplots(2)
fig.set_size_inches([10,8])
ax1.plot(ts,south_vsrf[indx]*mps2kts,'k',label='Surface Velocity Magnitude  [knots]')
ax1.plot(ts,south_vmag[indx]*mps2kts,'--k',label='Depth Averaged Velocity Magnitude  [knots]')
ax1.plot(ts,south_elev[indx],'g',label='Surface Elevation  [m]')
ax1.set_xticks(np.arange(min(ts), max(ts)+1.0, 1.0))
ax1.set_xlim([0,24])
ax1.set_ylim([-2,7])
ax1.grid('on')
ax1.plot([0,24],[0,0],'k',linewidth=1)
ax1.plot([0,24],[1,1],'--b',linewidth=1)
ax1.plot([0,24],[2,2],'--b',linewidth=1)
ax1.legend(ncols=2,loc='upper center',alignment='center')
ax1.set_title('Southern Location (' +  
              '{:+.6f}'.format(south_loc[1]) + 
              r"$^{\circ}$"+'N, ' + 
              '{:+.6f}'.format(south_loc[0]) + 
              r"$^{\circ}$"+'E) , Mean Water Depth = ' + 
              '{:.1f}'.format(south_msl)+' m')

ax2.plot(ts,north_vsrf[indx]*mps2kts,'k',label='Surface Velocity Magnitude  [knot/s]')
ax2.plot(ts,north_vmag[indx]*mps2kts,'--k',label='Depth Averaged Velocity Magnitude  [knots]')
ax2.plot(ts,north_elev[indx],'g',label='Surface Elevation  [m]')
ax2.set_xticks(np.arange(min(ts), max(ts)+1.0, 1.0))
ax2.set_xlim([0,24])
ax2.set_ylim([-2,7])
ax2.grid('on')
ax2.plot([0,24],[0,0],'k',linewidth=1)
ax2.plot([0,24],[1,1],'--b',linewidth=1)
ax2.plot([0,24],[2,2],'--b',linewidth=1)
ax2.legend(ncols=2,loc='upper center',alignment='center')
ax2.set_xlabel(dateStrFromDateNum(dstrt,timeFmt="%d %B %Y")+'  [ hrs ]')
ax2.set_title('Northern Location (' + 
              '{:+.6f}'.format(north_loc[1]) + 
              r"$^{\circ}$"+'N, ' + 
              '{:+.6f}'.format(north_loc[0]) + 
              r"$^{\circ}$"+'E) , Mean Water Depth = ' + 
              '{:.1f}'.format(north_msl)+' m')

fig.show()

print('South Slack Water Times:')
for t in get_slack_water_times(dnum[indx], south_vmag[indx]):
    print(t.split(' ')[1]+' UTC')
print()

lhrange,hlrange,lowTimes,highTimes,ltindx,htindx = get_tidal_range(dnum[indx],south_elev[indx])
lowElev = south_elev[indx][ltindx]
highElev = south_elev[indx][htindx]
print_tide_times(lowTimes,highTimes,lowElev,highElev)

print('North Slack Water Times:')
for t in get_slack_water_times(dnum[indx], north_vmag[indx]):
    print(t.split(' ')[1]+' UTC')
print()

lhrange,hlrange,lowTimes,highTimes,ltindx,htindx = get_tidal_range(dnum[indx],north_elev[indx])
lowElev = north_elev[indx][ltindx]
highElev = north_elev[indx][htindx]
print_tide_times(lowTimes,highTimes,lowElev,highElev)

input('Press ENTER to continue...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

