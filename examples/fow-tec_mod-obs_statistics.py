#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 10:18:31 2023

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
import matplotlib.pyplot as plt
# Local Module Dependencies
from ocmw.core.statsTools import initialize_results
from ocmw.core.statsTools import store_results
from ocmw.core.statsTools import stats_metrics
from ocmw.core.statsTools import stats_metrics_circular
from ocmw.core.statsTools import display_results_metrics
from ocmw.core.statsTools import circ_mod_pi
from ocmw.core.flowChar import tidal_flow_peak_directions
from ocmw.core.flowChar import id_tidal_flow_peaks
from ocmw.core.flowChar import get_flood_ebb_indices
from ocmw.dataproc.ocmw_extract import load_ocmw_file
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------
# Load data
datapath = "D:/Models/FoW_TEC"

# Surface elevation
h_mod = load_ocmw_file(datapath,'depth_mod.mat')['depth_mod']
h_obs = load_ocmw_file(datapath,'depth_mea.mat')['depth_mea']
# Velocity magnitude
vmag_mod = load_ocmw_file(datapath,'U_mod.mat')['U_mod']
vmag_obs = load_ocmw_file(datapath,'U_mea.mat')['U_mea']
# Velocity direction
dir_mod = load_ocmw_file(datapath,'theta_mod.mat')['theta_mod']
dir_obs = load_ocmw_file(datapath,'theta_mea.mat')['theta_mea']
# TEC power
pwr_mod = load_ocmw_file(datapath,'P_mod.mat')['P_mod']
pwr_obs = load_ocmw_file(datapath,'P_mea.mat')['P_mea']

# Initialize results structure
val_metrics = initialize_results()

# Generate Statistics
metrics = stats_metrics(h_mod,h_obs,'elevation')
store_results(val_metrics,metrics)

metrics = stats_metrics(vmag_mod,vmag_obs,'vel mag')
store_results(val_metrics,metrics)

metrics = stats_metrics_circular(90.0-dir_mod,90.0-dir_obs,'vel dir',degrees=True)
store_results(val_metrics,metrics)

metrics = stats_metrics(pwr_mod,pwr_obs,'TEC power')
store_results(val_metrics,metrics)

print('Full time series statistics')
display_results_metrics(val_metrics,'')

'''
u_obs = vmag_obs*np.cos(np.radians(dir_obs))
v_obs = vmag_obs*np.sin(np.radians(dir_obs))

u_mod = vmag_mod*np.cos(np.radians(dir_mod))
v_mod = vmag_mod*np.sin(np.radians(dir_mod))

plt.figure()
plt.plot(u_obs,v_obs,'.')
plt.plot(u_mod,v_mod,'.')
plt.gca().axis('equal')
'''

# Separate into Ebb/Flood
magn = vmag_mod
thta = 90.0-dir_mod
thta = np.degrees(circ_mod_pi(np.radians(thta)))
elv = h_mod

# Find tidal flow direction peaks
pkdir = tidal_flow_peak_directions(thta,magn)

# Identify flood peak
peak_id = id_tidal_flow_peaks(thta,elv,pkdir)

# Get Flood and Ebb Indices
[fldindices, ebbindices] = get_flood_ebb_indices(thta,pkdir,peak_id['flood'])


plt.figure()
plt.plot(fldindices,h_mod[fldindices],'.')
plt.plot(ebbindices,h_mod[ebbindices],'.')
plt.plot(fldindices,vmag_mod[fldindices],'.')
plt.plot(ebbindices,vmag_mod[ebbindices],'.')
plt.plot(fldindices,circ_mod_pi(np.radians(90.0-dir_mod[fldindices])),'.')
plt.plot(ebbindices,circ_mod_pi(np.radians(90.0-dir_mod[ebbindices])),'.')
plt.grid('on')
plt.legend(['h_fld','h_ebb','vel_fld','vel_ebb','dir_fld','dir_ebb'])
plt.show()


th_obs = np.degrees(circ_mod_pi(np.radians(90.0-dir_obs)))
th_mod = np.degrees(circ_mod_pi(np.radians(90.0-dir_mod)))
plt.figure(figsize=[9,9])
plt.plot(th_mod,th_obs,'k.',markersize=1)
#plt.plot(th_mod[fldindices],th_obs[fldindices],'.')
#plt.plot(th_mod[ebbindices],th_obs[ebbindices],'.')
icfld = np.where(vmag_mod[fldindices]>1.0)[0]
plt.plot(th_mod[fldindices][icfld],th_obs[fldindices][icfld],'.')
icebb = np.where(vmag_mod[ebbindices]>1.0)[0]
plt.plot(th_mod[ebbindices][icebb],th_obs[ebbindices][icebb],'.')
ax = plt.gca()
ax.set_aspect(1)
ax.set_xlim([-180,180])
ax.set_ylim([-180,180])
ax.set_yticks(list(np.arange(-180,181,60)))
ax.set_xticks(list(np.arange(-180,181,60)))
plt.grid('on')
plt.xlabel('Modelled Flow Direction')
plt.ylabel('Observed Flow Direction')
plt.legend(['_','Flood Tide','Ebb Tide'])
plt.title('Comparison of flow direction for speeds above TEC cut-in')
plt.show()

# Flood stats
# Initialize results structure
fld_metrics = initialize_results()

# Generate Statistics
metrics = stats_metrics(h_mod[fldindices],h_obs[fldindices],'elevation')
store_results(fld_metrics,metrics)

metrics = stats_metrics(vmag_mod[fldindices],vmag_obs[fldindices],'vel mag')
store_results(fld_metrics,metrics)

metrics = stats_metrics_circular(90.0-dir_mod[fldindices][icfld],90.0-dir_obs[fldindices][icfld],'vel dir',degrees=True)
store_results(fld_metrics,metrics)

print('Flood phase statistics')
display_results_metrics(fld_metrics,'')


# Ebb stats
# Initialize results structure
ebb_metrics = initialize_results()

# Generate Statistics
metrics = stats_metrics(h_mod[ebbindices],h_obs[ebbindices],'elevation')
store_results(ebb_metrics,metrics)

metrics = stats_metrics(vmag_mod[ebbindices],vmag_obs[ebbindices],'vel mag')
store_results(ebb_metrics,metrics)

metrics = stats_metrics_circular(90.0-dir_mod[ebbindices][icebb],90.0-dir_obs[ebbindices][icebb],'vel dir',degrees=True)
store_results(ebb_metrics,metrics)

print('Ebb phase statistics')
display_results_metrics(ebb_metrics,'')

print('Process complete.\n')
input('Press ENTER to exit...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

