#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exmple of OTM model surface elevation data validation against XTrack altimetry
data.

Chris Old
IES, School of Engineering, University of Edinburgh
Sep 2023

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import scipy.io as sio
import numpy as np
import utm
import matplotlib.pyplot as plt
import shapefile
# Local Module Dependencies
from ocmw.core.timeFuncs import datetimeFromNum
from ocmw.core.fileTools import getListOfFiles
from ocmw.core import tidal
from ocmw.calval import xtrackReader as xtrack
from ocmw.dataproc.otm_extraction import getMultiFileLocElevation
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#-----------------------------------------------------------------
# MODEL
#-----------------------------------------------------------------
modelpath = 'E:/RegionalModels/soi_hr_msl_04'
slfpath = modelpath+'/RESULTS'
slfstr = '202310'
slffiles = getListOfFiles(slfpath,'*'+slfstr+'*_2D.slf')

domainFile = modelpath+'/DESIGN/shapefiles/islay_extend_domain_fixed.shp'

sfile = shapefile.Reader(domainFile)
feature = sfile.shapeRecords()[0]
poly = np.asarray(feature.shape.points)

#----------------------------------------------------------------
# XTRACK Altimetry derived coastal tidal harmonics
# (https://www.aviso.altimetry.fr/en/data/products/auxiliary-products/coastal-tide-xtrack.html)
#----------------------------------------------------------------
dpath = 'E:/RegionalModels/aux_data/X-TRACK'
xtfiles = getListOfFiles(dpath,'.nc')
xtvar = xtrack.loadVars(os.path.join(dpath,xtfiles[0]))
domain = [285000,450000,5255000,5450000]
#xtpts,xtindx = xtrack.getPointsInDomain(xtvar,domain)
xtpts,xtindx = xtrack.getPointsInPolygon(xtvar,poly)
xtnpts = xtpts[0].shape[0]

#----------------------------------------------------------------
# Extract Model Surface Elevation time series at xtrack points
#----------------------------------------------------------------
# OTIS constituents
tidalCons = ['M2', 'S2', 'N2', 'K1', 'M4', 'O1', 'MN4', 'Q1', 'P1', 'K2', 'MS4']

mdm2 = []
xtm2 = []


for ipt in np.arange(xtnpts):
    east = xtpts[0][ipt].copy()
    north = xtpts[1][ipt].copy()
    Loc = [east , north]
    latlon = utm.to_latlon(east,north,30,'N')
    tsData = getMultiFileLocElevation(slfpath,slffiles,Loc)
    dn = tsData['t'].copy()
    dt = np.asarray([datetimeFromNum(dnum[0]) for dnum in dn])
    wl = np.squeeze(tsData['h'])
    #coef, mdci95 = tidal.harmonicReduction(wl,dt,lat=latlon[0],constit=tidalCons,fullOutput=True)
    coef, mdci95 = tidal.harmonicReduction(wl,dt,lat=latlon[0],constit='auto',fullOutput=True)
    mdharm,mdampl,mdphase = tidal.extractHarmValues(coef)
    tsData['mdmean'] = coef['mean']
    tsData['mdharm'] = mdharm[:]
    tsData['mdampl'] = mdampl[:]
    tsData['mdphase'] = mdphase[:]
    tsData['mdmserr'] = mdci95
    m2indx = mdharm.index('M2')
    mdm2 = np.append(mdm2,[mdampl[m2indx],mdphase[m2indx],mdci95[m2indx]])
    print(mdharm[:4])
    print(mdampl[:4])
    print(mdci95[:4])
    xtharm,xtampl,xtphase,xtci95 = xtrack.extractXTrackValues(xtvar,xtindx[ipt])
    tsData['xtharm'] = xtharm
    tsData['xtampl'] = xtampl
    tsData['xtphase'] = xtphase
    tsData['xtmserr'] = xtci95
    xtm2 = np.append(xtm2,[xtampl[0],xtphase[0],xtci95[0]])
    print(xtharm[:4])
    print(xtampl[:4])
    print(xtci95[:4])
    #sio.savemat('./data/'+slfstr+'_tidal_harmonics_xtrack_'+str(ipt).zfill(3)+'.mat',tsData)
    v,indx = np.unique(tsData['t'],return_index=True)
    print(ipt,Loc,np.shape(tsData['t']),np.shape(indx),np.shape(tsData['h_nodes']),np.shape(tsData['h']))
    plt.plot(tsData['t'].tolist(),tsData['h'].tolist())
    plt.ylim(-1.0,8.0)
    plt.ylabel('Elevation  (m)')
    plt.xlabel('Time Stamp')
    plt.title('XTRACK Point '+str(ipt).zfill(3)+' - ('+str(Loc[0])+','+str(Loc[1])+')')
    plt.clf()
mdm2 = mdm2.reshape(xtnpts,3)
xtm2 = xtm2.reshape(xtnpts,3)
plt.errorbar(xtindx,mdm2[:,0],yerr=mdm2[:,2],capsize=5)
plt.errorbar(xtindx,xtm2[:,0],yerr=xtm2[:,2],capsize=5)
plt.xlabel('XTrack Point')
plt.ylabel('M2 Amplitude')

print('Process complete.\n')
input('Press ENTER to exit...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

