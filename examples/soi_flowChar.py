#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exmple of flow characterisation for data extracted from OTM model output at a
specific geosptatial location and at the hub height of a particular class of 
tidal energy converter (TEC).

Chris Old
IES, School of Engineering, University of Edinburgh
Sep 2023

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.core.tecSpecs import getTec
from ocmw.core.graphics import flow_class_diagram
from ocmw.core.graphics import speed_bin_histograms
from ocmw.core.graphics import directional_spread_plot
from ocmw.dataproc.processModelData import hub_height_data
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

# Load TEC data
tecName = 'Nova100D'
tec = getTec(tecName)

# General file information
modelpath = 'D:/Models/soi_hr_msl_04'
datasrc = 'SoI MODEL (High Res TS=4s)'
prefix = 'soi_hr_msl_04_model'

#------------------------------------------------------
# South deployment location
#------------------------------------------------------
filename = prefix+'_merged_southern.mat'
dataset = 'South Deployment'
outfile = prefix+'_sth_loc.mat'

# Extract hub heigth data
ts_mod, data_mod, pwra_sth = hub_height_data(modelpath, filename,
                                             datasrc, dataset, tec,
                                             outfile)

# Generate graphics
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=3.5)
fh_mod.show()
fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=3.5)
fh_spdh.show()
fh_dsrd = directional_spread_plot(data_mod)
fh_dsrd.show()

#------------------------------------------------------
# North deployment location
#------------------------------------------------------
filename = prefix+'_merged_northern.mat'
dataset = 'North Deployment'
outfile = prefix+'_nth_loc.mat'

# Extract hub heigth data
ts_mod, data_mod, pwra_nth = hub_height_data(modelpath, filename,
                                             datasrc, dataset, tec,
                                             outfile)
# Generate graphics
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=3.5)
fh_mod.show()
fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=3.5)
fh_spdh.show()
fh_dsrd = directional_spread_plot(data_mod)
fh_dsrd.show()

print('Process complete.\n')
input('Press ENTER to exit...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

