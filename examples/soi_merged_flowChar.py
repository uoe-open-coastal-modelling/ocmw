# -*- coding: utf-8 -*-
"""
Created on Wed Aug  3 09:31:03 2022

@author: cold2
"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.core.tecSpecs import getTec
from ocmw.core.graphics import flow_class_diagram
from ocmw.core.graphics import speed_bin_histograms
from ocmw.core.graphics import directional_spread_plot
from ocmw.dataproc.processModelData import hub_height_data, merge_model_daily_files
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

# Load TEC data
tecName = 'Nova100D'
tec = getTec(tecName)

# General file information
modelpath = 'D:/Models/soi_hr_msl_04'
datasrc = 'SoI MODEL (High Res TS=4s)'

#------------------------------------------------------
# South deployment location
#------------------------------------------------------

srchstr = 'Sth_Loc'
dataset = 'South Deployment'
filename = 'soi_hr_msl_04_model_merged_sth_loc.mat'
outfile = 'soi_hr_msl_04_sth_loc.mat'

# Merge daily files
datapath = modelpath+'/EXTRACT'
merged = merge_model_daily_files(datapath, srchstr, filename)

# Extract hub heigth data
srchstr = 'merged_sth_loc'
ts_mod, data_mod, pwra_sth = hub_height_data(modelpath, filename,
                                             datasrc, dataset, tec,
                                             outfile)

# Generate graphics
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=3.5)
fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=3.5)
fh_dsrd = directional_spread_plot(data_mod)

#------------------------------------------------------
# North deployment location
#------------------------------------------------------
srchstr = 'Nth_Loc'
dataset = 'North Deployment'
filename = 'soi_hr_msl_04_model_merged_nth_loc.mat'
outfile = 'soi_hr_msl_04_nth_loc.mat'

# Merge daily files
datapath = modelpath+'/EXTRACT'
merged = merge_model_daily_files(datapath, srchstr, filename)

# Extract hub heigth data
srchstr = 'merged_nth_loc'
ts_mod, data_mod, pwra_sth = hub_height_data(modelpath, filename,
                                             datasrc, dataset, tec,
                                             outfile)

# Generate graphics
fh_mod = flow_class_diagram(data_mod,tec=tec,velmax=3.5)
fh_spdh = speed_bin_histograms(data_mod,tec=tec,binwidth=0.1,maxfrac=0.06,maxspd=3.5)
fh_dsrd = directional_spread_plot(data_mod)

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

