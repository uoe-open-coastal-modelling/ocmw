#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of applying tidal reduction to a time series of data extracted from
multiple OTM model output files for all nodes.

Chris Old
IES, School of Engineering, University of Edinburgh
Nov 2023
"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.core.timeFuncs import dateNumFromDateStr
from ocmw.core.tidal import signalDecomposition, extractHarmValues
from ocmw.dataproc.otm_extraction import extractMultiFile2DField
from ocmw.dataproc.processModelData import locDataExists
from ocmw.dataproc.ocmw_extract import load_ocmw_file, ocmwExtn
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Load data
#-----------------------------------------------------------------------------
modelPath = 'D:/Models/soi_hr_msl_04/RESULTS'
dataPath = modelPath.replace('RESULTS','EXTRACT')
modstr = 'soi_hr_msl_04'
startstr = '20230317'
nfiles = 14
varstr = 'FREE SURFACE'

fileName = modstr+'_'+varstr.replace(' ','')+'_multi_day'+ocmwExtn

if locDataExists(dataPath, fileName):
    data = load_ocmw_file(dataPath,fileName)
else:
    data = extractMultiFile2DField(modelPath,modstr,startstr,nfiles,varstr)


#-----------------------------------------------------------------------------
# Extract time and elevation data
#-----------------------------------------------------------------------------

dnum = data['times']+dateNumFromDateStr(data['epoch'].replace('/','-'))
elev = data['FREE_SURFACE']

nNodes = np.max(elev.shape)

#-----------------------------------------------------------------------------
# Loop over all nodes applying harmonic reduction
#-----------------------------------------------------------------------------

for n in np.arange(nNodes):
    elv = elev[:,n]
    signal = signalDecomposition(dnum,elv)
    harm, ampl, phase = extractHarmValues(signal['model'])
    print(harm)
    print(ampl)
    print(phase)

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------



