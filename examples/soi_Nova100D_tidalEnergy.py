#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of comparing predicted Turbine power production from data extracted from 
OTM model output at a pair of specfic geospatial locations.

Chris Old
IES, School of Engineering, University of Edinburgh
Oct 2023

"""
# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
import numpy as np
import matplotlib.pyplot as plt
# Local Module Dependencies
from ocmw.core.fileTools import getListOfFiles
from ocmw.core.timeFuncs import dateStrFromDateNum, dateNumFromDateStr
from ocmw.core.flowChar import streamwise_flow, tec_power
from ocmw.core.tecSpecs import getTec
from ocmw.dataproc.processModelData import hub_height_data, dav_data
from ocmw.dataproc.ocmw_extract import load_ocmw_file
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def get_soi_hubht_pwra_data(modelpath,filename,datasrc,dataset,tec,outfile):
    analpath = modelpath+'/ANALYSIS'
    if not os.path.isdir(analpath):
        os.mkdir(analpath)
    ofprts = outfile.split('.')

    hab = tec.bottomMounted
    zhub = tec.hubHeight
    
    if hab:
        rstr = '_hab_'
    else:
        rstr = '_dbs_'
    
    pwrafile = ofprts[0]+rstr+str(np.int32(np.ceil(zhub))).zfill(2)+'m_PWRA.'+ofprts[1]
    
    if os.path.isdir(analpath):
        pwra_data = load_ocmw_file(analpath,pwrafile)
    else:
        modelt_ts, model_data, pwra_data = hub_height_data(modelpath,filename,datasrc,dataset,tec,outfile)
    return pwra_data


def get_soi_depavg_pwr_data(modelpath,filename,datasrc,dataset,tec,outfile):
    analpath = modelpath+'/ANALYSIS'
    if not os.path.isdir(analpath):
        os.mkdir(analpath)
    ofprts = outfile.split('.')

    rdia = tec.rotorDiam
    Cp = tec.Cp
    
    files = getListOfFiles(analpath,ofprts[0]+'_depth_avg_TS')
    if len(files) == 0:
        model_ts, model_data = dav_data(modelpath, filename, datasrc, dataset, outfile)
    else:
        datafile = ofprts[0]+'_depth_avg.'+ofprts[1]
        model_data = load_ocmw_file(analpath,datafile)
    vel_strm = streamwise_flow(model_data)
    pwr_data = tec_power(vel_strm,rdia,Cp=Cp)
    return model_data, pwr_data


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

# Load TEC data
tec = getTec('Nova100D')

# General file information
modelpath = 'D:/Models/soi_hr_msl_04'
datasrc = 'SoI MODEL (High Res TS=4s)'
outprefix = 'soi_hr_msl_04_model'

# South deployment location
filename = outprefix+'_merged_sth_loc.mat'
dataset = 'South Deployment'
outfile = outprefix+'_sth_loc.mat'

pwra_tecSth = get_soi_hubht_pwra_data(modelpath, filename, datasrc, dataset, tec, outfile)

# North deployment location
filename = outprefix+'_merged_nth_loc.mat'
dataset = 'North Deployment'
outfile = outprefix+'_nth_loc.mat'

pwra_tecNth = get_soi_hubht_pwra_data(modelpath, filename, datasrc, dataset, tec, outfile)


# Compare Power based on PWRA

t = pwra_tecSth['times']+dateNumFromDateStr(pwra_tecSth['epoch'].replace('/','-'))
t0 = np.floor(t[0])
epstr = dateStrFromDateNum(t0)

fig = plt.figure(figsize=(12,8))
axs=fig.subplots(2,1)
dP = pwra_tecNth['tec_pwr']/pwra_tecSth['tec_pwr']
axs[0].plot(t-t0,pwra_tecSth['tec_pwr']/1.0e3,label='South')
axs[0].plot(t-t0,pwra_tecNth['tec_pwr']/1.0e3,label='North')
axs[0].set_xlim([0.0,27.0])
axs[0].set_ylim([0.0,100.0])
axs[0].grid('on')
axs[0].legend()
axs[0].set_ylabel('TEC Power  [ kW ]',fontsize=8)
axs[0].set_title('Comparison of Estimated Power Production by South and North Turbines')
axs[0].set_position([0.075,0.56,0.88,0.37])

dP = pwra_tecNth['tec_pwr']-pwra_tecSth['tec_pwr']
axs[1].plot(t-t0,dP/1.0e3)
axs[1].set_xlim([0.0,27.0])
axs[1].set_ylim([-50.0,0.0])
axs[1].grid('on')
axs[1].set_xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
axs[1].set_ylabel('North-South Power Differece  [ kW ]',fontsize=8)
axs[1].set_title('Difference in Estimated Power Production by South and North Turbines')
axs[1].set_position([0.075,0.09,0.88,0.37])

fig.show()

plt.figure(figsize=(9,9))
plt.plot(pwra_tecSth['tec_pwr']/1.0e3,pwra_tecNth['tec_pwr']/1.0e3,'.')
ax = plt.gca()
ax.set_aspect('equal')
plt.grid('on')
plt.xlabel('South Power  [ kW ]')
plt.ylabel('North Power  [ kW ]')

plt.show()

# Energy yield difference
energy_tec1 = np.nansum(pwra_tecSth['tec_pwr'])*300.0
energy_tec2 = np.nansum(pwra_tecNth['tec_pwr'])*300.0

yield_difference = 100.*energy_tec2/energy_tec1

print('Energy Yield Difference: North yield is '+"{0:+.1f}".format(yield_difference)+'% of South')

# Exclude speeds below TEC cut-in
tec1indx = np.where(pwra_tecSth['magn'] < tec.cutInSpeed)[0]
tec1_pwr = pwra_tecSth['tec_pwr'].copy()
tec1_pwr[tec1indx] = np.nan
energy_tec1_limited = np.nansum(tec1_pwr)*300.0

tec2indx = np.where(pwra_tecNth['magn'] < tec.cutInSpeed)[0]
tec2_pwr = pwra_tecNth['tec_pwr'].copy()
tec2_pwr[tec2indx] = np.nan
energy_tec2_limited = np.nansum(tec2_pwr)*300.0

yield_difference_limited = 100.*energy_tec2_limited/energy_tec1_limited

print('Energy Yield Difference (above cut-in speed): North yield is '+"{0:+.1f}".format(yield_difference_limited)+'% of South')



#=============================================
# Depth-Averaged Velocity Anaysis
#=============================================

# South deployment location
filename = outprefix+'_merged_southern.mat'
dataset = 'South Deployment'
outfile = outprefix+'_sth_loc.mat'

data_tec1, pwr_da_tec1 = get_soi_depavg_pwr_data(modelpath, filename, datasrc, dataset, tec, outfile)

# North deployment location
filename = outprefix+'_merged_northern.mat'
dataset = 'North Deployment'
outfile = outprefix+'_nth_loc.mat'

data_tec2, pwr_da_tec2 = get_soi_depavg_pwr_data(modelpath, filename, datasrc, dataset, tec, outfile)



# Compare Power based on depth average velocity power

t = data_tec1['time']+dateNumFromDateStr(data_tec1['epoch'].replace('/','-'))
t0 = np.floor(t[0])
epstr = dateStrFromDateNum(t0)

fig = plt.figure(figsize=(12,8))
axs=fig.subplots(2,1)
dP = pwr_da_tec2/pwr_da_tec1
axs[0].plot(t-t0,pwr_da_tec1/1.0e3,label='South')
axs[0].plot(t-t0,pwr_da_tec2/1.0e3,label='North')
axs[0].set_xlim([0.0,27.0])
axs[0].set_ylim([0.0,200.0])
axs[0].grid('on')
axs[0].legend()
axs[0].set_ylabel('TEC Power  [ kW ]',fontsize=8)
axs[0].set_title('Comparison of Estimated Power Production by South and North Turbines (Depth Averaged Velocity)')
axs[0].set_position([0.075,0.56,0.88,0.37])

dP = pwr_da_tec2-pwr_da_tec1
axs[1].plot(t-t0,dP/1.0e3)
axs[1].set_xlim([0.0,27.0])
axs[1].set_ylim([-150.0,0.0])
axs[1].grid('on')
axs[1].set_xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
axs[1].set_ylabel('North-South Power Differece  [ kW ]',fontsize=8)
axs[1].set_title('Difference in Estimated Power Production by South and North Turbines (Depth Averaged)')
axs[1].set_position([0.075,0.09,0.88,0.37])

fig.show()

# Energy yield difference
energy_da_tec1 = np.nansum(pwr_da_tec1)*300.0
energy_da_tec2 = np.nansum(pwr_da_tec2)*300.0

yield_difference_da = 100.*energy_da_tec2/energy_da_tec1
print('Depth-Averaged velocity analysis')
print('Energy Yield Difference: North yield is '+"{0:+.1f}".format(yield_difference_da)+'% of South')

# Exclude speeds below TEC cut-in
tec1indx = np.where(pwr_da_tec1 < tec.cutInSpeed)[0]
tec1_da_pwr = pwr_da_tec1.copy()
tec1_da_pwr[tec1indx] = np.nan
energy_da_tec1_limited = np.nansum(tec1_da_pwr)*300.0

tec2indx = np.where(pwr_da_tec2 < tec.cutInSpeed)[0]
tec2_da_pwr = pwr_da_tec2.copy()
tec2_da_pwr[tec2indx] = np.nan
energy_da_tec2_limited = np.nansum(tec2_da_pwr)*300.0

yield_difference_limited_da = 100.*energy_da_tec2_limited/energy_da_tec1_limited

print('Energy Yield Difference (above cut-in speed): North yield is '+"{0:+.1f}".format(yield_difference_limited_da)+'% of South')

fig = plt.figure(figsize=(12,4))
plt.plot(t-t0,pwr_da_tec1/1.0e3,label='South')
plt.plot(t-t0,pwr_da_tec2/1.0e3,label='North')
ax = plt.gca()
ax.set_xlim([0.0,27.0])
ax.set_ylim([0.0,200.0])
ax.tick_params(axis='both', labelsize=8)
ax.set_yticklabels([])
ax.grid('on')
plt.legend(fontsize=8)
plt.xlabel('Timestamp  [days from '+epstr+']',fontsize=8)
plt.ylabel('TEC Power  [ kW ]',fontsize=8)
plt.title('Comparison of Estimated Power Production by South and North Turbines (Depth Averaged Velocity)',fontsize=10)
ax.set_position([0.05,0.14,0.92,0.77])

fig.show()

print('Process complete.\n')
input('Press ENTER to exit...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

