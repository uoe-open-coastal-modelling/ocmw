#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Chris Old
IES, School of Engineering, University of Edinburgh
Oct 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.core.graphics import plotMeshField
from ocmw.dataproc.otm_extraction import parsePolygonRunFile
from ocmw.dataproc.otm_vectorfield import processNodesInPolygon
from ocmw.dataproc.ocmw_extract import load_ocmw_file
# Other Dependencies

# ----------------------------------------------------------------------------
#   GLOBAL CONSTANTS
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

polyRunFile = '../apps/soi_polygon_extract_runfile.txt'
runParams = parsePolygonRunFile(polyRunFile)
polygon = np.zeros([runParams['nVert'],2])
polygon[:,0] = runParams['vertX']
polygon[:,1] = runParams['vertY']

datapath = 'D:/Models/soi_hr_msl_04'
filename = 'soi_hr_msl_04_20230907_dyn2d.mat'

if not os.path.isfile(datapath+'/EXTRACT/'+filename):
    runStr = filename.split('_dyn2d')[0]
    processNodesInPolygon(datapath+'/RESULTS',runStr,polygon)

data = load_ocmw_file(datapath+'/EXTRACT',filename)

vmag = np.sqrt(np.square(data['U'])+np.square(data['V']))

mesh = data['mesh']

vals = np.zeros(mesh['meshx'].shape)
vals.fill(np.nan)
nodes = data['nodes']
vals[nodes] = vmag[0,:]

fig, figcb = plotMeshField(mesh,vals,figsize=(18.0,10),vmin=0.0,vmax=3.5,
                  titleStr="",unitStr="", vstr="", qstr="", 
                  roi=None,sub_reg=[303000.0,314000.0,6178000.0,6204000.0],
                  ismesh=False)

fig.show()

print('Process complete.\n')
input('Press ENTER to exit...')

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

