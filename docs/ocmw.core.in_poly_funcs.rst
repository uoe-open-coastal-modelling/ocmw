.. _core.inpolyfuncs:

ocmw.core.in_poly_funcs module
==============================

Functions for determining the relative position of a point to a line or polygon.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.in_poly_funcs.is_left
	~ocmw.core.in_poly_funcs.cn_PnPoly
	~ocmw.core.in_poly_funcs.wn_PnPoly


.. automodule:: ocmw.core.in_poly_funcs
   :members:
   :undoc-members:
   :show-inheritance:
