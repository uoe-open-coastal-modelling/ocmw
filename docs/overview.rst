.. _overview:

.. _OCMW example: https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/tree/main/examples
.. _OCMW applications: https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/tree/main/apps

Overview of OCMW tools
======================

OCWM is a library of tools that collates and consolidates our workflow 
for coastal model development, post-processing, and analysis. The modelling 
work was targeted at high-energy site characterisation for tidal energy 
developers, and ran across a number of research projects. The numerical 
simulations where carried out using the Open Telemac-Mascaret solver, so 
there is a focus on tools for interrogating output from this model. However, 
the concepts and methods can be transferred to the output from any numerical 
solver.

The library of tools has been developed in layers, with the :ref:`core <core>` 
tools providing generic functions that are independent of any modelling system. 
From these core functions, modules related to each section of the workflow 
(:ref:`domain definition <domain>`, :ref:`model design <design>`, 
:ref:`Cal/Val <calval>`, :ref:`data processing <dataproc>`, and 
:ref:`data management <dataman>` ) have been constructed, providing functions 
specific to the workflow application. From these workflow modules a set of 
executable :ref:`applications <apps>` have been written to provide tools for automated 
processing of model data, and a set of :ref:`example <examples>` scripts created to show 
how the tools can be used to characterise a site based on model data, and to 
validate a model against `in situ` and remotely sensed data.

Internal Standardization
------------------------

The are two levels of standardization used in the construction of the OCMW tools.

#. Model data directory structure
#. Standard internal OCMW data formats and structure

The use of a stardardized model directory structure simplifies the automation of OCMW 
processes by removing the requirement to identify all directories explicitly. Using a 
standardized directory structure means that only the model root directory needs to be 
explicitly defined. The use of a standardized internal data format simplifies the 
passing of information between the OCMW processes.

.. _model-dir-struct:

Model Directory Structure
.........................

The model directory structure used for Open Telemac-Mascaret simulations takes the 
form::

    MODEL                  ( Simulation root directory )
    │
    ├───DESIGN             ( Mesh generation scripts, project files and data )
    │   ├───bathy
    │   ├───bluekenue
    │   ├───qgis
    │   └───shapefiles
    ├───TIDEATLAS          ( Tidal atlas used to force the model )
    ├───GEO                ( OTM geometry and boundary conditons files )
    ├───RUNLOGS            ( Log files generated for individual model runs )
    ├───RESTART            ( Restart files used for sequential model runs )
    │
    ├───RESULTS            ( Simulation output files )
    │
    ├───EXTRACT            ( OCMW data extraction files )
    ├───ANALYSIS           ( OCMW data analysis results )
    ├───VELOCITY           ( OCMW extracted 2-D velocity field data )
    └───IMAGES             ( OCMW generated graphics )

The essential sub-directory used by the OCMW tools to access simulation data is ``MODEL\RESULTS``. 
This contains the simulation output files; if using another numerical solver the output must be 
placed in a ``\RESULTS`` sub-directory to be compatible with the OCMW tools.
The OCMW processing tools automatically generate the ``\EXTRACT``, ``\ANALYSIS``, ``\VELOCITY`` and ``\IMAGES`` sub-directories in the same root directory as the ``\RESULTS`` sub-directory. 

The other directories are specific to the construction and running of the Open Telemac-Mascaret 
simulations. The other main advantage of using a standardized directory structure is that it 
retains all the information related to the model in a suitable form for archiving.

OCMW Internal Data Format
.........................

To simplify the passing of data between OCMW processes, a standard internal data format has been used with a set of standard variable names for common parameters. For simplicity the internal data structure is currently a python data dictionary. This was intially chosen as it allowed the data to be save in Matlab format \*.mat files simplifying the sharing of data with user not familiar with python. It became embedded as the tools were developed over a number of years. If needed the format can be changed as the code is structured so that data are loaded and saved using the functions ``load_comw_data`` and ``save_ocmw_data`` which can be replaced. The implementation is not yet completely generic, so there is ongoing work to refactor the code for consistencies. 

Some of the standard internal variables are described in :numref:`ocmwvarstab`.

.. _ocmwvarstab:
.. csv-table:: : Standard OCMW internal variables.
               :header-rows: 1
               :widths: 20, 65
               :file: ./std_ocmw_vars.csv
