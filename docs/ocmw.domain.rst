.. _domain:

ocmw.domain module
==================

.. automodule:: ocmw.domain
   :members:
   :undoc-members:
   :show-inheritance:

Tools for defining the model domain.

.. autosummary::
	:nosignatures:
	
	~ocmw.domain.bathyTools
	~ocmw.domain.substrates


Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.domain.bathyTools
   ocmw.domain.substrates
