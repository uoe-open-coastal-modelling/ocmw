.. _install:

Installation
============

**OCMW** can be installed by downloading the source code locally so that you can modify
it. You can clone the repository and then use pip to install it as an 'editable' package::
    
   $ git clone https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw.git
   $ cd ocmw
   $ pip install -e .

Once installed, to create documentation::

   $ cd ocmw/docs
   $ make html

To avoid conflicts between different versions of packages, we recommend installing inside an Anaconda or python virtual environment.

.. _condaenv:

Conda virtual environment
-------------------------

Included in the repository is an ``environment.yml`` file that can be used to build a conda environment for installing the OCMW package. 
To build the conda environment in an Anaconda shell, run the commands::

   $ cd ocmw
   $ conda env create -f environment.yml
        
This will build a conda environment called `ocmw-env`. 
To check that the environment has been built use::

   $ conda env list
   
The environment ``ocmw-env`` should be in the list. 

To activate this environment use::

   $ conda activate ocmw-env

The **OCMW** package is install in this environment by changing to the location of the ``ocmw`` local repository then running::

   $ pip install -e .
    
.. _pythonenv:

Python virtual environment
--------------------------

A similar process can be followed to create a python virtual environment.
To create and activate the environment, run::

   $ python3 -m venv ocmw-env
   $ source ocmw-env/bin/activate
	
To install the **OCMW** tools, run::

   $ cd ocmw
   $ pip install -e .

 
.. _dependencies:

Dependencies
------------

**OCMW** was developed with python 3.9, using the following dependencies:

 - `NumPy <http://www.numpy.org>`_
 - `SciPy <http://www.scipy.org>`_
 - `pandas <https://pandas.pydata.org>`_
 - `matplotlib <https://matplotlib.org/>`_
 - `h5py <https://pypi.org/project/h5py/>`_
 - `netCDF4 <https://pypi.org/project/netCDF4/>`_
 - `MetPy <https://unidata.github.io/MetPy/latest/index.html>`_
 - `Cartopy <https://pypi.org/project/Cartopy/>`_
 - `utm <https://pypi.org/project/utm/>`_
 - `meshio <https://pypi.org/project/meshio/>`_
 - `scikit-learn <https://scikit-learn.org/stable/>`_
 - `UTide <https://pypi.org/project/UTide/>`_
