.. _calval.ww3WavesValidation:

ocmw.calval.ww3WavesValidation module
=====================================

Functions for calculating parameter based validation statistics for a set of matchup database records.

.. autosummary::
	:nosignatures:
	
	~ocmw.calval.ww3WavesValidation.getVarName
	~ocmw.calval.ww3WavesValidation.getSurfaceIndx
	~ocmw.calval.ww3WavesValidation.extract_model
	~ocmw.calval.ww3WavesValidation.time_interpolate_obs
	~ocmw.calval.ww3WavesValidation.initialize_results
	~ocmw.calval.ww3WavesValidation.calculate_circular_metrics
	~ocmw.calval.ww3WavesValidation.calculate_metrics
	~ocmw.calval.ww3WavesValidation.results_record
	~ocmw.calval.ww3WavesValidation.store_valid_results
	~ocmw.calval.ww3WavesValidation.display_tabulated_results
	~ocmw.calval.ww3WavesValidation.save_cleaned_ts_data
	~ocmw.calval.ww3WavesValidation.load_binary_results
	~ocmw.calval.ww3WavesValidation.process_record
	~ocmw.calval.ww3WavesValidation.validate_records


.. automodule:: ocmw.calval.ww3WavesValidation
   :members:
   :undoc-members:
   :show-inheritance:
