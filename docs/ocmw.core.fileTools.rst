.. _core.fileTools:

ocmw.core.fileTools module
==========================

Basic file manipulation tools used in the OCMW processes.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.fileTools.getListOfFiles
	~ocmw.core.fileTools.loadmat
	~ocmw.core.fileTools.generateGlobalAttributes


.. automodule:: ocmw.core.fileTools
   :members:
   :undoc-members:
   :show-inheritance:
