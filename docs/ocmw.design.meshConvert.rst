.. _design.meshConvert:

ocmw.design.meshConvert module
==============================

Functions for converting between mesh formats.

.. autosummary::
	:nosignatures:
	
	~ocmw.design.meshConvert.msh2slf
	~ocmw.design.meshConvert.WW3mesh2sms2dm


.. automodule:: ocmw.design.meshConvert
   :members:
   :undoc-members:
   :show-inheritance:
