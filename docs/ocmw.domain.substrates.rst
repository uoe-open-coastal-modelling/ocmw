.. _domain.substrates: 

ocmw.domain.substrates module
=============================

Functions to map sea bed substrate classes to friction coefficients.

.. autosummary::
	:nosignatures:
	
	~ocmw.domain.substrates.map_classname_C100
	~ocmw.domain.substrates.map_EMODNET_C100
	~ocmw.domain.substrates.map_EUSM_C100

.. automodule:: ocmw.domain.substrates
   :members:
   :undoc-members:
   :show-inheritance:
