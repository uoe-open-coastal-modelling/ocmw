.. _calval.flowValidation:

ocmw.calval.flowValidation module
=================================

Functions to generate validation statistics for timeseries of flow parameters from the model against in situ ADCP measurements.

The input timeseries are generated using the ``ocmw.dataproc.ocmw_extract`` functions on common timestamps.

.. autosummary::
	:nosignatures:
	
	~ocmw.calval.flowValidation.validateFlowParameter

.. automodule:: ocmw.calval.flowValidation
   :members:
   :undoc-members:
   :show-inheritance:
