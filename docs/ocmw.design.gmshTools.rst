.. _design.gmshTools:

ocmw.design.gmshTools module
============================

Tools for manupulating GMsh ASCII mesh files (\*.msh).

.. autosummary::
	:nosignatures:
	
	~ocmw.design.gmshTools.get_mesh_nodes
	~ocmw.design.gmshTools.isTag
	~ocmw.design.gmshTools.isScalar
	~ocmw.design.gmshTools.mshBathy


.. automodule:: ocmw.design.gmshTools
   :members:
   :undoc-members:
   :show-inheritance:
