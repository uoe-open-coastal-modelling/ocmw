.. _design.otmbcfile:

ocmw.design.otm_bcfile module
=============================

Tools for generating an Open Telemac-Mascaret boundary condtions file (\*.cli) from a \*.msh ASCII file.

.. autosummary::
	:nosignatures:
	
	~ocmw.design.otm_bcfile.parse_arguments
	~ocmw.design.otm_bcfile.initialise_bc_file
	~ocmw.design.otm_bcfile.clistr
	~ocmw.design.otm_bcfile.sort_bnds
	~ocmw.design.otm_bcfile.extract_mshbnds
	~ocmw.design.otm_bcfile.extract_bnd_nodes
	~ocmw.design.otm_bcfile.create_bc_file
	~ocmw.design.otm_bcfile.msh2cli


.. automodule:: ocmw.design.otm_bcfile
   :members:
   :undoc-members:
   :show-inheritance:
