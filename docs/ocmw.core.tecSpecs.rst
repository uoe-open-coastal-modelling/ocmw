.. _core.tecSpecs:

ocmw.core.tecSpecs module
=========================

Tidal energy converter (TEC) parameters used to estimate power production based on modelled and measured flow.
The intent is that these functions will be replaced with a database and query methods.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.tecSpecs.tidalTurbine
	~ocmw.core.tecSpecs.getTec
	~ocmw.core.tecSpecs.tecD10
	~ocmw.core.tecSpecs.tecDeepGenIV
	~ocmw.core.tecSpecs.tecHS1000
	~ocmw.core.tecSpecs.tecAR1500
	~ocmw.core.tecSpecs.tecAR2000
	~ocmw.core.tecSpecs.tecOrbitalO2
	~ocmw.core.tecSpecs.tecNova100D


.. automodule:: ocmw.core.tecSpecs
   :members:
   :undoc-members:
   :show-inheritance:
