.. _core.spectral:

ocmw.core.spectral module
=========================

Basic timeseries spectral analysis functions.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.spectral.signal_from_fft
	~ocmw.core.spectral.nextpow2
	~ocmw.core.spectral.ssas_fft


.. automodule:: ocmw.core.spectral
   :members:
   :undoc-members:
   :show-inheritance:
