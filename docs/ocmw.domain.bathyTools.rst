.. _domain.bathyTools:

ocmw.domain.bathyTools module
=============================

Bathymetry manipulation tools.

.. autosummary::
	:nosignatures:
	
	~ocmw.domain.bathyTools.load_bathy
	~ocmw.domain.bathyTools.interp_bathy


.. automodule:: ocmw.domain.bathyTools
   :members:
   :undoc-members:
   :show-inheritance:
