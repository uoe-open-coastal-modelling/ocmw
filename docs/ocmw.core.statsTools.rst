.. _core.statsTools:

ocmw.core.statsTools module
===========================

A set of functions for generating validation statistics based on timeseries of modelled and observed values.

The ``circ_`` functions apply the metrics to circular parameters, e.g. heading or wave direction.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.statsTools.corr_coef
	~ocmw.core.statsTools.bias
	~ocmw.core.statsTools.mean_bias
	~ocmw.core.statsTools.norm_mean_bias
	~ocmw.core.statsTools.mean_norm_bias
	~ocmw.core.statsTools.mean_absolute_error
	~ocmw.core.statsTools.norm_ma_error
	~ocmw.core.statsTools.rms_error
	~ocmw.core.statsTools.norm_rmse
	~ocmw.core.statsTools.goodness_of_fit
	~ocmw.core.statsTools.scatter_index
	~ocmw.core.statsTools.reliability_index
	~ocmw.core.statsTools.skill_score
	~ocmw.core.statsTools.model_efficiency
	~ocmw.core.statsTools.circ_mod_pi
	~ocmw.core.statsTools.circ_mean
	~ocmw.core.statsTools.circ_std
	~ocmw.core.statsTools.circ_corr
	~ocmw.core.statsTools.circ_mean_bias
	~ocmw.core.statsTools.circ_norm_mean_bias
	~ocmw.core.statsTools.circ_mean_abs_error
	~ocmw.core.statsTools.circ_norm_ma_error
	~ocmw.core.statsTools.circ_rms_error
	~ocmw.core.statsTools.circ_norm_rmse
	~ocmw.core.statsTools.circ_goodness_of_fit
	~ocmw.core.statsTools.circ_scatter_index
	~ocmw.core.statsTools.stats_metrics_circular
	~ocmw.core.statsTools.stats_metrics
	~ocmw.core.statsTools.initialize_results
	~ocmw.core.statsTools.store_results
	~ocmw.core.statsTools.save_results
	~ocmw.core.statsTools.display_metrics
	~ocmw.core.statsTools.display_results_metrics


.. automodule:: ocmw.core.statsTools
   :members:
   :undoc-members:
   :show-inheritance:
