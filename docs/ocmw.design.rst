.. _design:

ocmw.design module
==================

.. automodule:: ocmw.design
   :members:
   :undoc-members:
   :show-inheritance:

Tools for manipulating and converting unstructured triangular meshes.

.. autosummary::
	:nosignatures:
	
	~ocmw.design.gmshTools
	~ocmw.design.meshConvert
	~ocmw.design.otm_bcfile
	~ocmw.design.gradRaster
    ~ocmw.design.osuTools

Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.design.gmshTools
   ocmw.design.meshConvert
   ocmw.design.otm_bcfile
   ocmw.design.gradRaster
   ocmw.design.osuTools
