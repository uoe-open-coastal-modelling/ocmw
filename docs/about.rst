.. _about:

.. _Licence-BSD-3.txt: https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/blob/main/Licence-BSD-3.txt
.. _OCMW: https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw

About
-----

`OCMW`_ is a library of tools collates and consolidates our workflow for coastal model 
development, post-processing, and analysis.


License
^^^^^^^
`OCMW`_ is released under a BSD-3 licence (see the `Licence-BSD-3.txt`_ file in the
repository).

