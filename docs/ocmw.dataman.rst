.. _dataman:

ocmw.dataman module
===================

.. automodule:: ocmw.dataman
   :members:
   :undoc-members:
   :show-inheritance:

A set of modules with functions used to manage both modelled and observed data.
The aim is to standdardise data formats with a view to linking to advanced database systems.
The WW3 matchup database tools were developed for the validation of the ResourceCode 20 year 
hindcasty model against in situ wavebuoy data.
Some of the database methods will be ported to more general tools in the future.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataman.dataReaders
	~ocmw.dataman.adcp2netcdf
	~ocmw.dataman.ww3WavesMatchUpDatabase


Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.dataman.dataReaders
   ocmw.dataman.adcp2netcdf
   ocmw.dataman.ww3WavesMatchUpDatabase
