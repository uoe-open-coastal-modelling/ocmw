.. _core.physics:

ocmw.core.physics module
========================

Functions for calculating physical quantities.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.physics.pressure2depth
	~ocmw.core.physics.g_wgs84
	~ocmw.core.physics.circulation2D
	~ocmw.core.physics.kinetic_energy_density_2D
	~ocmw.core.physics.potential_energy_density_2D
	~ocmw.core.physics.frictionCoeff
	~ocmw.core.physics.chezy
	~ocmw.core.physics.manning
	~ocmw.core.physics.strickler
	~ocmw.core.physics.nikuradse


.. automodule:: ocmw.core.physics
   :members:
   :undoc-members:
   :show-inheritance:
