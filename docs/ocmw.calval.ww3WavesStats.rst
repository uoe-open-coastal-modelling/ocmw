_ ..calval.ww3WaveStats:

ocmw.calval.ww3WavesStats module
================================

Tools for post-processing the validation statistics generated by the ``ww3WavesValidation`` module.

.. autosummary::
	:nosignatures:
	
	~ocmw.calval.ww3WavesStats.get_metric_stats_global
	~ocmw.calval.ww3WavesStats.get_parameter_stats_global
	~ocmw.calval.ww3WavesStats.get_metric_stats_by_year
	~ocmw.calval.ww3WavesStats.get_parameter_stats_by_year
	~ocmw.calval.ww3WavesStats.get_metric_stats_by_buoy
	~ocmw.calval.ww3WavesStats.get_parameter_stats_by_buoy
	~ocmw.calval.ww3WavesStats.test_for_failure
	~ocmw.calval.ww3WavesStats.get_buoy_locations


.. automodule:: ocmw.calval.ww3WavesStats
   :members:
   :undoc-members:
   :show-inheritance:
