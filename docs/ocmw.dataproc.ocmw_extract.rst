.. _dataproc.ocmwextract:

ocmw.dataproc.ocmw_extract module
=================================

Functions to extract timeseries of data at specific locations in the water column from model data location extracts.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.ocmw_extract.extract_ts_at_hub
	~ocmw.dataproc.ocmw_extract.extract_depthavg_ts
	~ocmw.dataproc.ocmw_extract.extract_rotav_ts_at_hub
	~ocmw.dataproc.ocmw_extract.extract_model_ts_at_surface
	~ocmw.dataproc.ocmw_extract.load_ocmw_file
	~ocmw.dataproc.ocmw_extract.save_ocmw_file


.. automodule:: ocmw.dataproc.ocmw_extract
   :members:
   :undoc-members:
   :show-inheritance:
