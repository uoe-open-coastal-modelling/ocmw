.. _core.tidal:

ocmw.core.tidal module
======================

Functions for applying tidal analysis to timeseries of tidal flow parameters.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.tidal.harmonicReduction
	~ocmw.core.tidal.extractHarmValues
	~ocmw.core.tidal.saveTide
	~ocmw.core.tidal.signalDecomposition
	~ocmw.core.tidal.get_slack_water_times
	~ocmw.core.tidal.get_tidal_range
	~ocmw.core.tidal.print_tide_times


.. automodule:: ocmw.core.tidal
   :members:
   :undoc-members:
   :show-inheritance:
