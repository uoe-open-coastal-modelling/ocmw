.. _examples:

Example processing scripts
==========================

A set of example python scripts are included to demonstrate how the various modules within the OCWM tools can be used to analyse model data, visualise model data, validate model data against in situ measurements, and convert ADCP data to the standardised netCDF file format and from netCDF to OCMW format.
These python scripts all require access to the OCMW tools, so must be run from an environment where the tools have been installed.
The example scripts were used for the analysis of an Open Telemac-Mascaret model of the Sound of Islay (soi), but can be used for any model by altering the data paths, input and output file names, etc. 
The analysis and validation processes assume that a timeseries of model data have been extracted for a specific spatial location using the :ref:`otm_extract_by_location.py <extrctbyloc>` application, and the ADCP data have been converted to the internal OCMW data format using the ``adcp2ocmw`` function in the :ref:`processADCPData <dataproc.processADPCData>` module.

The example processing and analysis scripts provided are summarised in :numref:`examplisttab`.

.. _examplisttab:
.. csv-table:: : Example processing and analysis scripts.
               :header-rows: 1
               :widths: 45, 110
               :file: ./examples_list.csv



.. _exampflowchar:

All of the python scripts take the standard form::

   #!/usr/bin/env python3
   # -*- coding: utf-8 -*-
   """
   'Description of script'
   """
   
   # ----------------------------------------------------------------------------
   #   IMPORTS
   # ----------------------------------------------------------------------------
   'Dependent package imports sorted by type'
   # Standard Python Dependencies
   # Non-Standard Python Dependencies
   # Local Module Dependencies
   # Other Dependencies
   
   # ----------------------------------------------------------------------------
   #   GLOBAL VARIABLES
   # ----------------------------------------------------------------------------
   'Global variable definitions - if used'
   
   # ----------------------------------------------------------------------------
   #   CLASS DEFINITIONS
   # ----------------------------------------------------------------------------
   'Internal class definitions - if used'
   
   # ----------------------------------------------------------------------------
   #   FUNCTION DEFINITIONS
   # ----------------------------------------------------------------------------
   'Internal function definitions - if used'
   
   # ----------------------------------------------------------------------------
   #   MAIN PROCESS
   # ----------------------------------------------------------------------------
   'Main processing code'
   
   #--------------------------------------------------------------------------
   # INTERFACE
   #--------------------------------------------------------------------------
   'Code to allow command line input via a call to "__main__" - if used'
   
.. _input: https://docs.python.org/3/library/functions.html#input

The shebang line at the head of the file allows the script to be run direclty from the command line. 
Scripts that generate graphical output include a python `input`_ request to allow the figures to be seen when the script is run from the command line.


Flow Characterisation
---------------------

The flow characterisation process takes a timeseries of data for a specified height above the seabea from a set of velocity profiles extracted for a single spatial location.
The flow characterisation can be tied to a particular turbine design by extracting the data using the :ref:`hub_height_data <dataproc.processModelData>` function, or to a user-defined height above bed by using the :ref:`hab_data <dataproc.processModelData>` function. 
When calculated for a turbine, the generated characterisation plot includes information on the turbine cut-in and rated speeds.

The velocity data are preprocessed by sorting into ebb and flood phases, then the peak flow directions determined for each phase, flow asymmetry is defined, the percentage of time in the ebb and flood phases, and if the characterisations is tied to a turbine the percentage of time below cut-in, between cut-in and rated, and above rated speeds for both tide phases.
These data are collated into a python dictionary structure along with timestamps and metadata describing the source data.

If a turbine is specified, then the power-weighted-rotor-averaged (PWRA) velocity is calculated from the pre-processed data and a separate python dictionary generated.

The function :ref:`hub_height_data <dataproc.processModelData>` returns three dictionaries: (1) the extracted timeseries, (2) the pre-processed timeseries, and (3) the PWRA timeseries. 
The function :ref:`hab_data <dataproc.processModelData>` only returns two dictionaries: (1) the extracted timeseries, and (2) the pre-processed timeseries.
The data dictionaries are automatically saved to the ``ANALYSIS`` directory under the model data root directory. If the ``ANALYSIS`` directory does not exist, it is created.

The pre-processed data are passed to the graphics functions :ref:`flow_class_diagram <core.graphics>`, :ref:`speed_bin_histogram <core.graphics>`, and :ref:`directional_spread_plot <core.graphics>` to present the characterisation in pictorial form.

The script shown in :numref:`soiflowcharlisting` is for a Sound of Islay model where the characterisation is carried out for two spearate locations and for a Nova 100D tidal turbine.

.. _soiflowcharlisting:
.. literalinclude:: ../examples/soi_flowChar.py
  :linenos:
  :caption: soi_flowChar.py



.. _examptidalenergy:

Tidal Energy
------------
The estimation of energy yield is a key component of site assessment prior to site development. The yield will depend on the type of tidal turbine deployed and the spatial location of the turbine. Turbine energy yield is calculated from the PWRA velocity for the given turbine, the rotor area, the seawater density, and the turbines power coefficient. 
The code currently assumes a constant power coefficient, *i.e.* the coefficient is independent of flow speed or turbine controls. 

The script shown in :numref:`soitidalenergylisting` is for a Sound of Islay model where the power generated by a Nova 100D turbine is estimated for two separate locations and the predicted yields compared. 
For the purpose of this example, it has been assumed that the turbine is always oriented along the direction of peak flow, *i.e.* the turbine yaws.
The power has also been calculated using the depth-averaged velocities to show the discrepancies that would arise from using a 2D depth-averaged model for site assessment. 

There are two internal functions (``get_soi_hubht_pwra_data``, ``get_soi_depavg_pwr_data``) used to determine where the PWRA data have already been extracted. 
If the PWRA data exist then the file is loaded, otherwise the data are generated prior to the analysis.

.. _soitidalenergylisting:
.. literalinclude:: ../examples/soi_Nova100D_tidalEnergy.py
  :linenos:
  :caption: soi_Nova100D_tidalEnergy.py


.. _examptidechart:

Tide Charts
-----------
Prediction of tidal information is important for operations planning in tehmarine environment. Free-surface hydrodynamic model generate both the 2D or 3D velocity fields and a 2D surface elevation field. From these data tidal information can be generated for specific spatial locations. Tidal charts typically present data on a daily basis, this has been followed in both the model runs and the generation of tidal charts.

The script shown in :numref:`soitidechartlisting` generated tide plots and summaries of the high and low water times and ranges, and the times of slack water for two separate locations based on the surface elevation data and surface and depth averaged velocity data from a Sound of Islay model. 
The data are calculated and presented for a single day of model run predictions. 
There is no dedicated graphics function for plotting tide charts, the script includes the plotting function. The charts for both locations are presented in a single plot. 
The speeds have been converted to knots and the a pair of upper and lower operting speed bands are included to support operations planning.


.. _soitidechartlisting:
.. literalinclude:: ../examples/soi_tideCharts.py
  :linenos:
  :caption: soi_tideCharts.py

.. _examptidalanal:

Tidal Analysis
--------------
Tidal reduction to a set of harmonics is a standard analysis applied to tidal data. This allows a time series to be represented in a spectral form, which in turn can be used to predict tides at other times. The accuracy of the predicted tides depends on the length of the original timeseries used in the harmonic reduction. The OCMW tools includes a :ref:1tidal <core.tidal>` module that uses `UTide <https://pypi.org/project/UTide/>`_ to apply harmonic reduction to timeseries of tidal data. The generation of 2D maps of tidal amplitude and phase for key harmonics requires the application of harmonic reduction to every model node. 

The script shown in :numref:`soi2dtideharmslisting` demonstrates how tidal reduction can be applied to a multi-day timeseries of surface elevation data. The data generated can be used to map 2D phase and amplitude fields.

.. _soi2dtideharms:
.. literalinclude:: ../examples/soi_free_surface_tidal_analysis.py
  :linenos:
  :caption: soi_free_surface_tidal_analysis.py

.. _exampfieldmaps:

Map 2D model fields
-------------------
Regional models provide detailed spatial information that can be used to generate 2D maps to visualise spatial variability. The model mesh definition is required to generate 2D maps, and to produce a smooth colourisation the node data should be interpolated onto the element centroids and the centroid value used to colour fill the element. The :ref:`otm_extraction <dataproc.otm_extraction>` module includes functions to extract mesh data from a Selafin 2D output file or input geometry file, and the option to extend the mesh data which include the calculation of the element node centroid weighting factors for interpolating node data onto element centroids.

The script shown in :numref:`soi2dmapslisting` gives an example of how to extract data from a 2D output Sealfin file and generate 2D maps of the bathymetry, velocity magnitude, and 2D vorticity field for a single model time step. The base plotting function ``plotMeshField`` in the :ref:`graphics <core.graphics>` module. This returns handles to the figure, axes and colour bar objects so that the figure can be manually modified.

.. _soi2dmapslisting:
.. literalinclude:: ../examples/soi_map_2d_fields.py
  :linenos:
  :caption: soi_map_2d_fields

.. _exampmodelval:

Model Validation Against ADCP Data
----------------------------------
The OCMW tools include a module for :ref:`validataion <calval.flowValidation>` of model velocity and elevation data against *in situ* ADCP data . The input data are co-located and contemporaneous timeseries of model and *in situ* dat, as generated using the :ref:`otm_extract_by_location.py <extrctbyloc>` application and the :ref:`processModelData <dataproc.processModelData>` module functions. The observation data are time interpoltated onto the model data timestamps, then a set of standard validatiaon statistics are calculated and saved to the ``ANALYSIS`` directory, and summarised in a graphic including a correlation plot. 

The script shown in :numref:`fowflowvallisting` presents an example of validation of a Falls of warness, Orkneys model against ReDAPT ADCP data. in this example the PWRA velocity for a DeepGen IV turbine are used as the validation input data. These are calculated from both the model and the ADCP data. Validation statistics are calculated for the velocity magnitude, and graphics displayed.

.. _fowflowvallisting:
.. literalinclude:: ../examples/fow_model_flow_validation.py
  :linenos:
  :caption: fow_model_flow_validation.py

.. _exampdataconvert:

Convert ADCP data to netCDF
---------------------------
For integration with the OCMW tools and data archiving, raw ADCP data are converted into a standardised netCDF file format. The data are sorted into daily files to produce managable data fragments and to simplify data processing and archiving. The tools currrently support RDI Workhorse and Nortek Signature instruments. There are tools in-place to read Sonardyne Origin data, at some point conversion to netCDF will be included.

The script shown in :numref:`sig2netcdflisting` shows the conversion of Nortek Signature ADCP data into daily netCDF files. The standardised netCDF files store depoyment and processing metadata in the global attributes for the netCDF file. The global attributes can be used to catalogue the files for databasing. Some of the metadata needs to be manually added, this done with the internal function ``populateMetadata``. The remaining fields are populated using data from the raw ADCP files. A list of raw files is generated and sorted, then sequentially processed to extract the data into daily netCDF files. The functions for doing this are in the :ref:`adcp2netcdf <dataman.adcp2netcdf>` module.

.. _sig2netcdflisting:
.. literalinclude:: ../examples/convert_sig_to_netcdf.py
  :linenos:
  :caption: convert_sig_to_netcdf

