.. _core.geometry:

ocmw.core.geometry module
=========================

Basic geometry tools used in the OCMW processes.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.geometry.pol2cart
	~ocmw.core.geometry.cart2pol
	~ocmw.core.geometry.pol2cmplx
	~ocmw.core.geometry.cmplx2pol
	~ocmw.core.geometry.cart2cmplx
	~ocmw.core.geometry.cmplx2cart
	~ocmw.core.geometry.rotateVectorField
	~ocmw.core.geometry.unitVect
	~ocmw.core.geometry.circSign2D
	~ocmw.core.geometry.spatialCoverage


.. automodule:: ocmw.core.geometry
   :members:
   :undoc-members:
   :show-inheritance:
