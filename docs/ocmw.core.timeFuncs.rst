.. _core.timeFuncs:

ocmw.core.timeFuncs module
==========================

Basic time variable manipulation functions. 

The format conversion functions take a single input timestamp value and convert to the requested format.
To convert an array of values dtvals, use an embedded for loop around the function.
For example to convert a list of date strings (dateStrList[])to a python datetime object use:

	>>> dt = [datetimeFromStr(ds) for ds in dateStrList]

.. autosummary::
	:nosignatures:
	
	~ocmw.core.timeFuncs.datetimeFromStr
	~ocmw.core.timeFuncs.datetimeFromNum
	~ocmw.core.timeFuncs.dateStrFromDatetime
	~ocmw.core.timeFuncs.dateStrFromDateNum
	~ocmw.core.timeFuncs.dateNumFromDateStr
	~ocmw.core.timeFuncs.dateNumFromEpoch
	~ocmw.core.timeFuncs.python2matlab
	~ocmw.core.timeFuncs.matlab2python
	~ocmw.core.timeFuncs.temporalCoverage


.. automodule:: ocmw.core.timeFuncs
   :members:
   :show-inheritance:
