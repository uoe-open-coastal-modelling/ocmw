.. _core.waveSpectra:

ocmw.core.waveSpectra module
============================

Functions for generating 1D and 2D wave spectra from parameters, modelled or observed data.
Functions for calculating integrated wave parameters from modelled or observed wave data.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.waveSpectra.pierson_moskowitz
	~ocmw.core.waveSpectra.jonswap
	~ocmw.core.waveSpectra.dsfunc1d
	~ocmw.core.waveSpectra.significant_wave_height
	~ocmw.core.waveSpectra.mean_wave_period
	~ocmw.core.waveSpectra.zero_crossing_period
	~ocmw.core.waveSpectra.wave_energy_period
	~ocmw.core.waveSpectra.spectral_period
	~ocmw.core.waveSpectra.wave_steepness
	~ocmw.core.waveSpectra.spectral_width
	~ocmw.core.waveSpectra.spectral_narrowness
	~ocmw.core.waveSpectra.spectralMoment
	~ocmw.core.waveSpectra.getSpectralMoments
	~ocmw.core.waveSpectra.getSpectralParameters
	~ocmw.core.waveSpectra.wavehgtvar
	~ocmw.core.waveSpectra.dsfuncnorm
	~ocmw.core.waveSpectra.spec2dnorm
	~ocmw.core.waveSpectra.dsfunc
	~ocmw.core.waveSpectra.spec2d
	~ocmw.core.waveSpectra.spec2dparam


.. automodule:: ocmw.core.waveSpectra
   :members:
   :undoc-members:
   :show-inheritance:
