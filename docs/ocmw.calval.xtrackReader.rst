.. _calval.xtrackReader:

ocmw.calval.xtrackReader module
===============================

Tools for loading XTrack harmonic data and identifying points that lie 
within a model domain for surface elevation Cal/Val.

.. autosummary::
	:nosignatures:
	
	~ocmw.calval.xtrackReader.isInDomain
	~ocmw.calval.xtrackReader.isInPolygon
	~ocmw.calval.xtrackReader.latlon2utm
	~ocmw.calval.xtrackReader.readConstitName
	~ocmw.calval.xtrackReader.extractXTrackValues
	~ocmw.calval.xtrackReader.loadVars
	~ocmw.calval.xtrackReader.getPointsInDomain
	~ocmw.calval.xtrackReader.getPointsInPolygon


.. automodule:: ocmw.calval.xtrackReader
   :members:
   :undoc-members:
   :show-inheritance:
