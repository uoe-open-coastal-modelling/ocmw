.. _core.similitude:

ocmw.core.similitude module
===========================

A set of basic non-dimensional parameters used for flow classification.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.similitude.courantNumber
	~ocmw.core.similitude.froudeNumber
	~ocmw.core.similitude.ekmanNumber
	~ocmw.core.similitude.rossbyNumber


.. automodule:: ocmw.core.similitude
   :members:
   :undoc-members:
   :show-inheritance:
