.. _core.meshTools:

ocmw.core.meshTools module
============================

Functions for the manipulation and interpolation of data on unstructured triangular meshes.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.meshTools.getBarycentricWeights
	~ocmw.core.meshTools.getTriangleArea
	~ocmw.core.meshTools.getTriangleCentroid
	~ocmw.core.meshTools.isInsideTriangle
	~ocmw.core.meshTools.interpTriangle
	~ocmw.core.meshTools.getElemAroundLoc
	~ocmw.core.meshTools.getElemNodes
	~ocmw.core.meshTools.getNearestNode
	~ocmw.core.meshTools.getNodesAroundNode
	~ocmw.core.meshTools.weightsMatrix
	~ocmw.core.meshTools.getCentroidWeights
	~ocmw.core.meshTools.getNodesWeights
	~ocmw.core.meshTools.gradientAtNode
	~ocmw.core.meshTools.meshDensity

.. automodule:: ocmw.core.meshTools
   :members:
   :undoc-members:
   :show-inheritance:
