.. _dataman.ww3WavesMDB:

ocmw.dataman.ww3WavesMatchUpDatabase module
===========================================

Tools for generating an SQLite matchup database from a set of model extracts 
and an archive of wavebuoy data.

These tools are tied to the ResourceCode WaveWatch III model hindcast data, 
but the undelying functions for opening, populating and querying and SQLite 
dataase are generic. 


.. autosummary::
	:nosignatures:
	
	~ocmw.dataman.ww3WavesMatchUpDatabase.db_match_to_csv
	~ocmw.dataman.ww3WavesMatchUpDatabase.create_connection
	~ocmw.dataman.ww3WavesMatchUpDatabase.close_database
	~ocmw.dataman.ww3WavesMatchUpDatabase.create_rscd_table
	~ocmw.dataman.ww3WavesMatchUpDatabase.create_buoy_table
	~ocmw.dataman.ww3WavesMatchUpDatabase.insert_rscd_records
	~ocmw.dataman.ww3WavesMatchUpDatabase.insert_buoy_records
	~ocmw.dataman.ww3WavesMatchUpDatabase.get_match_records
	~ocmw.dataman.ww3WavesMatchUpDatabase.insert_match_record
	~ocmw.dataman.ww3WavesMatchUpDatabase.load_rscd_archive
	~ocmw.dataman.ww3WavesMatchUpDatabase.load_buoy_archive
	~ocmw.dataman.ww3WavesMatchUpDatabase.construct_rscd_mdb


.. automodule:: ocmw.dataman.ww3WavesMatchUpDatabase
   :members:
   :show-inheritance:
