.. _dataproc.otmvectorfield:

ocmw.dataproc.otm_vectorfield module
====================================

Functions for extracting velocity, velocity gradient, and vorticity 2D fields from Open Telemac-Mascret model output.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.otm_vectorfield.initialiseVectorField
	~ocmw.dataproc.otm_vectorfield.getVariableAtNodes
	~ocmw.dataproc.otm_vectorfield.getVariablesAtTime
	~ocmw.dataproc.otm_vectorfield.vectorFieldAtLocation
	~ocmw.dataproc.otm_vectorfield.characteriseAtNode
	~ocmw.dataproc.otm_vectorfield.getVelocityField
	~ocmw.dataproc.otm_vectorfield.frameSetIndices
	~ocmw.dataproc.otm_vectorfield.getCirculationField
	~ocmw.dataproc.otm_vectorfield.mergeCirculationFrames
	~ocmw.dataproc.otm_vectorfield.processNodeSet
	~ocmw.dataproc.otm_vectorfield.processNodesInPolygon
	~ocmw.dataproc.otm_vectorfield.processLocation


.. automodule:: ocmw.dataproc.otm_vectorfield
   :members:
   :undoc-members:
   :show-inheritance:
