.. _dataproc.processModelData:

ocmw.dataproc.processModelData module
=====================================

A set of functions that run a series of processes defined in ``ocmw_extract`` to apply standard post-processing to model extract data.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.processModelData.hub_height_data
	~ocmw.dataproc.processModelData.hab_data
	~ocmw.dataproc.processModelData.dbs_data
	~ocmw.dataproc.processModelData.dav_data
	~ocmw.dataproc.processModelData.srf_data
	~ocmw.dataproc.processModelData.merge_model_daily_files
	~ocmw.dataproc.processModelData.merge_model_daily_poly_files


.. automodule:: ocmw.dataproc.processModelData
   :members:
   :undoc-members:
   :show-inheritance:
