.. _calval:

ocmw.calval module
==================

.. automodule:: ocmw.calval
   :members:
   :undoc-members:
   :show-inheritance:

Data processing tools supporting Cal/Val assessments of model data against in situ and remote observations.

The WW3 tools are design around the validation of the ResourceCode hindcast model against the CMEMS archive of wavebuoy data.

The ``xtrackReader`` functions are used to apply Cal/Val to the model surface elevation in open shelf waters.


.. autosummary::
	:nosignatures:
	
	~ocmw.calval.flowValidation
	~ocmw.calval.ww3WavesStats
	~ocmw.calval.ww3WavesValidation
	~ocmw.calval.xtrackReader


Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.calval.flowValidation
   ocmw.calval.ww3WavesStats
   ocmw.calval.ww3WavesValidation
   ocmw.calval.xtrackReader
