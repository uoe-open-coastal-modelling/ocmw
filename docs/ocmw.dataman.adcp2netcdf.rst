.. _dataman.adcp2netcdf:

ocmw.dataman.adcp2netcdf module
===============================

Tools for standardizing the conversion of ADCP data to netCDF4 format for archiving and analysis.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataman.adcp2netcdf.metaDataObj
	~ocmw.dataman.adcp2netcdf.create_adcp_netcdf
	~ocmw.dataman.adcp2netcdf.add_adcp_array_fields
	~ocmw.dataman.adcp2netcdf.build_adcp_netcdf
	~ocmw.dataman.adcp2netcdf.populate_sig_core
	~ocmw.dataman.adcp2netcdf.populate_sig_fields
	~ocmw.dataman.adcp2netcdf.populate_adcp_core
	~ocmw.dataman.adcp2netcdf.populate_adcp_fields



.. automodule:: ocmw.dataman.adcp2netcdf
   :members:
   :undoc-members:
   :show-inheritance:
