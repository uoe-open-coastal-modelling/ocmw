.. _core:

ocmw.core module
================

.. automodule:: ocmw.core
   :members:
   :undoc-members:
   :show-inheritance:

Core functions used to build the OCWM process tools.

These functions represent the generic level 0 tools from which the level 1 OCMW specific tools are constructed.

.. autosummary::
	:nosignatures:
	
	~ocmw.core.timeFuncs
	~ocmw.core.fileTools
	~ocmw.core.geometry
	~ocmw.core.in_poly_funcs
	~ocmw.core.meshTools
	~ocmw.core.physics
	~ocmw.core.similitude
	~ocmw.core.spectral
	~ocmw.core.waveSpectra
	~ocmw.core.tidal
	~ocmw.core.statsTools
	~ocmw.core.tecSpecs
	~ocmw.core.flowChar
	~ocmw.core.graphics


Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.core.timeFuncs
   ocmw.core.fileTools
   ocmw.core.geometry
   ocmw.core.in_poly_funcs
   ocmw.core.meshTools
   ocmw.core.physics
   ocmw.core.similitude
   ocmw.core.spectral
   ocmw.core.waveSpectra
   ocmw.core.tidal
   ocmw.core.statsTools
   ocmw.core.tecSpecs
   ocmw.core.flowChar
   ocmw.core.graphics
