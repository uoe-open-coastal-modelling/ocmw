.. _ocmwdocs:

OCMW documentation
==================
.. figure:: ../images/OCMW_workflow_diagram_cropped.png
   :align: center
   :scale: 70%
   :alt: OCMW workflow diagram.

.. _OCMW: https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw

   
`OCMW`_ is a library of Open Coastal Modelling Workflow tools written in python.

This library collates and consolidates regional coastal modelling methods 
and tools developed across a number projects focused on the characterisation of 
high-energy tidal sites for tidal-stream energy extraction assessment.

   
Table of Contents
#################

.. toctree::
   
   about
   install
   overview
   applications
   examples
   datasets



OCMW Package Content
####################

.. toctree::
   :maxdepth: 2

   ocmwpkg


Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
