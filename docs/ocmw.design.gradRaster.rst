.. _design.gradRaster:

ocmw.design.gradRaster module
==============================

Mesh gradation raster generation functions for parameter-based mesh refinement.

.. autosummary::
	:nosignatures:
	
	~ocmw.design.gradRaster.writeRasterFile
	~ocmw.design.gradRaster.getRasterCoords
	~ocmw.design.gradRaster.normalise
	~ocmw.design.gradRaster.gradate
	~ocmw.design.gradRaster.gradation2meshDensity
	~ocmw.design.gradRaster.gradation2numElems
	~ocmw.design.gradRaster.distance2coast
	~ocmw.design.gradRaster.rasterise
	~ocmw.design.gradRaster.vargradient
	~ocmw.design.gradRaster.plotRaster


.. automodule:: ocmw.design.gradRaster
   :members:
   :undoc-members:
   :show-inheritance:
