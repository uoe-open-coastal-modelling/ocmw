.. _dataproc.otmextraction:

ocmw.dataproc.otm_extraction module
===================================

Open Telemac-Mascaret data extraction functions.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.otm_extraction.InitialiseFile
	~ocmw.dataproc.otm_extraction.OTMDate2Num
	~ocmw.dataproc.otm_extraction.OTMDate2Str
	~ocmw.dataproc.otm_extraction.genOTMTimeStamps
	~ocmw.dataproc.otm_extraction.getSlfTriangle
	~ocmw.dataproc.otm_extraction.getSlfNodesWeights
	~ocmw.dataproc.otm_extraction.getMeshTriangle
	~ocmw.dataproc.otm_extraction.getMeshNodesWeights

Functions for saving extracted model data into standard OCMW file format.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.generateOutputFileStr
	~ocmw.dataproc.otm_extraction.save2ocmw
	~ocmw.dataproc.otm_extraction.saveLoc2ocmw
	~ocmw.dataproc.otm_extraction.saveNodes2ocmw

Functions for extracting the model mesh into a standard OCMW format.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.enhanceMesh
	~ocmw.dataproc.otm_extraction.extractMesh
	~ocmw.dataproc.otm_extraction.extractGeometry

Functions for extracting 2D fields from multiple model output files.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.get2DElevation
	~ocmw.dataproc.otm_extraction.getMultiFileLocElevation
	~ocmw.dataproc.otm_extraction.extractElevationFrames
	~ocmw.dataproc.otm_extraction.extractMultiFile2DField

Functions for extracting data at set of geospatial locations.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.getVariableData
	~ocmw.dataproc.otm_extraction.getLoc2DVars
	~ocmw.dataproc.otm_extraction.getLoc3DVars
	~ocmw.dataproc.otm_extraction.getLocTS
	~ocmw.dataproc.otm_extraction.extract2DLocData
	~ocmw.dataproc.otm_extraction.extract3DLocData
	~ocmw.dataproc.otm_extraction.processLocsDataFile

Functions for extracting data at points along a linear transect.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.getTransectPoints
	~ocmw.dataproc.otm_extraction.extractTransect

Functions for extracting data at a set of model nodes.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.getVarTimeSeries
	~ocmw.dataproc.otm_extraction.getNodesTS
	~ocmw.dataproc.otm_extraction.extractNodeTSData
	~ocmw.dataproc.otm_extraction.processNodeDataFile

Functions for extracting data for nodes within a polygon

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.getMeshNodeLocs
	~ocmw.dataproc.otm_extraction.processPolygonDataFile

Parsers for user supplied ascii run file used by ``processLocDataFile``, ``processNodesDataFile``, and ``processPolygonDataFile``.

.. autosummary::
	:nosignatures:

	~ocmw.dataproc.otm_extraction.parseLocsRunFile
	~ocmw.dataproc.otm_extraction.parseNodesRunFile
	~ocmw.dataproc.otm_extraction.parseTransectRunFile
	~ocmw.dataproc.otm_extraction.parsePolygonRunFile


.. automodule:: ocmw.dataproc.otm_extraction
   :members:
   :undoc-members:
   :show-inheritance:
