.. _dataman.dataReaders:

ocmw.dataman.dataReaders module
===============================

These functions are aimed at simplifying the interrogation of NetCDF4 files.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataman.dataReaders.netcdfGeneric
	~ocmw.dataman.dataReaders.ww3
	~ocmw.dataman.dataReaders.ww3Mesh
	~ocmw.dataman.dataReaders.ssw_rs
	~ocmw.dataman.dataReaders.sswMesh
	~ocmw.dataman.dataReaders.wavebuoy
	~ocmw.dataman.dataReaders.emecwb
	~ocmw.dataman.dataReaders.drifter


.. automodule:: ocmw.dataman.dataReaders
   :members:
   :undoc-members:
   :show-inheritance:

