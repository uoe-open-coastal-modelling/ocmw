.. _design.osuTools:

ocmw.design.osuTools module
==============================

Class objects for manipulating OSU tidal atlas data.

.. autosummary::
	:nosignatures:
	
	~ocmw.design.osuTools.osuTidesObj


.. automodule:: ocmw.design.osuTools
   :members:
   :undoc-members:
   :show-inheritance:
