.. _dataproc.processADCPData:

ocmw.dataproc.processADCPData module
====================================

Tools for converting ADCP data into OCMW standard format for OCMW post-processing.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.processADCPData.adcp_obj
	~ocmw.dataproc.processADCPData.saveADCP2ocmw
	~ocmw.dataproc.processADCPData.adcp2ocmw
	~ocmw.dataproc.processADCPData.merge_adcp_daily_files


.. automodule:: ocmw.dataproc.processADCPData
   :members:
   :undoc-members:
   :show-inheritance:
