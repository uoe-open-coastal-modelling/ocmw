.. _datasets:

.. _UK Hydrographic Office: https://data.admiralty.co.uk/portal/apps/sites/#/marine-data-portal/pages/seabed-mapping-services
.. _European Environment Agency: https://www.eea.europa.eu/data-and-maps/data/eea-coastline-for-analysis-1/gis-data/europe-coastline-shapefile
.. _SOEST - University of Hawaii: https://www.soest.hawaii.edu/pwessel/gshhg/
.. _OpenStreetMap: https://osmdata.openstreetmap.de/data/coastlines.html


Open Access Datasets
====================

Coastline
---------

There are a variety of open access coastline data available, some standard datasets are given in :numref:`cstlndatatab`.

.. _cstlndatatab:
.. csv-table:: : Open-access coastline datasets.
               :header-rows: 1
               :widths: 55, 55
               :file: ./coastline_datasets.csv
               
               
Bathymetry
----------

.. _UK Renewables Atlas: https://www.renewables-atlas.info/

Bathymetry data is typically provided as local depths for the lowest astronomical (LAT) tide.
For modelling purposes, these need to be corrected to mean sea level (MSL).
The `UK Renewables Atlas`_ provides tidal range data for the UK shelf, North Sea and coastal waters.
These can be used to either select a regional average correction or can be mapped to produce a more accurate correction across the model domain.
Alternatively the model can be run for the largest spring-tide period and the modelled tidal range can be used to correct the bathymetry data.

The UKHO provides open acess high resolution bathymetry data at 2m and 4m resolution for specific regions. 
These data are available as \*.CSV and \*.BAG files.
The \*.CSV data should be used to generate the bathymetry shapefiles, and the \*.BAG files can be loaded into GIS package as a raster for visualisation purposes.

Depending on the extent of the regional model the CRS will either be spherical (e.g. WGS84 Lat/Lon) or a flat Universal Transverse Mercator (UTM) projection (e.g. UTM Zone 30N).
Conversion between CRS can be done using a GIS package, GDAL software, or using python packages (e.g. shapely, geopandas, fiona, etc.).


.. _GEBCO Data and Products: https://www.gebco.net/data_and_products/gridded_bathymetry_data/
.. _EMODnet Geoviewer: https://emodnet.ec.europa.eu/geoviewer/
.. _Admiralty Marine Data Portal: https://data.admiralty.co.uk/portal/apps/sites/#/marine-data-portal

The two most common open-access bathymetry datasets are GEBCO (US product) and EMODnet (EU product). 
The GEBCO data are approximately 450m resolution and the EMODnet data are approximetly 100m resolution. 
For coastal modelling higher resolution bathymetry should be used. 
The UK Hydrographic Office (UKHO) now provide their high resolution (2 to 4m) bathymetry as open-access using a UK Governement public license. 
The UKHO data have unrestricted use, but the coverage is limited to areas where high resolution bathymetric surveys have been carried out.
Data service access is given in :numref:`bathydatatab`.

.. _bathydatatab:
.. csv-table:: : Open-access bathymetry datasets.
               :header-rows: 1
               :widths: 20, 40, 40, 60
               :file: ./bathy_datasets.csv

High resolution bathymetric data covering most of the UK waters are available through DigiMap, but their uses is restricted to academic and educational only.

Substrate Class Data
--------------------

.. _EMODnet: https://emodnet.ec.europa.eu/en
.. _EMODnet seabed habitats: https://www.emodnet-seabedhabitats.eu/about/

The generation of spatially varying bottom friction maps requires maps of seabed substrate type, from which appropriate bottom friction coefficients can be calculated. 

`EMODnet`_ provides spatial maps of substrate type for the European shelf seas (including the Arctic), the Baltic Sea, the Mediteranean Sea and the Black Sea based on seabed habitat and geology mapping data. 
The data are provided as Generic Database (\*.gdb) directory structure. These can be loaded into QGIS as a vector layer from a directory, and maipulate to extract the data required for your model region. The mapping is in the form of polygons with a range of attributes, where the ``Substrate`` attribute gives a text description of the substrate class. The substrate classification can be used to estimate a bottom friction coefficient. The :ref:`substrates <domain.substrates>` module gives examples of functions for mapping between substrate class and bottom drag, where the drag coefficient can be converted to a corresponding bottom friction using the functions in the :ref:`physics <core.physics>` module.
A link to the EMODnet substrate data service is given in :numref:`seabeddatatab`.

.. _seabeddatatab:
.. csv-table:: : Open-access seabed substrate datasets.
               :header-rows: 1
               :widths: 55, 55
               :file: ./substrate_datasets.csv
               

Tidal Atlases
-------------

**Under construction**


Model Constructs
----------------

**Under construction**


Model Output
------------

**Under construction**

