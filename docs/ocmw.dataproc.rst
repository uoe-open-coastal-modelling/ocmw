.. _dataproc:

ocmw.dataproc module
====================

.. automodule:: ocmw.dataproc
   :members:
   :undoc-members:
   :show-inheritance:

A set of data processing modules with functions to extract and process model and ADCP data.

Models deveopled using the Open Telemac-Mascaret solver formed the starting point for the development of these tools.
The modules ``otm_extraction`` and ``otm_vectorfield`` are specific to simulation data generated using Open Telemac-Mascaret.
The OTM modules output data in a standardized OCMW internal format that can be processed using ``ocmw_extract``, ``processModelData``.
To apply these tools to other numerical solvers, an equivalent set of data extraction tools will need to be written.

At present the OCMW internal data format used is a structured matlab file, this allows the data generated to directly processed using matlab.
The intent is to move to a more generic format, e.g. netCDF4.

.. autosummary::
	:nosignatures:
	
	~ocmw.dataproc.otm_extraction
	~ocmw.dataproc.otm_vectorfield
	~ocmw.dataproc.ocmw_extract
	~ocmw.dataproc.processModelData
	~ocmw.dataproc.processADCPData


Submodules
----------

.. toctree::
   :maxdepth: 1

   ocmw.dataproc.otm_extraction
   ocmw.dataproc.otm_vectorfield
   ocmw.dataproc.ocmw_extract
   ocmw.dataproc.processModelData
   ocmw.dataproc.processADCPData
