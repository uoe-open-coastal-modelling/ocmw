# -*- coding: utf-8 -*-

"""
A set of basic non-dimensional parameters used for flow classification.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import numpy as np
# Non-Standard Python Dependencies
# Local Module Dependencies
from .physics import grav
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
def courantNumber(node, nodes, nodesX, nodesY, velX, velY, timestep):
    """Calculates the Courant Number at a model mesh node.
    """
    iref = np.where(nodes == node)
    ipts = np.where(nodes != node)
    dx = nodesX[ipts]-nodesX[iref]
    dy = nodesY[ipts]-nodesY[iref]
    dr = np.sqrt(np.square(dx)+np.square(dy))
    vel = np.sqrt(np.square(velX)+np.square(velY))
    ux = dx/dr
    uy = dy/dr
    velR = ux*velX+uy*velY
    Cr = velR*timestep/dr
    Cr = np.max(Cr)
    Cr = vel*timestep/np.min(dr)
    return Cr


def froudeNumber(dpth, vel):
    """Calculates the Froude Number at a model mesh node.
    """
    Fr = vel/np.sqrt(grav*dpth)
    return Fr


def ekmanNumber(dpth, omega, kinematicVisc=1.04E-6):
    """Calculates the Ekman Number at a model mesh node.
    """
    Ek = kinematicVisc/(omega*np.square(dpth))
    return Ek


def rossbyNumber(dpth, omega):
    """Calculates the Rosby Number at a model mesh node.
    """
    Ro = np.sqrt(grav*np.abs(dpth))/np.abs(omega)
    return Ro

