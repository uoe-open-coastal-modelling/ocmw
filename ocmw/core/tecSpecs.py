#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tidal energy converter (TEC) parameters used to estimate power production.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------

# Standard Python Dependencies
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
tecList = ['D10','DeepGenIV','HS1000','AR1500','AR2000','OrbitalO2',
           'OrbitalO3','Nova100D','Nova250D','Nova500D']

# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------
class tidalTurbine:
    """
    Object for storing key Tidal Turbine paramters
    """
    def __init__(self):
        self.model = ''
        self.designer = ''
        self.bottomMounted = True
        self.rotorDiam = None
        self.hubHeight = None
        self.cutInSpeed = None
        self.cutOutSPeed = None
        self.ratedSpeed = None
        self.Cp = None



# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def getTec(tecName: str):
    """
    Function to select a specific tidal energy converter (TEC).
    """
    if tecName in tecList:
        if "d10" in [tecName.lower()]:
            tec = tecD10()
        if "deepgeniv" in [tecName.lower()]:
            tec = tecDeepGenIV()
        if "hs1000" in [tecName.lower()]:
            tec = tecHS1000()
        if "ar1500" in [tecName.lower()]:
            tec = tecAR1500()
        if "ar2000" in [tecName.lower()]:
            tec = tecAR2000()
        if "orbitalo2" in [tecName.lower()]:
            tec = tecOrbitalO2()
        if "orbitalo3" in [tecName.lower()]:
            tec = tecOrbitalO3()
        if "nova100d" in [tecName.lower()]:
            tec = tecNova100D()
        if "m250d" in [tecName.lower()]:
            tec = tecNova250D()
        if "nova500d" in [tecName.lower()]:
            tec = tecNova500D()
    else:
        print('Warning: TEC '+tecName+' not avaialble.')
        print('Options: ',tecList)
        tec = None
    return tec


def tecD10():
    """Saballa D10 turbine parameters
    """
    # Sabella D10
    D10 = tidalTurbine
    D10.model = 'D10'
    D10.designer = 'Sabella'
    D10.bottomMounted = True
    D10.rotorDiam = 10.0
    D10.hubHeight = 11.0
    D10.cutInSpeed = 0.4
    D10.cutOutSpeed = 4.0
    D10.ratedSpeed = 3.1
    D10.Cp = 0.39
    return D10

def tecDeepGenIV():
    """Alstrom DeepGen IV turbine parameters
    """
    # DeepGen IV
    DeepGenIV = tidalTurbine
    DeepGenIV.model = 'DeepGenIV'
    DeepGenIV.designer = 'Alstrom'
    DeepGenIV.bottomMounted = True
    DeepGenIV.rotorDiam = 18.0
    DeepGenIV.hubHeight = 19.0
    DeepGenIV.cutInSpeed = 1.0
    DeepGenIV.cutOutSpeed = 4.0
    DeepGenIV.ratedSpeed = 2.7
    DeepGenIV.Cp = 0.39
    return DeepGenIV


def tecHS1000():
    """HammerFest HS1000 turbine parameters
    """
    # HS1000
    HS1000 = tidalTurbine
    HS1000.model = 'HammerFest HS1000'
    HS1000.designer = 'Andritz Hydro'
    HS1000.bottomMounted = True
    HS1000.rotorDiam = 21.0
    HS1000.hubHeight = 15.0
    HS1000.cutInSpeed = 1.0
    HS1000.cutOutSpeed = 4.0
    HS1000.ratedSpeed = 3.15
    HS1000.Cp = 0.33
    return HS1000


def tecAR1500():
    """SIMEC-Atlantis AR1500 turbine parameters
    """
    # AR1500
    AR1500 = tidalTurbine
    AR1500.model = 'AR1500'
    AR1500.designer = 'SIMEC-Atlantis'
    AR1500.bottomMounted = True
    AR1500.rotorDiam = 18.0
    AR1500.hubHeight = 14.0
    AR1500.cutInSpeed = 1.0
    AR1500.cutOutSpeed = 5.0
    AR1500.ratedSpeed = 3.00
    AR1500.Cp = 0.39
    return AR1500


def tecAR2000():
    """SIMEC-Atlantis AR2000 turbine parameters
    """
    # AR2000
    AR2000 = tidalTurbine
    AR2000.model = 'AR2000'
    AR2000.designer = 'SIMEC-Atlantis'
    AR2000.bottomMounted = True
    AR2000.rotorDiam = 20.0
    AR2000.hubHeight = 15.0
    AR2000.cutInSpeed = 1.0
    AR2000.cutOutSpeed = 4.0
    AR2000.ratedSpeed = 3.05
    AR2000.Cp = 0.36
    return AR2000


def tecOrbitalO2():
    """Orbital O2 turbine parameters
    """
    # Orbital O2
    Orbital = tidalTurbine
    Orbital.model = 'Orbital O2'
    Orbital.designer = 'Orbital Marine Power'
    Orbital.bottomMounted = False
    Orbital.rotorDiam = 22.0
    Orbital.hubHeight = 14.0
    Orbital.cutInSpeed = 1.0
    Orbital.cutOutSpeed = 4.5
    Orbital.ratedSpeed = 2.5
    Orbital.Cp = 0.36
    return Orbital


def tecOrbitalO3():
    """Orbital O3 turbine parameters
    """
    # Orbital O3
    Orbital = tidalTurbine
    Orbital.model = 'Orbital O3'
    Orbital.designer = 'Orbital Marine Power'
    Orbital.bottomMounted = False
    Orbital.rotorDiam = 24.0
    Orbital.hubHeight = 15.0
    Orbital.cutInSpeed = 1.0
    Orbital.cutOutSpeed = 3.4
    Orbital.ratedSpeed = 2.5
    Orbital.Cp = 0.36
    return Orbital


def tecNova100D():
    """Nova Innovation M100_D turbine parameters
    """
    # Nova Innovation M100_D
    Nova100D = tidalTurbine
    Nova100D.model = 'M100_D'
    Nova100D.designer = 'Nova Innovation'
    Nova100D.bottomMounted = True
    Nova100D.rotorDiam = 8.5
    Nova100D.hubHeight = 8.9
    Nova100D.cutInSpeed = 0.5
    Nova100D.cutOutSpeed = 6.0
    Nova100D.ratedSpeed = 2.0
    Nova100D.Cp = 0.43
    return Nova100D


def tecNova250D():
    """Nova Innovation M250_D turbine parameters
    """
    # Nova Innovation M250_D
    Nova250D = tidalTurbine
    Nova250D.model = 'M250_D'
    Nova250D.designer = 'Nova Innovation'
    Nova250D.bottomMounted = True
    Nova250D.rotorDiam = 7.5
    Nova250D.hubHeight = 9.0
    Nova250D.cutInSpeed = 0.5
    Nova250D.cutOutSpeed = 6.0
    Nova250D.ratedSpeed = 2.5
    Nova250D.Cp = 0.43
    return Nova250D


def tecNova500D():
    """Nova Innovation M500_D turbine parameters
    """
    # Nova Innovation M500_D
    Nova500D = tidalTurbine
    Nova500D.model = 'M500_D'
    Nova500D.designer = 'Nova Innovation'
    Nova500D.bottomMounted = True
    Nova500D.rotorDiam = 13.5
    Nova500D.hubHeight = 13.0
    Nova500D.cutInSpeed = 1.0
    Nova500D.cutOutSpeed = 6.0
    Nova500D.ratedSpeed = 2.5
    Nova500D.Cp = 0.43
    return Nova500D


# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#def main():
#    return

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

#if __name__ == "__main__":
#    main()

