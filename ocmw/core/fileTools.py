# -*- coding: utf-8 -*-
"""
Basic file manipulation tools used in the OCMW processes.

"""


# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import fnmatch
import io
from datetime import datetime
import numpy as np
import scipy.io as sio
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def getListOfFiles(filePath, searchStr=""):
    """
    Get a list of file from a directory based on a search string.
    
    Parameters
    ----------
    filePath : str
        path string to directory to be searched for files.
    searchStr : str
        sub-string fragment that can be used to identify a file or set of files.

    Returns
    -------
    files : list, str
        a list of the file names containing the search string.

    """
    if not searchStr:
        searchStr = "*"
    else:
        srchStr = "*"+searchStr+"*"
    files = fnmatch.filter(os.listdir(filePath), srchStr)
    files.sort()
    return files

def loadmat(filename):
    """
    This function should be called instead of direct scipy.io.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects.
    """
    data = {}
    data = sio.loadmat(filename, mdict=data, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)

def _check_keys(dict):
    """
    Checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries.
    
    """
    for key in dict:
        if isinstance(dict[key], sio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict        

def _todict(matobj):
    """
    A recursive function which constructs from matobjects nested dictionaries.
    
    """
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, sio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict


def print_mat_nested(d, indent=0):
    """
    Pretty print nested structures from .mat files   

    """
    
    if isinstance(d, dict):
        for key, value in d.items():         # iteritems loops through key, value pairs
          print("\t" * indent + "Key: " + str(key))
          print_mat_nested(value, indent+1)

    if isinstance(d,np.ndarray) and d.dtype.names is not None:  # Note: and short-circuits by default
        for n in d.dtype.names:    # This means it's a struct, it's bit of a kludge test.
            print("\t" * indent + "Field: " + str(n))
            print_mat_nested(d[n], indent+1)


def generateGlobalAttributes(titleStr):
    """
    Generate a generic global attributes dictionary for adding meta data to a 
    data dictionary.

    Parameters
    ----------
    titleStr : str
        a string defining the data set being attributed.

    Returns
    -------
    globalAttr : dict
        a dictionary containing generic data about the dataset creation.

    """
    globalAttr = {}
    globalAttr["title"] = titleStr
    globalAttr["creation_date"] = datetime.now().strftime("%d-%b-%Y %H:%M:%S")
    globalAttr["contact"] = " "
    globalAttr["institution"] = "School of Engineering, The University of Edinburgh, UK"
    return globalAttr

