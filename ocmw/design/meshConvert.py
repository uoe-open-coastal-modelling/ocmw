# -*- coding: utf-8 -*-
"""
Functions for converting between mesh formats

Chris Old
IES, School of Engineering, University of Edinburgh
Nov 2023
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
import os
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.otm_gpl.parser_gmsh import MSH
from ocmw.otm_gpl.selafin import Selafin
from ocmw.dataman.dataReaders import ww3Mesh
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
def msh2slf(datapath,meshfile,slftitle=''):
    """
    Convert gmesh \*.msh file to an Open Telemac-Mascaret selafin file.

    """
    mesh = MSH(datapath+'/'+meshfile)
    head, _ = os.path.splitext(meshfile)
    slf_file = datapath+'/'+head+'.slf'
    mesh.put_content(slf_file)
    # Correct title bug
    slf = Selafin(slf_file)
    slf.title = "{:<80}".format(slftitle)
    # Remove Bathymetry units
    slf.varunits = ['                ']
    # Correct iparam
    slf.iparam = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    # Correct ipob2 & ipob3
    #slf.ipob2[:] = 0
    #slf.ipob3[:] = 0
    # Update file
    slf.put_content(slf_file.replace('_bathy.','.'))
    return


def WW3mesh2sms2dm(outpath,outfile,mesh: ww3Mesh):
    """
    Convert WW3 mesh data to SMS2DM mesh format.
    
    """
    f = open(os.path.join(outpath,outfile),'w')
    f.write('MESH2D\n')
    elems = mesh.getVar('tri')
    X = mesh.getVar('longitude')
    Y = mesh.getVar('latitude')
    bathy = mesh.getVar('bathy')
    nelems = len(elems)
    for el in range(nelems):
        s = ['E3T',str(el+1),
             str(elems[el,0]),
             str(elems[el,1]),
             str(elems[el,2]),'\n']
        aline = ' '.join(s)
        f.write(aline)
    nnodes = len(X)
    for nd in range(nnodes):
        s = ['ND',str(nd+1),str(X[nd]),str(Y[nd]),str(bathy[nd]),'\n']
        aline = ' '.join(s)
        f.write(aline)
    f.close()
    return


#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------


