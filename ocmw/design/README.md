# Model Design

![Model Design Workflow Diagram](https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/raw/main/images/OCMW_model_design_workflow_diagram_cropped.png)

Model design is the process of building the construct and collating the data that are passed to the numerical solver used to simulate the coastal hydrodynamics, ensuring numerically stability of the resulting model, and ensuring the solver configuration produces a convergent solution.
The model design workflow is shown in the above graphic.

## Domain Extent
There are no hard and fast rules for choosing the model domain extent.
The choice depends on the purpose of the model, the available coastline and bathymetry data, the sensitivity of the flow in the region of interest to open boundary proximity and far-field processes, and the potential computational cost.

Depending on the solver being used, the boundary will be defined by either a polygon or a set of lines. 
These are typically created using GIS software, e.g. the open-source [QGIS](https://qgis.org/en/site/) package.

## Mesh Construction
There are a variety of tools available for mesh construction, Table lists the more common packages.
The format of the resulting mesh file is solver-dependent, so it may be necessary to convert the format depending on the tool used.
A mesh is defined by a set of node points and a set of elements that are constructed from the nodes. 
We will focus on unstructured triangular meshes where each mesh element is defined by a set of three node points.
The mesh is constrained by the hard coastling, which may include islands, and the domain extent. 
Nodes are identified by their location in the domain as open boundary nodes, hard boundary nodes, or internal nodes.
Some meshing tools require the open boundary nodes to be identified separately as part of the meshing process, e.g. BlueKenue.

To geo-locate the model, a suitable coordinate reference system (CRS) must be used when constructing the mesh.
The two most common CRS used are a system of spherical coordinates based on some geoid representation of the Earth (e.g. WGS84) or a system of rectangular coordinates based on a local Universal Tranverse Mercator (UTM) projection of the geoid used to represent the Earth.
The CRS used will depend on the spatial extent of the model domain. 
A transveres Mercator project introduces a distortion in lengths with a the largest effect occurring at the North and South poles.
If the domain extent is sufficiently small then the distorting effect of curvature on the UTM projection is negligible across the domain. 
All data used to construct the model, i.e. the coastline, bathymetry, and domain extent data, must use the same CRS.
The advantage of using a UTM projection is that post-processing of the model output is simplified as there is no requirement to use spherical trigonometry to calculate parameters from the data.


## Model Forcing

## Numerical Solver Configuration
There are many solvers available for modelling coastal regions. 
In keeping with our Open ethos, we will only consider open-source numerical solvers.

### Open Telemac-Mascaret
[Open Telemac-Mascaret](http://www.opentelemac.org/) is a well established and verified suite of tools for solving free-surface flow problems. 
The telemac-mascaret software is available through a [GitHub](https://gitlab.pam-retd.fr/otm/telemac-mascaret) repository. 
There are integrated tools for surface wave modelling (TOMAWAC) and sediment transport modelling (SISYPHE).
These packages can be fully coupled with the hydrodynamic solver.
The models are written in FORTRAN with a python wrapper for ease of use. 
In general the tools are easier to install in Linux than Microsoft Windows.

The simulation is controlled by a steering file which defines the input data (mesh geomerty, open boundaries, and forcing fields) and the numerical configuration, i.e. the time step, friction model, turbulence models, coriolis forcing, wetting/drying, numerical schemes used and their associated constraints, and the output frequency for storing the model. 
The model has a restart capability, where either the output of the previous model run or a record of the last time step of the previous model can be used to initialise the follow on run.
The later can be controlled through the steering file.

### Thetis
[Thetis](https://thetisproject.org/)

## Numerical Stability

## Numerical Sensitivity



