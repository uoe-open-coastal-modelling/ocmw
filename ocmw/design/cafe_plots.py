# -*- coding: utf-8 -*-
"""
Generation of CAFE (Cummulative Area Fraction Error) plots.

Chris Old
IES, School of Engineering, University of Edinburgh
Aug 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
# Non-Standard Python Dependencies
import numpy as np
import matplotlib.pyplot as plt
# Local Module Dependencies
from ocmw.core.fileTools import loadmat
# Other Dependencies


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def getParams(paramfile):
    data = loadmat(paramfile)
    x = data['mesh']['cLoc'][:,0]
    y = data['mesh']['cLoc'][:,1]
    A = data['mesh']['area']
    KED = data['kedens']
    return [x,y,A,KED]

def getNNIndex(tru,mod):
    NNI = np.zeros(mod[0].shape, dtype=int)
    for i in range(len(mod[0])):
        dx = tru[0]-mod[0][i]
        dy = tru[1]-mod[1][i]
        dr = np.sqrt(np.square(dx)+np.square(dy))
        indx = np.where(dr == np.min(dr))[0]
        NNI[i] = indx[0]
    return NNI

def getNodesInRoI(x,y,roi):
    indx1 = np.where( (x>=roi[0]) & (x<=roi[1]) )[0]
    indx2 = np.where( (y[indx1]>=roi[2]) & (y[indx1]<=roi[3]) )
    roiNodes = indx1[indx2]
    return roiNodes

def calcMeanBias(tru,mod):
    mbias = np.nanmean(mod-tru,axis=0)
    return mbias

def calcCAFE(error,area):
    isort = np.argsort(error)
    ksort = np.sort(error)

    A_tot = np.sum(area)

    negIndx = np.where(ksort < 0.0)[0]
    posIndx = np.where(ksort >= 0.0)[0]

    negCumArea = 100.0*np.cumsum(area[isort[negIndx]])/A_tot
    posCumArea = (100.0*np.cumsum(area[isort[posIndx]][::-1])/A_tot)[::-1]
    
    x_cafe = np.concatenate((ksort[negIndx],0.0,ksort[posIndx]),axis=None)
    y_cafe = np.concatenate((negCumArea,np.nan,posCumArea),axis=None)
    
    return x_cafe, y_cafe

def processModel(paramfile,refStr,truth,drng=[600,720],roi=None):
    print('Model: '+refStr)
    mod_data = getParams(paramfile)

    # Interpolate truth element data onto model element centroids (nearest??)
    NNI = getNNIndex(tru_data,mod_data)

    # Get nodes in roi
    if roi is not None:
        x = mod_data[0]-36.0
        y = mod_data[1]
        roi_indx = getNodesInRoI(x,y,roi)
        tru = truth[3][drng[0]:drng[1],NNI[roi_indx]]
        mod = mod_data[3][drng[0]:drng[1],roi_indx]
        Area = mod_data[2][roi_indx]
    else:
        tru = truth[3][drng[0]:drng[1],NNI]
        mod = mod_data[3][drng[0]:drng[1],:]
        Area = mod_data[2]
    
    # Calculate errors
    #sigma_ked = np.nanmean(mod,0)-np.nanmean(tru,0)
    mbias_ked = calcMeanBias(tru,mod)

    # Calculate CAFE (x,y) data
    #x_cafe, y_cafe = calcCAFE(sigma_ked, Area)
    x_cafe, y_cafe = calcCAFE(mbias_ked, Area)

    # Plot CAFE - semilogy plot, include cumulative area = 1% line across plot.
    if "base" in refStr:
        labStr = "0"
        plt.semilogy(x_cafe, y_cafe,'k',label=r"$\mathcal{H}$"+labStr)
    else:
        labStr = refStr.split('0')[-1]
        plt.semilogy(x_cafe, y_cafe,label=r"$\mathcal{H}$"+labStr)
    plt.xlim([-0.7,0.7])
    plt.ylim([0.001,100.0])
    return
    

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------
dpath = "D:/Models/mesh_refine"
#dpath = "F:/Models/mesh_refine"
'''
#---------------------------------------------------------------
# Constant inflow case
#---------------------------------------------------------------
projStr = "inflow_const"

# Load truth extracts
print("Loading truth data...")
paramfile = dpath+'/'+projStr+'/truth_params.mat'
tru_data = getParams(paramfile)

#xlabelStr = 'KE Density Difference'
xlabelStr = 'KE Density Mean Bias'
ylabelStr = 'Cumulative Area %'

# Set RoI
roi=None
#roi = [-18.0,18.0,0.0,19.0]

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Curl(v)
print("Refinements based on Curl(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_con_vcurl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Grad(H)
print("Refinements based on Grad(h)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_con_hgrad"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Grad(v)
print("Refinements based on Grad(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_con_vgrad"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - max + std dev Curl(v)
print("Refinements based on max[Curl(v)] + std[Curl(v)]")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_con_msvcl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Curl(v)
print("Refinements based on Curl(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_cum_vcurl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Grad(H)
print("Refinements based on Grad(h)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_cum_hgrad"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Grad(v)
print("Refinements based on Grad(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_cum_vgrad"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - max + std dev Curl(v)
print("Refinements based on max[Curl(v)] + std[Curl(v)]")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
processModel(paramfile,refStr,tru_data,roi=roi)
for iref in range(6):
    refStr = "refine_cum_msvcl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_params.mat'
    processModel(paramfile,refStr,tru_data,roi=roi)

# Add legend
plt.legend()
'''
#---------------------------------------------------------------
# Transient inflow case
#---------------------------------------------------------------
projStr = "inflow_trans"

# Load truth extracts
print("Loading truth data...")
paramfile = dpath+'/'+projStr+'/truth_1p0_params_trans.mat'
tru_data = getParams(paramfile)

#xlabelStr = 'KE Density Difference'
xlabelStr = 'KE Density Mean Bias'
ylabelStr = 'Cumulative Area %'

# Set RoI
#roi=None
roi = [-18.0,18.0,0.0,19.0]

# Set figure size in inches
fx = 8.5
fy = 5.0

#-----------------------------
# Full cycle (Ebb+Flood)
#-----------------------------
drng = [480,600]

# Initialise plot
plt.figure()
#plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Curl(v)
print("Refinements based on Curl(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_vcurl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend([r'$\mathcal{H}_{0}$',r'$\mathcal{H}_{1}$',r'$\mathcal{H}_{2}$',r'$\mathcal{H}_{3}$',r'$\mathcal{H}_{4}$',r'$\mathcal{H}_{5}$',r'$\mathcal{H}_{6}$'])
ax = plt.gca()
xlm = ax.get_xlim()
ylm = ax.get_ylim()
plt.plot([xlm[0],xlm[1]],[1.0,1.0],'gray',linewidth=1)
plt.plot([0.0,0.0],[ylm[0],ylm[1]],'gray',linewidth=1)
tstr = r'$\mathrm{W2} - \nabla{\times{\mathbf{u}}}$'
plt.text(.01, .99, tstr, ha='left', va='top', transform=ax.transAxes, fontsize=14)
plt.tight_layout()
plt.gcf().set_size_inches((fx,fy))


# Initialise plot
plt.figure()
#plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - max + std dev Curl(v)
print("Refinements based on max[Curl(v)] + std[Curl(v)]")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_msvcl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend()
ax = plt.gca()
xlm = ax.get_xlim()
ylm = ax.get_ylim()
plt.plot([xlm[0],xlm[1]],[1.0,1.0],'gray',linewidth=1)
plt.plot([0.0,0.0],[ylm[0],ylm[1]],'gray',linewidth=1)
#ax.get_yaxis().set_visible(False)
#scale = 0.90235294117647
#scale = 0.95293117647059
scale = 1.0
tstr = r'$\mathrm{W2} - \{ \nabla{\times{\mathbf{u}}}, \sigma{(\nabla{\times{\mathbf{u}}})} \}$'
plt.text(.01, .99, tstr, ha='left', va='top', transform=ax.transAxes, fontsize=14)
plt.tight_layout()
plt.gcf().set_size_inches((fx*scale,fy))

'''
#-----------------------------
# Flood
#-----------------------------
drng = [480,540]

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Curl(v)
print("Refinements based on Curl(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_vcurl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - max + std dev Curl(v)
print("Refinements based on max[Curl(v)] + std[Curl(v)]")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_msvcl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend()

#-----------------------------
# Ebb
#-----------------------------
drng = [540,600]

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - Curl(v)
print("Refinements based on Curl(v)")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_vcurl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend()

# Initialise plot
plt.figure()
plt.grid('on')
plt.xlabel(xlabelStr)
plt.ylabel(ylabelStr)

# Process models - max + std dev Curl(v)
print("Refinements based on max[Curl(v)] + std[Curl(v)]")
# Base
refStr="base"
paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)
for iref in range(6):
    refStr = "refine_con_msvcl"+str(iref+1).zfill(2)
    paramfile = dpath+'/'+projStr+'/'+refStr+'_1p0_params_trans.mat'
    processModel(paramfile,refStr,tru_data,drng=drng,roi=roi)

# Add legend
plt.legend()
'''


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

