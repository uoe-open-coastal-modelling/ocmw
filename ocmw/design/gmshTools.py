# -*- coding: utf-8 -*-
"""
Tools for manupulating GMsh ASCII mesh files.

Chris Old
IES, School of Engineering, University of Edinburgh
Aug 2023

"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
# Non-Standard Python Dependencies
import meshio
# Local Module Dependencies
from ocmw.domain.bathyTools import load_bathy, interp_bathy
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------
def get_mesh_nodes(inmeshfile):
    """
    Load mesh node locations from \*.msh file.

    """
    mesh = meshio.read(inmeshfile.replace('\\','/'))
    nodes = mesh.points[:,0:2]
    return nodes

def isTag(astr):
    """
    Identify line in \*.msh as starting with a tag

    """
    flag =  astr[0] == '$'
    return flag

def isScalar(astr):
    """
    Test if line in \*.msh file contains a single value

    """
    flag = len([pos for pos, char in enumerate(astr) if char == ' ']) == 0
    return flag

def mshBathy(datapath,meshfile,bathyfile,newmeshfile,interp_radius,boffsets=[0.0,0.0,0.0]):
    """
    Interpolate bathymetry onto mesh nodes and add to \*.msh file.

    """
    nodes = get_mesh_nodes(datapath+'/'+meshfile)
    bathy = load_bathy(datapath+'/'+bathyfile)
    z = interp_bathy(nodes,bathy,interp_radius)
    fmsh = open(datapath+'/'+meshfile.replace('\\','/'),'r')
    fmshout = open(datapath+'/'+newmeshfile.replace('\\','/'),'w',newline='')
    nnode = 0
    for line in fmsh:
        if isTag(line[:-1]):
            tagname = line[:-1]
            print(tagname)
        elif (tagname == '$Nodes') & (not isScalar(line[:-1])):
            vals = line[:-1].split(' ')
            line = ' '.join((vals[0],vals[1],vals[2],str(z[nnode]),'\n'))
            nnode += 1
        fmshout.writelines(line)
    fmsh.close()
    return

#--------------------------------------------------------------------------
# MAIN PROCESS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

