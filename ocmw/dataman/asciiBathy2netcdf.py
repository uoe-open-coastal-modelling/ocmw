# -*- coding: utf-8 -*-

"""
Tools for standardizing the conversion of ASCII bathymetry data to netCDF4 
format for archiving and analysis.

Chris Old
IES, School of Engineering, University of Edinburgh
Mar 2024

"""

#-----------------------------------------------------------------------------
# IMPORTS
#-----------------------------------------------------------------------------
# Standard Python Dependencies
import os
import numpy as np
from datetime import datetime
# Non-Standard Python Dependencies
from netCDF4 import Dataset
import utm
# Local Module Dependencies
# Other Dependencies

# NOTE:
# NetCDF4 in python requires the following packages installed
#     cftime
#     python_hdf4
#     python_netcdf4
#
# These may come by default with an Anaconda install, otherwise the 
# packages can be installed using pip.
#

#-----------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------
ncfTimeUnits = 'days since 1900-01-01 00:00:00 UTC'
ncfEpochStr = '1900-01-01 00:00:00'

data_license = 'Creative Commons License: Attribution 4.0 International'

#-----------------------------------------------------------------------------
# CLASS DEFINITIONS
#-----------------------------------------------------------------------------
class metaDataObj:
    """
	Data object for collating bathymetry acquisition and data processing 
    metadata.
    """
    def __init__(self):
        # Source
        self.title = ''
        self.references = ''
        self.site = ''
        self.data_crs = ''
        self.data_type = ''
        self.data_owner = ''
        self.processing_level = 0
        self.resolution = 0.0

        # Survey information
        self.survey_client = ''
        self.survey_vessel = ''
        self.survey_operator = ''
        self.survey_vorf = ''
        self.survey_datum =''
        self.survey_positioning_system = ''
        self.survey_heading_reference = ''
        self.survey_motion_sensor = ''
        self.survey_instrument = ''
        self.survey_sound_velocity_profiler = ''
        self.survey_rtk_corrections = ''
        
        # File generation
        self.contact = ''
        self.institution = ''
        self.data_assembly_center = ''
        self.date_created = ''
        self.date_updated = ''
        self.history = ''
        self.naming_authority = ''
        self.data_format_version = ''
        self.conventions = 'CF-1.8'
        self.netcdf_version = 'netCDF-4 classic model'
        self.license = data_license
        self.citation = ''
        self.doi = ''


#-----------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------

def check_path(path):
    pathExists = True
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            pathExists = False
            raise
    return pathExists

def is_correct_file_extension(filename,extension):
    fileName, fileExtension = os.path.splitext(filename)
    correctFileExtension = False
    if fileExtension == extension:
        correctFileExtension = True
    return correctFileExtension
    
def set_file_extension(filename,extension):
    fileName, fileExtension = os.path.splitext(filename)
    newFileName = fileName + extension
    return newFileName

def check_file(path,filename,extension):
    path = os.path.normpath(path)
    check_path(path)
    if not is_correct_file_extension(filename,extension):
        filename = set_file_extension(filename,extension)
    return path , filename
    
def create_bathy_netcdf(ncfilename, num_recs=None):
    """
    Create a generic netCDF data file structure
    """
    # create new netcdf file
    ncfile = Dataset(ncfilename,'w',format='NETCDF4')
    
    # define dimensions
    ncfile.createDimension('nrecs', num_recs)
    
    # define variables
    #--------------------------------
    # scalars
    #--------------------------------

    #--------------------------------
    # vectors
    #--------------------------------
    lat = ncfile.createVariable('latitude',np.float32,('nrecs'))
    lat.standard_name = 'latitude'
    lat.long_name = 'latitude'
    lat.crs = 'WGS84'
    lat.epsg = 4326
    lat.units = 'degrees N'
    
    lon = ncfile.createVariable('longitude',np.float32,('nrecs'))
    lon.standard_name = 'longitude'
    lon.long_name = 'longitude'
    lon.crs = 'WGS84'
    lat.epsg = 4326
    lon.units = 'degrees E'
    
    locx = ncfile.createVariable('easting',np.float32,('nrecs'))
    locx.standard_name = 'easting'
    locx.long_name = 'easting'
    locx.crs = 'WGS84 / UTM Zone 30N'
    lat.epsg = 32630
    locx.units = 'm'
    
    locy = ncfile.createVariable('northing',np.float32,('nrecs'))
    locy.standard_name = 'northing'
    locy.long_name = 'northing'
    locx.crs = 'WGS84 / UTM Zone 30N'
    lat.epsg = 32630
    locy.units = 'm'
    
    bathy = ncfile.createVariable('bathy',np.float32,('nrecs'))
    bathy.standard_name = 'bathy'
    bathy.long_name = 'bathymetry'
    bathy.datum = 'lowest astronomical tide'
    bathy.units = 'm'

    # close output netcdf file
    ncfile.close()
    
    return


def build_bathy_netcdf(path:str, filename: str, num_recs=None):
    """
    Build a netCDF4 data file for a specific instrument
    """
    # check path and file name
    path , filename = check_file(path, filename, '.nc')
    # create full file name for NetCDF file
    ncfilename = os.path.join(path,filename)
    create_bathy_netcdf(ncfilename, num_recs)
    return


def populate_core(ncpath,ncfilename,metadata):
    """
    Populate the core data for bathymetry NetCDF4 file.
    """
    # check path and file name
    ncpath , ncfilename = check_file(ncpath, ncfilename, '.nc')
    
    # create full file name for NetCDF file
    bathy_file = os.path.join(ncpath,ncfilename).replace('\\','/')
    
    # open destination netcdf file
    dst = Dataset(bathy_file,'r+',format='NETCDF4')
    
    # set global attributes
    # Description
    dst.title = metadata.title
    dst.references = metadata.references
    dst.site = metadata.site
    dst.data_crs = metadata.data_crs
    dst.data_type = metadata.data_type
    dst.data_owner = metadata.data_owner
    dst.processing_level = metadata.processing_level
    dst.resolution = metadata.resolution

    # Survey information
    dst.survey_client = metadata.survey_client
    dst.survey_vessel = metadata.survey_vessel
    dst.survey_operator = metadata.survey_operator
    dst.survey_vorf = metadata.survey_vorf
    dst.survey_datum = metadata.survey_datum
    dst.survey_positioning_system = metadata.survey_positioning_system
    dst.survey_heading_reference = metadata.survey_heading_reference
    dst.survey_motion_sensor = metadata.survey_motion_sensor
    dst.survey_instrument = metadata.survey_instrument
    dst.survey_sound_velocity_profiler = metadata.survey_sound_velocity_profiler
    dst.survey_rtk_corrections = metadata.survey_rtk_corrections

    # File generation
    dst.contact = metadata.contact
    dst.institution = metadata.institution
    dst.data_assembly_center = metadata.data_assembly_center
    if metadata.date_created == '':
        dst.date_created = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        dst.date_updated = ''
    else:
        dst.date_updated = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    dst.history = metadata.history
    dst.naming_authority = metadata.naming_authority
    dst.data_format_version = metadata.data_format_version
    dst.conventions = metadata.conventions
    dst.netcdf_version = metadata.netcdf_version
    if metadata.license == '':
        dst.license = data_license
    else:
        dst.license = metadata.license
    dst.citation = metadata.citation
    dst.doi = metadata.doi
    
    dst.close()
    
    return


def convert_bathy(datpath,datfilename,ncpath,ncfilename,metadata,
                  delimiter=',',n_header_lines=0,prj='UTM'):
    """
    Generate NetCDF from ASCII bathymetry data (CSV, XYZ).
    """
    
    # create full file name for data file
    data_file = os.path.join(datpath,datfilename).replace('\\','/')
    
    # load bathymetry data file
    data = np.genfromtxt(data_file,delimiter=delimiter,skip_header=n_header_lines)
    src = {}
    if prj == 'UTM':
        src['eastings'] = data[:,0]
        src['northings'] = data[:,1]
        lat, lon = utm.to_latlon(data[:,0],data[:,1],30,'N')
        src['latitude'] = lat
        src['longitude'] = lon
    else:
        src['latitude'] = data[:,0]
        src['longitude'] = data[:,1]
        lat = data[:,0]
        lon = data[:,1]
        loc = utm.from_latlon(lat,lon,30,'N')
        src['eastings'] = loc[0]
        src['northings'] = loc[1]
    src['bathy'] = data[:,2]
    
    nrecs = len(src['bathy'])
    
    # check path and file name
    ncpath , ncfilename = check_file(ncpath, ncfilename, '.nc')
    
    # build netcdf file
    build_bathy_netcdf(ncpath, ncfilename, num_recs=nrecs)
    
    # populate core data
    populate_core(ncpath,ncfilename,metadata)
    
    # create full file name for NetCDF file
    adcp_file = os.path.join(ncpath,ncfilename).replace('\\','/')
    
    # open destination netcdf file
    dst = Dataset(adcp_file,'r+',format='NETCDF4')
    
    dst.variables['easting'][:] = np.float32(np.squeeze(src['eastings']))
    dst.variables['northing'][:] = np.float32(np.squeeze(src['northings']))
    dst.variables['latitude'][:] = np.float32(np.squeeze(src['latitude']))
    dst.variables['longitude'][:] = np.float32(np.squeeze(src['longitude']))
    dst.variables['bathy'][:] = np.float32(np.squeeze(src['bathy']))
    
    dst.close()
    
    return


#-----------------------------------------------------------------------------
# MODULE TEST CODE
#-----------------------------------------------------------------------------
