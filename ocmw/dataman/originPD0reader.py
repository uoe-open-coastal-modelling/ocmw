#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 11:06:42 2023

@author: cold2

NOTE: This requires the Sonardyne Origin data processing tools
      Available from:
      
      https://github.com/Sonardyne/origin-message-deserialisers/tree/main
"""

#-----------------------------------------------------------------------------
# IMPORTS
#-----------------------------------------------------------------------------
# Standard Python Dependencies
import sys
import os
import math
# Non-Standard Python Dependencies
import numpy as np
# Local Module Dependencies
from ocmw.core.timeFuncs import dateNumFromDateStr, python2matlab
# Other Dependencies
sys.path.insert(1, '../../src/') # This assumes this script is in origin/examples/python3
from PDNMessages.PDN_messages   import *
from PDNMessages.UserPD0Section import *


# ----------------------------------------------------------------------------
#   GLOBAL VARIABLES
# ----------------------------------------------------------------------------
c1 = 9.72659
c2 = -2.2512e-5
c3 = 2.279e-10
c4 = -1.82e-15

g0 = 9.780318
g2 = 5.2788e-3
g4 = 2.36e-5
gv = 2.184e-6


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   FUNCTION DEFINITIONS
# ----------------------------------------------------------------------------
def pressure2depth(d0,lat,P):
    """
    Convert pressure sensor data to depth below surface.
    """
    l = np.sin(np.radians(lat))
    gc = 0.5*gv*P+g0*(1.0+g2*l*l+g4*l*l*l*l)
    depth = d0 + (c1*P+c2*P*P+c3*P*P*P+c4*P*P*P*P)/gc
    return depth


def dateStrFromVLTimeStamp(varldr):
    """
    Convert the variable leader timestamp to a date string.
    """
    year = varldr.RTCTSYear + 2000
    month = varldr.RTCTSMonth
    day = varldr.RTCTSDay
    hour = varldr.RTCTSHour
    minute = varldr.RTCTSMin
    sec = varldr.RTCTSSec
    hnd = varldr.RTCTSHSec
    dstr = str(year)+'-'+str(month).zfill(2)+'-'+str(day).zfill(2)
    tstr = str(hour).zfill(2)+':'+str(minute).zfill(2)+':'+str(sec).zfill(2)
    tstr = tstr+'.'+str(hnd).zfill(2)
    return dstr+' '+tstr


def unpackFixedLeader(fxldr):
    """
    Unpack the fixed leader to extract the instrument configuration.
    """
    fixLeader = {}
    fixLeader['SerialNumber'] = fxldr.SerialNumber
    fixLeader['CpuFirmwareVer'] = fxldr.CpuFirmwareVer
    fixLeader['CpuFirmwareRev'] = fxldr.CpuFirmwareRev
    fixLeader['BeamAngle'] = fxldr.BeamAngle / 1.0
    fixLeader['NumberOfBeams'] = fxldr.NumberOfBeams
    fixLeader['NumberOfCells'] = fxldr.NumberOfCells
    fixLeader['BlankAfterTransmit'] = fxldr.BlankAfterTransmitCm / 100.0
    fixLeader['Bin1Distance'] = fxldr.Bin1DistanceCm / 100.0
    fixLeader['DepthCellLength'] = fxldr.DepthCellLengthCm / 100.0
    fixLeader['HeadingAlignment'] = fxldr.HeadingAlignment
    fixLeader['HeadingBias'] = fxldr.HeadingBias
    fixLeader['PingsPerEnsemble'] = fxldr.PingsPerEnsemble
    fixLeader['samplePeriod'] = fxldr.TPPMins*3600.0+fxldr.TPPSecs*60.0+fxldr.TPPHunds/100.0
    fixLeader['sampleFreq'] = 1.0/(fxldr.TPPMins*3600.0+fxldr.TPPSecs*60.0+fxldr.TPPHunds/100.0)
    fixLeader['CodeRepeats'] = fxldr.CodeRepeats
    fixLeader['ErrorVelMax'] = fxldr.ErrorVelMax
    fixLeader['FalseTargetThresh'] = fxldr.FalseTargetThresh
    fixLeader['LagLength'] = fxldr.LagLengthCm / 100.0
    fixLeader['LowcorrThresh'] = fxldr.LowcorrThresh
    fixLeader['PercentGood'] = fxldr.PercentGood
    fixLeader['SystemConfig'] = fxldr.SystemConfig
    fixLeader['SystemPower'] = fxldr.SystemPower
    fixLeader['TPPMins'] = fxldr.TPPMins
    fixLeader['TPPSecs'] = fxldr.TPPSecs
    fixLeader['TPPHunds'] = fxldr.TPPHunds
    fixLeader['TxLagDistance'] = fxldr.TxLagDistanceCm / 100.0
    fixLeader['TxPulseLength'] = fxldr.TxPulseLengthCm / 100.0
    fixLeader['WBSystemBandwidth'] = fxldr.WBSystemBandwidth
    return fixLeader


def unpackTimeStamp(varldr):
    """
    Unpack the timestamp data and convert to a date number
    """
    year = varldr.RTCTSYear + 2000
    month = varldr.RTCTSMonth
    day = varldr.RTCTSDay
    hour = varldr.RTCTSHour
    minute = varldr.RTCTSMin
    sec = varldr.RTCTSSec
    hnd = varldr.RTCTSHSec
    dstr = str(year)+'-'+str(month).zfill(2)+'-'+str(day).zfill(2)
    tstr = str(hour).zfill(2)+':'+str(minute).zfill(2)+':'+str(sec).zfill(2)
    dateNum = dateNumFromDateStr(dstr+' '+tstr)
    dateNum = dateNum + (hnd / 100.0)/(24.0*60.0*60.0)
    return dateNum


def loadPD0File(datapath,filename):
    """
    Load an Origin ADCP PD0 data file into a data dictionary
    """
    print('Loading '+filename)

    pd0file = datapath+'/'+filename

    filesize = os.path.getsize(pd0file)

    with open(pd0file, "rb", buffering = 0) as f:
        # figure out how big the ensemble is from the 6-byte header
        hdr = RDIPD0Header()
        cnt = 0
        num_ensembles = 0
        
        print('Extracting ensembles...')
        
        while (data := f.read(RDIPD0Header.SIZE_BYTES)):
    
            pd0 = PD0Message()
    
            # ---------------- read header ----------------- #
            # put data into byte array
            buf = bytearray(data)
            
            bytes_deserialised = pd0.deserialise(buf)
    
            ensemble_bytes = pd0.get_section(RDIPD0Header.ID).EnsembleBytes + PD0Message.CHECKSUM_SIZE_BYTES
            if (cnt == 0):
                num_ensembles = math.ceil(filesize / ensemble_bytes)
                print('Number of ensembles on file = '+str(num_ensembles))
    
            # seek back on the header size
            f.seek(-RDIPD0Header.SIZE_BYTES, 1)
    
            # ---------------- read entire ensemble ----------------- #
            data = f.read(ensemble_bytes)
    
            # put data into byte array
            orig_ensemble_in_buf = bytearray(data)
            
            bytes_deserialised = pd0.deserialise(orig_ensemble_in_buf)
    
            # Allocate data arrays
            if cnt == 0:
                fxldr = unpackFixedLeader(pd0.get_section(RDIFixedLeader.ID))
                nbeams = fxldr['NumberOfBeams']
                ncells = fxldr['NumberOfCells']
                cellsize = fxldr['DepthCellLength']
                blank = fxldr['BlankAfterTransmit']
                b1dist = fxldr['Bin1Distance']
                z = np.arange(ncells)*cellsize+blank+b1dist
            
                times = np.zeros((num_ensembles,))
                speedOfSound = np.zeros((num_ensembles,))
                depthOfTxdr = np.zeros((num_ensembles,))
                heading = np.zeros((num_ensembles,))
                pitch = np.zeros((num_ensembles,))
                roll = np.zeros((num_ensembles,))
                salinity = np.zeros((num_ensembles,))
                temperature = np.zeros((num_ensembles,))
                pressure = np.zeros((num_ensembles,))
                
                vel_BMS = np.zeros((num_ensembles,ncells,nbeams))
                vel_XYZ = np.zeros((num_ensembles,ncells,nbeams))
                vel_ENU = np.zeros((num_ensembles,ncells,nbeams))
                XCR = np.zeros((num_ensembles,ncells,nbeams))
                BSI = np.zeros((num_ensembles,ncells,nbeams))
                PCG = np.zeros((num_ensembles,ncells,nbeams))
                
            # Load variable leader data
            vl = pd0.get_section(RDIVariableLeader.ID)
            if cnt == 0:
                startTime = dateStrFromVLTimeStamp(vl)
            times[cnt] = unpackTimeStamp(vl)
            speedOfSound[cnt] = vl.SpeedOfSoundMs
            depthOfTxdr[cnt] = vl.DepthOfTxdrDm / 10.0
            heading[cnt] = vl.HeadingCD / 100.0
            pitch[cnt] = vl.PitchCD / 100.0
            roll[cnt] = vl.RollCD / 100.0
            salinity[cnt] = vl.SalinityPPT
            temperature[cnt] = vl.TempCD / 100.0
            pressure[cnt] = vl.PressureDP / 1000.0
            
            # Load array data
            XCR[cnt,:,:] = np.asarray(pd0.get_section(RDIXC.ID).XC).reshape((ncells,nbeams))
            BSI[cnt,:,:] = np.asarray(pd0.get_section(RDIIntensity.ID).Intensities).reshape((ncells,nbeams)) * 0.45
            PCG[cnt,:,:] = np.asarray(pd0.get_section(RDIPrctGood.ID).PrctGood).reshape((ncells,nbeams))
            
            vel_BMS[cnt,:,:] = np.asarray(pd0.get_section(RDIVelocity.ID).BeamVelocitiesMms).reshape((ncells,nbeams)) / 1000.0
            pd0.transform(Frame.INSTRUMENT)
            vel_XYZ[cnt,:,:] = np.asarray(pd0.get_section(RDIVelocity.ID).VelocitiesMms).reshape((ncells,nbeams)) / 1000.0
            pd0.transform(Frame.EARTH)
            vel_ENU[cnt,:,:] = np.asarray(pd0.get_section(RDIVelocity.ID).VelocitiesMms).reshape((ncells,nbeams)) / 1000.0
                
            cnt += 1
    
    stopTime = dateStrFromVLTimeStamp(vl)

    print('Constructing data dictionary...')
    
    PD0 = {}
    PD0['config'] = fxldr
    PD0['startTime'] = startTime
    PD0['stopTime'] = stopTime
    PD0['time_python'] = times
    PD0['time_matlab'] = python2matlab(times)
    PD0['z'] = z
    PD0['speedOfSound'] = speedOfSound
    PD0['depthOfTxdr'] = depthOfTxdr
    PD0['heading'] = heading
    PD0['pitch'] = pitch
    PD0['roll'] = roll
    PD0['salinity'] = salinity
    PD0['temperature'] = temperature
    PD0['pressure'] = pressure
    PD0['XCorr'] = XCR
    PD0['Intensities'] = BSI
    PD0['PrctGood'] = PCG
    PD0['vel_BMS'] = vel_BMS
    PD0['vel_XYZ'] = vel_XYZ
    PD0['vel_ENU'] = vel_ENU
    
    return PD0
    
    
# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

#--------------------------------------------------------------------------
# INTERFACE
#--------------------------------------------------------------------------

