# -*- coding: utf-8 -*-
"""
@author: Chris Old
         IES, School of Engineering, The University of Edinburgh

@note ... Tools developed under the RealTide project (WP2).

@brief
         Tools for connecting to and querying a MySQL database.

@details
         Contains time handling functions, query building functions, database
         connection functions, and database query functions.

@history 23/03/2021 -- Chris Old:
         Created original file on Thu Mar 11 09:58:43 2021
"""

#-----------------------------------------------------------------------------
# IMPORTS
#-----------------------------------------------------------------------------
# Standard Python Dependencies
#from sqlite3 import connect as dbConnect, Error as dbError
import pymysql
from numpy import abs, array, mod
# Non-Standard Python Dependencies
# Local Module Dependencies
from ocmw.core.timeFuncs import dateNumFromDateStr
# Other Dependencies

#-----------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------
stdTimeUnits = 'days since 0001-01-01 00:00:00'
stdTimeFmt = "%Y-%m-%d %H:%M:%S.%f"
stdCalendar = 'gregorian'

matEpochStr = '1900-01-01 00:00:00'
matEpochDateNum = 693962.0

TEC_ebb_bearing = 321.0
TEC_flood_bearing = 140.0
TEC_bw_flood_bearing = 227.0
TEC_bw_ebb_bearing = 52.0
TEC_bearing_tolerance = 10.0

default_fields = ["times", "uref", "tideState", "Hm0Fusion", "Tp", "TECPower", "TECYaw"]
default_deploy = ["ADCP01_NW_Dep0", "ADCP01_NW_Dep1", "ADCP01_NW_Dep2", "ADCP01_NW_Dep3",
                  "ADCP01_NW_Dep5", "ADCP02_NW_Dep5", "ADCPTD7_01_Dep1", "ADCPTD7_02_Dep1", 
                  "ADCPD1", "ADCPD2"]

#-----------------------------------------------------------------------------
# CLASS DEFINITIONS
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------

def mysql_db_connect(dbHost,dbName,un,pw):
    db_conn = pymysql.connect(user=un,password=pw,host=dbHost,database=dbName)
    return db_conn

def mysql_db_close(db_conn):
    db_conn.close()
    return

def get_turbine2tide_angle_range(option: str):
    if option == 'FF':
        min_angle = TEC_flood_bearing - 180.0 - TEC_bearing_tolerance
        min_angle = mod(min_angle, 360.0)
        max_angle = TEC_flood_bearing - 180.0 + TEC_bearing_tolerance
        max_angle = mod(max_angle, 360.0)
        tideState = 0
    if option == 'RF':
        min_angle = TEC_flood_bearing - TEC_bearing_tolerance
        min_angle = mod(min_angle, 360.0)
        max_angle = TEC_flood_bearing + TEC_bearing_tolerance
        max_angle = mod(max_angle, 360.0)
        tideState = 0
    if option == 'FE':
        min_angle = TEC_ebb_bearing - 180.0 - TEC_bearing_tolerance
        min_angle = mod(min_angle, 360.0)
        max_angle = TEC_ebb_bearing - 180.0 + TEC_bearing_tolerance
        max_angle = mod(max_angle, 360.0)
        tideState = 1
    if option == 'RE':
        min_angle = TEC_ebb_bearing - TEC_bearing_tolerance
        min_angle = mod(min_angle, 360.0)
        max_angle = TEC_ebb_bearing + TEC_bearing_tolerance
        max_angle = mod(max_angle, 360.0)
        tideState = 1
    return min_angle, max_angle, tideState


def gen_time_filter_case(timeStrs):
    dt_start = dateNumFromDateStr(timeStrs[0])
    dt_stop = dateNumFromDateStr(timeStrs[1])
    filter_str = "( ( times >= "+str(dt_start)+\
                 " ) AND ( times <= "+str(dt_stop)+" ) )"
    return filter_str

def gen_ambient_filter_case(ambient):
    if ambient == 0:
        filter_str = "( tideState = 0 )" # 0 = Ebb tide
    elif ambient == 1:
        filter_str = "( tideState = 1 )" # 1 = Flood tide
    else:
        filter_str = None
    return filter_str


def gen_tide_state_filter_case(tidestate):
    if tidestate == 0:
        filter_str = "( tideState = 0 )" # 0 = Ebb tide
    elif tidestate == 1:
        filter_str = "( tideState = 1 )" # 1 = Flood tide
    else:
        filter_str = None
    return filter_str


def gen_speed_bin_filter_case(SpdBin: float, DelSpd: float):
    SpdMinStr = str(SpdBin - abs(DelSpd))
    SpdMaxStr = str(SpdBin + abs(DelSpd))
    filter_str = "( ( " + SpdMinStr + " <= uref ) AND ( " + \
                          SpdMaxStr + " >= uref ) )"
    return filter_str


def gen_speed_filter_case(SpdRng):
    minSpdStr = str(SpdRng[0])
    maxSpdStr = str(SpdRng[1])
    if minSpdStr == "None":
        if maxSpdStr == "None":
            filter_str = None
        else:
            filter_str = "( " + maxSpdStr + " >= uref )"
    else:
        if maxSpdStr == "None":
            filter_str = "( "+ minSpdStr + " <= uref )"
        else:
            filter_str = "( ( " + minSpdStr + " <= uref ) AND ( " + \
                                  maxSpdStr + " >= uref ) )"
    return filter_str


def gen_orient_filter_case(TurbTide):
    min_bearing, max_bearing, tideState = get_turbine2tide_angle_range(TurbTide)
    minBrngStr = str(min_bearing)
    maxBrngStr = str(max_bearing)
    tideStr = str(tideState)
    filter_str = "( ( " + minBrngStr + " <= TECYaw ) AND ( " + \
                          maxBrngStr + " >= TECYaw ) AND ( " + \
                          " tideState = " + tideStr + ") )"
    return filter_str


def gen_power_filter_case(PwrRng, Allow_Nulls):
    minPwrStr = str(PwrRng[0])
    maxPwrStr = str(PwrRng[1])
    if minPwrStr == "None":
        if maxPwrStr == "None":
            filter_str = None
        else:
            filter_str = "( " + maxPwrStr + " >= TECPower )"
    else:
        if maxPwrStr == "None":
            filter_str = "( " + minPwrStr + " <= TECPower )"
        else:
            filter_str = "( ( " + minPwrStr + " <= TECPower ) AND ( " + \
                                  maxPwrStr + " >= TECPower ) )"
    if Allow_Nulls & (filter_str is not None):
        filter_str = "( " + filter_str + " OR ( TECPower IS NULL ) )"
    return filter_str


def gen_wavhgt_filter_case(Hm0Set, Allow_Nulls):
    Hm0Field = Hm0Set[0]
    minHm0Str = str(Hm0Set[1])
    maxHm0Str = str(Hm0Set[2])
    if minHm0Str == "None":
        if maxHm0Str == "None":
            filter_str = None
        else:
            filter_str = "( " + maxHm0Str + " >= " + Hm0Field + " )"
    else:
        if maxHm0Str == "None":
            filter_str = "( " + minHm0Str + " <= " + Hm0Field + " )"
        else:
            filter_str = "( ( " + minHm0Str + " <= " + Hm0Field + " ) AND ( " + \
                                  maxHm0Str + " >= " + Hm0Field + " ) )"
    if Allow_Nulls & (filter_str is not None):
        filter_str = "( " + filter_str + " OR ( "+ Hm0Field +" IS NULL ) )"
    return filter_str


def gen_wavprd_filter_case(TpRng, Allow_Nulls):
    minTpStr = str(TpRng[0])
    maxTpStr = str(TpRng[1])
    if minTpStr == "None":
        if maxTpStr == "None":
            filter_str = None
        else:
            filter_str = "( " + maxTpStr + " >= Tp )"
    else:
        if maxTpStr == "None":
            filter_str = "( " + minTpStr + " <= Tp )"
        else:
            filter_str = "( ( " + minTpStr + " <= Tp ) AND ( " + \
                                  maxTpStr + " >= Tp ) )"
    if Allow_Nulls & (filter_str is not None):
        filter_str = "( " + filter_str + " OR ( Tp IS NULL ) )"
    return filter_str


def create_filter_query(fields: list, deploy: list, filters: dict):

    sql_query = "SELECT "

    NFields = len(fields)
    if NFields == 0:
        fields = default_fields
    elif fields[0] == "*":
        fields = default_fields
    
    NDeploy = len(deploy)
    if NDeploy == 0:
        deploy = default_deploy
    elif deploy[0] == "*":
        deploy = default_deploy

    fieldset = fields
    for d in deploy:
        fieldset.append(d)
    NFields = len(fieldset)

    # select fields to return from query
    for n, f in enumerate(fieldset):
        if n < NFields-1:
            sql_query = sql_query+f+", "
        else:
            sql_query = sql_query+f+" FROM ts"
            
    # FILTER 01 - TIME
    # apply time filter if requested
    tmeFlt = filters['time']
    if tmeFlt is not None:
        #time_filter = gen_time_filter_case(tmeFlt[0], tmeFlt[1])
        time_filter = gen_time_filter_case(tmeFlt)
    else:
        time_filter = None
    if time_filter is not None:
        sql_query = sql_query + " WHERE " + time_filter

    # FILTER 02 - TIDESTATE
    # apply tide state flow filter if requested
    tidFlt = filters['tidestate']
    tide_filter = gen_tide_state_filter_case(tidFlt)
    if (sql_query[-1] == ')') and (tide_filter is not None):
        sql_query = sql_query + ' AND ' + tide_filter
    elif tide_filter is not None:
        sql_query = sql_query + " WHERE " + tide_filter

    # FILTER 03 - SPEED BIN
    # apply speed filter if requested
    spdFlt = filters['speed']
    if spdFlt is not None:
        speed_filter = gen_speed_filter_case(spdFlt)
    else:
        speed_filter = None
    if (sql_query[-1] == ')') and (speed_filter is not None):
        sql_query = sql_query + ' AND ' + speed_filter
    elif speed_filter is not None:
        sql_query = sql_query + " WHERE " + speed_filter
    
    # FILTER 04 - TURBINE ORIENTATION
    # apply turbine orientation filter if requested
    oriFlt = filters['orient']
    if oriFlt is not None:
        orient_filter = gen_orient_filter_case(oriFlt)
    else:
        orient_filter = None
    if (sql_query[-1] == ')') and (orient_filter is not None):
        sql_query = sql_query + ' AND ' + orient_filter
    elif orient_filter is not None:
        sql_query = sql_query + " WHERE " + orient_filter

    # FILTER 05 - TURBINE POWER
    # apply turbine power filter if requested
    pwrFlt = filters['power']
    if pwrFlt is not None:
        power_filter = gen_power_filter_case(pwrFlt[0:2], pwrFlt[2])
    else:
        power_filter = None
    if (sql_query[-1] == ')') and (power_filter is not None):
        sql_query = sql_query + ' AND ' + power_filter
    elif power_filter is not None:
        sql_query = sql_query + " WHERE " + power_filter

    # FILTER 06 - WAVES HM0 FILTER
    # apply waves Hm0 filter if requested
    hgtFlt = filters['wavesHm0']
    if hgtFlt is not None:
        wavhgt_filter = gen_wavhgt_filter_case(hgtFlt[0:3], hgtFlt[3])
    else:
        wavhgt_filter = None
    if (sql_query[-1] == ')') and (wavhgt_filter is not None):
        sql_query = sql_query + ' AND ' + wavhgt_filter
    elif wavhgt_filter is not None:
        sql_query = sql_query + " WHERE " + wavhgt_filter

    # FILTER 07 - WAVES PERIOD FILTER
    # apply waves period filter if requested
    prdFlt = filters['wavesTp']
    if prdFlt is not None:
        wavprd_filter = gen_wavprd_filter_case(prdFlt[0:2], prdFlt[2])
    else:
        wavprd_filter = None
    if (sql_query[-1] == ')') and (wavprd_filter is not None):
        sql_query = sql_query + ' AND ' + wavprd_filter
    elif wavprd_filter is not None:
        sql_query = sql_query + " WHERE " + wavprd_filter

    # terminate query string
    sql_query = sql_query + ';'
    return sql_query, fieldset


def apply_filter_query(db_conn, sql_query, fields):
    cur = db_conn.cursor()
    res = cur.execute(sql_query)
    if res > 0:
        results = cur.fetchall()
        if fields[0].strip() == '*':
            cur.execute("DESCRIBE ts;")
            tsFields = cur.fetchall()
            fields = [f[0] for f in list(map(list, tsFields))[1:]]
            data = array(list(results), dtype='float')
            data = data[:,1:]
        else:
            data = array(list(results), dtype='float')
        
        query_result = {}
        for n, f in enumerate(fields):
            if data.shape[0] > 0:
                query_result[f.strip()] = data[:,n]
            else:
                query_result[f] = []
    else:
        data = []
        if fields[0].strip() == '*':
            cur.execute("DESCRIBE ts;")
            tsFields = cur.fetchall()
            fields = [f[0] for f in list(map(list, tsFields))[1:-1]]
        query_result = {}
        for n, f in enumerate(fields):
            query_result[f] = []
    return data, query_result


#-----------------------------------------------------------------------------
# MODULE TEST CODE
#-----------------------------------------------------------------------------
