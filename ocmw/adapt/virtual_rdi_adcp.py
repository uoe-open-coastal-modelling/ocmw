# -*- coding: utf-8 -*-
"""
Created on Sun Aug 14 06:49:50 2022

@author: cold2
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt

'''
from toolbox.general.timeFuncs import dateNumFromStr
from toolbox.general.fileTools import loadmat

from flowChar import extract_redapt_motion
from flowChar import extract_model_ts_at_height
from flowChar import preprocess_ts

from flowChar import flow_class_diagram
'''

#-----------------------------------------------------------------------------
# GLOBAL CONSTANTS
#-----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
#   CLASS DEFINITIONS
# ----------------------------------------------------------------------------
class acoustic_beam:

    def __init__(self, azi: float, alt: float):
        self.uvec = self.__unit_vector(azi,alt)
        
    def __unit_vector(self, azi: float, alt: float):
        u = np.asarray([1.0,0.0,0.0])
        altrot = self.__alt_rotation_matrix(alt)
        azirot = self.__azi_rotation_matrix(azi)
        uvec = np.matmul(azirot,np.matmul(altrot,u))
        tol = 1e-16
        uvec[np.abs(uvec)<tol] = 0.0
        return uvec
    
    def __alt_rotation_matrix(self, theta):
        sa = np.sin(np.radians(theta))
        ca = np.cos(np.radians(theta))
        rotmat = np.asarray([[ca,0.0,-sa],[0.0,1.0,0.0],[sa,0.0,ca]])
        return rotmat

    def __azi_rotation_matrix(self,phi):
        sa = np.sin(np.radians(phi))
        ca = np.cos(np.radians(phi))
        rotmat = np.asarray([[ca,-sa,0.0],[sa,ca,0.0],[0.0,0.0,1.0]])
        return rotmat

class rdi_workhorse:
    
    def __init__(self):
        self.beam_angle = 20.0
        self.n_beams = 4
        self.down_facing = True
        
    def config(self, down_facing: bool, cell_size: float, blank_dist: float, n_cells: int):
        self.down_facing = down_facing
        self.cell_size = cell_size
        self.blank_dist = blank_dist
        self.n_cells = n_cells
        if self.down_facing:
            alt_sign = 1.0
        else:
            alt_sign = -1.0
        self.beams = []
        self.beams.append(acoustic_beam(0.0,(90.0-self.beam_angle)*alt_sign))
        self.beams.append(acoustic_beam(180.0,(90.0-self.beam_angle)*alt_sign))
        self.beams.append(acoustic_beam(270.0,(90.0-self.beam_angle)*alt_sign))
        self.beams.append(acoustic_beam(90.0,(90.0-self.beam_angle)*alt_sign))
    
    def rotate_beams(self, pitch: float, roll: float, heading: float):
        #if not self.down_facing:
        #    roll = roll + 180.0
        
        rrotmat = roll_rotation_matrix(roll)
        protmat = pitch_rotation_matrix(pitch)
        hrotmat = head_rotation_matrix(-heading)
        
        b1_uv = self.beams[0].uvec
        b2_uv = self.beams[1].uvec
        b3_uv = self.beams[2].uvec
        b4_uv = self.beams[3].uvec
        
        b1p = np.matmul(rrotmat,np.matmul(protmat,np.matmul(hrotmat,b1_uv)))
        b2p = np.matmul(rrotmat,np.matmul(protmat,np.matmul(hrotmat,b2_uv)))
        b3p = np.matmul(rrotmat,np.matmul(protmat,np.matmul(hrotmat,b3_uv)))
        b4p = np.matmul(rrotmat,np.matmul(protmat,np.matmul(hrotmat,b4_uv)))
        return b1p,b2p,b3p,b4p
        

#-----------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#-----------------------------------------------------------------------------
def roll_rotation_matrix(roll):
    sr = np.sin(np.radians(roll))
    cr = np.cos(np.radians(roll))
    rotmat = np.asarray([[cr,0.0,-sr],[0.0,1.0,0.0],[sr,0.0,cr]])
    return rotmat

def pitch_rotation_matrix(pitch):
    sp = np.sin(np.radians(pitch))
    cp = np.cos(np.radians(pitch))
    rotmat = np.asarray([[1.0,0.0,0.0],[0.0,cp,sp],[0.0,-sp,cp]])
    return rotmat

def head_rotation_matrix(heading):
    sh = np.sin(np.radians(heading))
    ch = np.cos(np.radians(heading))
    rotmat = np.asarray([[ch,sh,0.0],[-sh,ch,0.0],[0.0,0.0,1.0]])
    return rotmat

# ----------------------------------------------------------------------------
#   MAIN PROCESS
# ----------------------------------------------------------------------------

adcp = rdi_workhorse()
adcp.config(down_facing=False, cell_size=0.5, blank_dist=1.2, n_cells=80)

print(adcp.beams[0].uvec)
print(adcp.beams[1].uvec)
print(adcp.beams[2].uvec)
print(adcp.beams[3].uvec)
print()

#---------------------------------------------
# Emulate ADCP data sampling - Virtual ADCP
#---------------------------------------------

# HAB
zhub = 18.0

# Radial distance for vetical hub height
cba = np.cos(np.radians(adcp.beam_angle))
R = zhub/cba

# Rotate beams for pitch, roll and heading effects
pitch = 0.0
roll = 180.0
heading = 0.0

b1p, b2p, b3p, b4p = adcp.rotate_beams(pitch,roll,heading)

print(b1p)
print(b2p)
print(b3p)
print(b4p)
print()

# Set rotated beam extents and display
R1p = R*b1p
R2p = R*b2p
R3p = R*b3p
R4p = R*b4p

plt.figure()
plt.axes(projection='3d')
plt.plot([0.0,R1p[0]],[0.0,R1p[1]],[0.0,R1p[2]],'k')
plt.plot([0.0,R2p[0]],[0.0,R2p[1]],[0.0,R2p[2]],'g')
plt.plot([0.0,R3p[0]],[0.0,R3p[1]],[0.0,R3p[2]],'m')
plt.plot([0.0,R4p[0]],[0.0,R4p[1]],[0.0,R4p[2]],'c')

ax = plt.gca()
ax.set_xlim(-9,9)
ax.set_ylim(-9,9)
ax.set_zlim(0,18)

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Y')

