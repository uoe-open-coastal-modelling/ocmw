# Bathymetry

We will use an ESRI shapefile with point geometry as the default bathymetry data format.
For use with BlueKenue, the geometry needs to be included as the first two attributes or properties for each point record in the shapefile. 
The properties need to be in the order x-coord, y-coord , z-coord, where the x-coord is longitude or easting, the y-coord is latitude or northing, and the z-coord is depth.
It may be necessary to convert these to netCDF format for use with QMesh.

Bathymetry data is typically provided as local depths for the lowest astronomical (LAT) tide.
For modelling purposes, these need to be corrected to mean sea level (MSL).
The [UK Renewables Atlas](https://www.renewables-atlas.info/) provides tidal range data for the UK shelf, North Sea and coastal waters.
These can be used to either select a regional average correction or can be mapped to produce a more accurate correction across the model domain.
Alternatively the model can be run for the largest spring-tide period and the modelled tidal range can be used to correct the bathymetry data.

The UKHO provides open acess high resolution bathymetry data at 2m and 4m resolution for specific regions. 
These data are available as *.CSV and *.BAG files.
The *.CSV data should be used to generate the bathymetry shapefiles, and the *.BAG files can be loaded into GIS package as a raster for visualisation purposes.

Depending on the extent of the regional model the CRS will either be spherical (e.g. WGS84 Lat/Lon) or a flat Universal Transverse Mercator (UTM) projection (e.g. UTM Zone 30N).
Conversion between CRS can be done using a GIS package, GDAL software, or using python packages (e.g. shapely, geopandas, fiona, etc.).

## Open-access Data Sets

The two most common open-access bathymetry datasets are GEBCO (US product) and EMODNet (EU product). 
The GEBCO data are approximately 450m resolution and the EMODNet data are approximetly 100m resolution. 
For coastal modelling higher resolution bathymetry should be used. 
The UK Hydrographic Office (UKHO) now provide their high resolution (2 to 4m) bathymetry as open-access using a UK Governement public license. 
The UKHO data have unrestricted use, but the coverage is limited to areas where high resolution bathymetric surveys have been carried out.
Data service access is given in [Table 1](#table-1-open-access-bathymetry-datasets).

##### Table 1: Open-access bathymetry datasets.

| Data Set | Resolution | CRS | Data Service |
| ------ | ------ | ------ | ------ |
| GEBCO | 15 arc-seconds | WGS84 Lat/Lon | https://www.gebco.net/data_and_products/gridded_bathymetry_data/ |
| EMODNet | 3.75 arc-seconds | WGS84 Lat/Lon | https://emodnet.ec.europa.eu/geoviewer/ |
| UKHO | 2m or 4m | WGS84 Lat/Lon | https://data.admiralty.co.uk/portal/apps/sites/#/marine-data-portal |

High resolution bathymetric data covering most of the UK waters are available through DigiMap, but their uses is restricted to academic and educational only.

### Known Issues

An error in georeferencing of the EMODnet data has been identified.
The data are tiled as ESRI ASCII files with the cells defined relative to the lower lefthand (LL) corner with row order from north to south (i.e. with a negative step in the y direction).
The north-south ordering based on the LL corner introduces a half-cell offest southwards.
The data also appear to be for the cell centre rather than the LL corner, so there is also a half-cell offset eastwards.
This can be corrected by applying a translation to the geometry data in the shapefile (longitude minus half a cell, latitude plus half a cell).
