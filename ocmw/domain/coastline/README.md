# Coastline

There are a variety of open access coastline data available, some standard datasets are given in Table

| Dataset | Data Source |
| ------ | ------ |
| UKHO_satellite_derived_caostline_v1.shp | [UK Hydrographic Office](https://datahub.admiralty.co.uk/portal/apps/sites/#/marine-data-portal) |
| Europe_coastline.shp | [European Environment Agency](https://www.eea.europa.eu/data-and-maps/data/eea-coastline-for-analysis-1/gis-data/europe-coastline-shapefile) |
|    gshhg-shp_2.3.7   |  [SOEST, University of Hawaii](https://www.soest.hawaii.edu/pwessel/gshhg/)    |
| land-polygons-complete-4326 | [OpenStreetMap](https://osmdata.openstreetmap.de/data/coastlines.html) |

