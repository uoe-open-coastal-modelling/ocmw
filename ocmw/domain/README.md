# Model Domain Constraints

Methods, data sources, and tools for defining the model domain hard boundary constraint that is used to generate the model mesh.

The model domain is constrained by the hard boundaries defined by the coastline and bathymetry. 
The model domain extent is variable and depends on the region being modelled and the model design, this will be considered under [design](https://git.ecdf.ed.ac.uk/uoe-open-coastal-modelling/ocmw/-/tree/main/design).

## Coastline
Coastline data are generally available as polygons or lines.
The geometry required depends on the tools being used. 
In general coastline data need some manipulation for model definitions; this needs to be done maunally using GIS software (e.g. [QGIS](https://www.qgis.org/en/site/))

## Bathymetry
Bathymetry data are available in a variety of formats: [ESRI ASCII](https://desktop.arcgis.com/en/arcmap/latest/manage-data/raster-and-images/esri-ascii-raster-format.htm), [ESRI shapefiles](https://www.esri.com/content/dam/esrisites/sitecore-archive/Files/Pdfs/library/whitepapers/pdfs/shapefile.pdf), [CSV](https://en.wikipedia.org/wiki/Comma-separated_values), [BAG](https://www.ngdc.noaa.gov/mgg/bathymetry/noshdb/ons_fsd.pdf), [GeoTIFF](https://www.ogc.org/standard/geotiff/).
Most models take bathymetry data in point format to allow interpolation onto the model mesh nodes. 
The data are often provided as Lowest Astronomical Tide (LAT) depths for navigation purposes, whereas models require Mean Sea Level (MSL) depths.
The bathymetry data can be used to define a baseline mesh resolution for the mesh design process.

