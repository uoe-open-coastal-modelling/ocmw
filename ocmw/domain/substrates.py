# -*- coding: utf-8 -*-
"""
Functions to map sea bed substrate classes to friction coefficients.
"""

# ----------------------------------------------------------------------------
#   IMPORTS
# ----------------------------------------------------------------------------
# Standard Python Dependencies
# Non-Standard Python Dependencies
# Local Module Dependencies
# Other Dependencies


#--------------------------------------------------------------------------
# GLOBAL CONSTANTS
#--------------------------------------------------------------------------


#--------------------------------------------------------------------------
# FUNCTION DEFINITIONS
#--------------------------------------------------------------------------

def map_classname_C100(name):
    """
    Map the substrate class names to C_100 drag 
    coefficients.
    ( https://www.emodnet-geology.eu/data-products/seabed-substrates/ )

    Parameters
    ----------
    subs_class : integer
        Substrate class index.

    Returns
    -------
    C100 : float
        C_100 drag coefficient.
    name : string
        Substrate class name

    """
    if name.lower() in ['sand/silt','fine mud']:
        C100 = 0.0016
    elif name.lower in ['mud']:
        C100 = 0.0022
    elif name.lower in ['sand/shell','sand/gravel','mud/sand','gravel']:
        C100 = 0.0024
    elif name.lower in ['sand','sand (unrippled)']:
        C100 = 0.0026
    elif name.lower in ['sandy mud','muddy sand','sediment']:
        C100 = 0.0030
    elif name.lower in ['coarse','coarse substrate']:
        C100 = 0.0035
    elif name.lower in ['gravel','mixed sediiment']:
        C100 = 0.0047
    elif name.lower in ['seabed']:
        C100 = 0.0050
    elif name.lower in ['rock','boulders','hard substrate','sand (rippled)','rock or other hard substrata']:
        C100 = 0.0061
    else: # Default for unrecognised substrate is sand (unrippled)
        C100 = 0.0026
        
    return C100

def map_EMODNET_C100(subs_class):
    """
    Map the EMODNET Geology substrate classes to C_100 drag 
    coefficients.
    ( https://www.emodnet-geology.eu/data-products/seabed-substrates/ )

    Parameters
    ----------
    subs_class : integer
        Substrate class index.

    Returns
    -------
    C100 : float
        C_100 drag coefficient.
    name : string
        Substrate class name

    """
    if subs_class == 1:
        C100 = 0.0030
        name = 'Mud or Muddy Sand'
    elif subs_class == 2:
        C100 = 0.0026
        name = 'Sand'
    elif subs_class == 3:
        C100 = 0.0035
        name = 'Coarse Substrate'
    elif subs_class == 4:
        C100 = 0.0047
        name = 'Mixed Sediment'
    elif subs_class == 5:
        C100 = 0.0061
        name = 'Rock or Boulders'
    else:
        C100 = 0.0026
        name = 'Unkown'
    
    return C100,name


def map_EUSM_C100(subs_class):
    """
    Map the EMODNET Seabed Habitats substrate classes to C_100 drag 
    coefficients. 
    ( https://www.emodnet-seabedhabitats.eu/about/ )

    Parameters
    ----------
    subs_class : integer
        Substrate class index.

    Returns
    -------
    C100 : float
        C_100 drag coefficient.
    name : string
        Substrate class name

    """
    if subs_class == 1:
        C100 = 0.0016
        name = 'Fine Mud'
    elif subs_class == 2:
        C100 = 0.0026
        name = 'Sand'
    elif subs_class == 3:
        C100 = 0.0030
        name = 'Sandy Mud or Muddy Sand or Sediment'
    elif subs_class == 4:
        C100 = 0.0035
        name = 'Coarse Substrate'
    elif subs_class == 5:
        C100 = 0.0047
        name = 'Mixed Sediment'
    elif subs_class == 6:
        C100 = 0.0050
        name = 'Seabed'
    elif subs_class == 7:
        C100 = 0.0061
        name = 'Rock or Hard Substrate'
    else:
        C100 = 0.0026
        name = 'Unknown'
    
    return C100,name

